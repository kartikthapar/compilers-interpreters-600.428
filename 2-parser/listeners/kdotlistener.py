#!/usr/bin/python
#
# kdotlistener.py
#
# Created by Kartik Thapar on 02/25/2013 at 15:54:57
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Implements class dot listener for the Parser.

class DotListener (Listener)
"""

"""
See output type on Piazza.
"""

from klistener import Listener as KListener

class DotListener(KListener):
	"""
	Dot Listener: observes events related to parse tree dot output.
	"""

	def __init__(self):
		self.__boxStack = []
		self.__labelCounter = 0
		self.__dotOutput = ""


	def _generateLabel (self, value, shape):
		leftlabel = "L%d" % (self.__labelCounter)
		rightlabel = "[label=\"%s\",shape=%s]" % (value, shape)
		self.__dotOutput += leftlabel + " " + rightlabel + "\n"


	def _generateFlow (self):

		leftlabel = self.__boxStack[-1] # get top most element
		rightlabel = "L%d" % (self.__labelCounter)
		self.__dotOutput += leftlabel + " -> " + rightlabel + "\n"


	def inNonTerminal (self, nterm):
		# print self.__boxStack

		if not self.__boxStack:
			self.__boxStack = ["L0"] # starts with L0
			self._generateLabel(nterm, "box")
		else:
			self._generateLabel(nterm, "box")
			self._generateFlow()
			self.__boxStack.append("L%d" % (self.__labelCounter))
		
		self.__labelCounter += 1


	def outNonTerminal (self):
		# print self.__boxStack

		self.__boxStack.pop()


	def matchTerminal (self, token):
		# print self.__boxStack

		value = ""
		if token.kind() is not "eof":
			value = token.value()
		else:
			value = "eof"

		self._generateLabel(value, "diamond")
		self._generateFlow()
		self.__labelCounter += 1


	def dotOutput (self):
		"""
		Returns dot output from Parser.
		"""

		# change from temp X to strict digraph CST as in Piazza
		self.__dotOutput = "strict digraph CST {\n" + self.__dotOutput + "}"
		return self.__dotOutput

