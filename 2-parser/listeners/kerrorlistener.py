#!/usr/bin/python
#
# kerrorlistener.py
#
# Created by Kartik Thapar on 02/26/2013 at 21:57:21
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Implements class error listener for the Parser.

class ErrorListener (Listener)
"""

from klistener import Listener as KListener

class ErrorListener (KListener):

	"""
	Error Listener: observes error events.
	"""

	def __init__(self):
		self.__errorOutput = []


	def handleError (self, error):
		"""
		Adds errors to a list of errors.
		"""

		self.__errorOutput.append(error)


	def errorOutput (self):
		"""
		Returns the error list to the driver
		"""

		return self.__errorOutput


	def isError (self):
		"""
		Public method to check if errors exist.
		"""

		if self.__errorOutput:
			return True
		else:
			return False

