#!/usr/bin/python
#
# kparser.py
#
# Created by Kartik Thapar on 02/23/2013 at 02:31:40
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Parser for SIMPLE
"""

import sys
import kexception as KExcept
import types as PType
import listeners.klistener as Klistener


"""
identifier = letter {letter | digit} .
integer = digit {digit} .
letter = "a" | "b" | .. | "z" | "A" | "B" | .. | "Z" .
digit = "0" | "1" | .. | "9" .
"""

# see @91 Piazza - for more information
weakTokens = ["[", "]","(", ")", ";", "."]
strongTokens = ["CONST", "TYPE", "VAR", "IF", "REPEAT", "WHILE", "WRITE", "READ"]
strongDecl = ["CONST", "TYPE", "VAR"]
strongInstr = ["IF", "REPEAT", "WHILE", "WRITE", "READ"]

ERROR_MOVE_OVER = 8 # spec


class Parser:
    """
    Parser:
    """
    
    def __init__(self, tokenList):
        
        assert type(tokenList) is PType.ListType
        self.__tokenList = tokenList

        self.__tokenCounter = 0
        self.__numberOfTokens = len(tokenList)

        self.__prog = "" # not required right now.
        self.__listeners = [] # listeners for events in Parser

        # hack code --- needs to behave in future assignments
        self.__toReport = 0
        

    # ---------------------------------------------------------------
    # Define Listener functions
    #
    # - (void)registerListener: register listener
    # - (void)removeListener: remove listener
    # - (void)_notifyListeners: dispatch notifications to listeners
    # - (void)_notifyInNonTerminal
    # - (void)_notifyOutNonTerminal
    # - (void)_notifyMatchTerminal
    # ---------------------------------------------------------------


    def registerListener (self, listener):
        """
        Register listener to Parser.
        """

        # Can be extended to be based on a particular event. Not required at this point

        # we have different implementations of DOT and TEXT
        # therefore we can simply have inNonTerminal, outNonTerminal, matchTerminal
        # methods that simply call the respective methods of TextListener and
        # DotListener.

        # This way we don't have to care about the differences in respondence from DOT
        # or TEXT

        assert isinstance(listener, Klistener.Listener)
        assert listener not in self.__listeners
        self.__listeners.append(listener)


    def removeListener (self, listener):
        """
        Remove listener from the list of listeners for Parser.
        """

        assert isinstance(listener, Klistener.Listener)
        assert listener in self.__listeners

        self.__listeners.remove(listener)


    def _notifyListeners (self, event = None, context = None):
        """
        Dispatch notifications to all listeners. Notifications are
        specific to their registration.
        """

        assert event is not None # event cannot be default

        # depending on the type of event ---> do notify
        if event is "inNonTerminal":
            self._notifyInNonTerminal(context)
        elif event is "outNonTerminal":
            self._notifyOutNonTerminal()
        elif event is "matchTerminal":
            self._notifyMatchTerminal(context)
        elif event is "handleError": # if error
            self._notifyError(context)


    def _notifyInNonTerminal (self, context):
        """
        Notify listeners to execute inNonTerminal().
        """

        for listener in self.__listeners:
            listener.inNonTerminal(context)


    def _notifyOutNonTerminal (self):
        """
        Notify listeners to execute outNonTerminal().
        """
        
        for listener in self.__listeners:
            listener.outNonTerminal()


    def _notifyMatchTerminal (self, context):
        """
        Notify listeners to execute matchTerminal().
        """

        for listener in self.__listeners:
            listener.matchTerminal(context)


    def _notifyError (self, context):
        """
        Notify listeners (specifically ErrorListener) to handle error.
        """

        assert type(context) is str
        for listener in self.__listeners:
            listener.handleError(context)


    # ---------------------------------------------------------------
    # Define HELPER functions
    #
    # - (BOOL)_tokenExists: is code exists
    # - (void)_counter: increments file read counter
    # - (Token)_currentToken: check if part of language
    # ---------------------------------------------------------------


    def _tokenExists (self):
        """
        Checks if tokens exist.
        """

        if self.__tokenCounter < self.__numberOfTokens:
            return True
        else:
            return False


    def _currentToken (self):
        """
        Returns the current token from the list of tokens.
        """
        
        tokenValue = self.__tokenList[self.__tokenCounter] # retrieve token
        return tokenValue


    def _counter (self):
        self.__tokenCounter += 1 # increment the token counter


    # ---------------------------------------------------------------
    # Define MATCH functions
    #
    # - (void)_reportError: add error to list based on move_over
    # - (void)_weakError: reports weak error
    # - (void)_nonWeakError: reports strong error + bad stuff
    # - (BOOL)_checkToken: increments file read counter
    # - (Token/void)_matchToken: check if part of language
    # ---------------------------------------------------------------


    def _reportError (self, error):

        if self.__toReport > 0: # will only add to output if move_over is not breached
            return

        token = self._currentToken()

        # eof should've had a value as well --- goddamn eof. change it soon.
        val = ""
        if token.kind() is "eof":
            val = "eof"
        else:
            val = token.value()

        # generate error message. Don't scare / but don't be discrete. This is a class.
        a = "token value mismatch --- "
        b = str(error) + "; "
        c = "found: '%s' at position: '%d'." % (val, token.startPosition())

        self._notifyListeners("handleError", a + b + c) # add to error list
        
        # after you report the error; set the counter again
        self.__toReport = ERROR_MOVE_OVER


    def _weakError (self, error):
        """
        Weak Error implementation. This simply adds the error to the list of errors.
        Major error checking is part of the match function.
        """

        # report error
        self._reportError(error)


    def _nonWeakError (self, error):

        """
        Non Weak Error implementation. Only called when the compiler has acknowledged
        move_over tokens (number of tokens that must be ignored in case of an error).
        Restarts parsing to the state of next Instruction OR Declaration depending on the
        type of strong token discovered.
        """

        # report error
        token = self._currentToken()
        self._reportError(error)

        # depending on the type of wrong token --- do Decl or Instr
        # while token is not EOF or is not in the strong tokens list
        #   move on and check for Decl and Instr
        try:
            while (self._currentToken().kind() is not "eof") and\
              (self._currentToken().value() not in strongTokens):
                self._counter()
                if self.__toReport > 0: # still need to count for this
                    self.__toReport -= 1
                if self._currentToken().value() in strongDecl:
                    self._Declarations()
                elif self._currentToken().value() in strongInstr:
                    self._Instructions()
        # because this is now are code of execution stream --> errors will be here.
        except KExcept.TokenMismatchException, e:
            self._nonWeakError(e)


    def _checkToken (self, expectedTokenList):
        
        kind = "" 
        # temp kind value --- why did I classify so much
        # if possible, change scanner :(

        # set this straight ---> change code in future assignments :: Maybe?
        if self._currentToken().kind() == "identifier":
            kind = "identifier"
        elif self._currentToken().kind() == "integer":
            kind = "integer"
        elif self._currentToken().kind() == "symbol":
            kind = self._currentToken().value()
        elif self._currentToken().kind() == "keyword":
            kind = self._currentToken().value()
        elif self._currentToken().kind() == "eof":
            kind = "eof"
        else:
            assert False

        # look for kind in token list
        if kind in expectedTokenList:
            return True
        else:
            return False


    def _matchToken (self, expectedTokenList):
        """
        Match a terminal value in the program as specified in the grammar.
        """

        if self.__tokenCounter == len(self.__tokenList):
            return # don't check if it's gone mad

        token = self._currentToken()

        # if token is fine ---> notify and move ahead
        if self._checkToken(expectedTokenList):
            if token.kind() is not "eof":
                self._notifyListeners("matchTerminal", token)
            self._counter() # increment counter after token match
            
            if self.__toReport > 0:
                self.__toReport -= 1
            
            return token
        else:
            return False
            


    # ---------------------------------------------------------------
    # Program = "PROGRAM" identifier ";" Declarations
    #   ["BEGIN" Instructions] "END" identifier "." .
    # ---------------------------------------------------------------


    def _Program (self):
        """
        Program = "PROGRAM" identifier ";" Declarations
            ["BEGIN" Instructions] "END" identifier "." .
        """

        self._notifyListeners("inNonTerminal", "Program")

        if not self._matchToken(["PROGRAM"]):
            raise KExcept.TokenMismatchException("expected 'PROGRAM' at the start of the program")

        if not self._matchToken(["identifier"]):
            raise KExcept.TokenMismatchException(
                "expected an identifier to name the program at the start of the program"
            )

        if not self._matchToken([";"]):
            self._weakError("expected ';' to end the program declaration statement")

        self._Declarations()

        # ["BEGIN" Instructions]
        if self._checkToken(["BEGIN"]):
            self._matchToken(["BEGIN"])
            self._Instructions()

        if not self._matchToken(["END"]):
            self._weakError("expected 'END' to end the current program")

        if not self._matchToken(["identifier"]):
            raise KExcept.TokenMismatchException("expected identifier to match the end of the program")

        if not self._matchToken(["."]): # this is specific to PROGRAM
            self._weakError("expected '.' to end the SIMPLE program")

        self._notifyListeners("outNonTerminal")

    # ---------------------------------------------------------------
    # Designator = identifier Selector .
    # Selector = {"[" ExpressionList "]" | "." identifier} .
    # IdentifierList = identifier {"," identifier} .
    # ExpressionList = Expression {"," Expression} .
    # ---------------------------------------------------------------


    def _Designator (self):
        self._notifyListeners("inNonTerminal", "Designator")
        
        self._matchToken(["identifier"])

        self._Selector()

        self._notifyListeners("outNonTerminal")

    
    def _Selector (self):
        self._notifyListeners("inNonTerminal", "Selector")

        while (self._checkToken(["["]) or self._checkToken(["."])):
            if self._checkToken(["["]):
                self._matchToken(["["])
                self._ExpressionList()
                if not self._matchToken(["]"]):
                    self._weakError("expected ']' to close the expression list")
            
            elif self._checkToken(["."]):
                self._matchToken(["."])
                if not self._matchToken(["identifier"]):
                    raise KExcept.TokenMismatchException(
                        "expected identifier in the selector"
                    )

        self._notifyListeners("outNonTerminal")


    def _IdentifierList (self):
        self._notifyListeners("inNonTerminal", "IdentifierList")
        
        self._matchToken(["identifier"])
        
        while self._checkToken([","]):
            self._matchToken([","])
            
            if not self._matchToken(["identifier"]):
                raise KExcept.TokenMismatchException(
                    "expected an identifier to continue the list of identifiers"
                )

        self._notifyListeners("outNonTerminal")
    

    def _ExpressionList (self):
        self._notifyListeners("inNonTerminal", "ExpressionList")

        self._Expression()
        while self._checkToken([","]):
            self._matchToken([","])
            self._Expression()

        self._notifyListeners("outNonTerminal")


    # ---------------------------------------------------------------
    # Instructions = Instruction {";" Instruction} .
    # Instruction = Assign | If | Repeat | While | Read | Write .
    # Assign = Designator ":=" Expression .
    # If = "IF" Condition "THEN" Instructions ["ELSE" Instructions] "END" .
    # Repeat = "REPEAT" Instructions "UNTIL" Condition "END" .
    # While = "WHILE" Condition "DO" Instructions "END" .
    # Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
    # Write = "WRITE" Expression .
    # Read = "READ" Designator .
    # ---------------------------------------------------------------


    def _Instruction (self):
        self._notifyListeners("inNonTerminal", "Instruction")

        if self._checkToken(["identifier"]): # all other are keywords duh!
            self._Assign()
        elif self._checkToken(["IF"]):
            self._If()
        elif self._checkToken(["REPEAT"]):
            self._Repeat()
        elif self._checkToken(["WHILE"]):
            self._While()
        elif self._checkToken(["READ"]):
            self._Read()
        elif self._checkToken(["WRITE"]):
            self._Write()
        else:
            raise KExcept.TokenMismatchException(
                "expected one of 'assignment using an identifier', 'IF', 'WHILE', 'REPEAT', 'READ', 'WRITE'"
            )

        self._notifyListeners("outNonTerminal")
    

    def _Instructions (self):
        self._notifyListeners("inNonTerminal", "Instructions")

        self._Instruction()
        
        while self._checkToken([";"]):
            self._matchToken([";"])
            self._Instruction()

        self._notifyListeners("outNonTerminal")


    def _Assign (self):
        self._notifyListeners("inNonTerminal", "Assign")

        self._Designator()

        self._matchToken([":="])
        
        self._Expression()

        self._notifyListeners("outNonTerminal")


    def _If (self):
        self._notifyListeners("inNonTerminal", "If")

        self._matchToken(["IF"])
        self._Condition()
        self._matchToken(["THEN"])
        self._Instructions()

        # ["ELSE" Instructions]
        if self._checkToken(["ELSE"]):
            self._matchToken(["ELSE"])
            self._Instructions()

        self._matchToken(["END"])

        self._notifyListeners("outNonTerminal")


    def _Repeat (self):
        self._notifyListeners("inNonTerminal", "Repeat")

        self._matchToken(["REPEAT"])
        self._Instructions()
        self._matchToken(["UNTIL"])
        self._Condition()
        self._matchToken(["END"])

        self._notifyListeners("outNonTerminal")


    def _While (self):
        self._notifyListeners("inNonTerminal", "While")

        self._matchToken(["WHILE"])
        self._Condition()
        self._matchToken(["DO"])
        self._Instructions()
        self._matchToken(["END"])

        self._notifyListeners("outNonTerminal")


    def _Condition (self):
        self._notifyListeners("inNonTerminal", "Condition")

        self._Expression()
        matchTokenList = ["=", "#", "<", ">", "<=", ">="]
        self._matchToken(matchTokenList)
        self._Expression()
        
        self._notifyListeners("outNonTerminal")


    def _Write (self):
        self._notifyListeners("inNonTerminal", "Write")

        self._matchToken(["WRITE"])
        self._Expression()
        
        self._notifyListeners("outNonTerminal")


    def _Read (self):
        self._notifyListeners("inNonTerminal", "Read")

        self._matchToken(["READ"])
        self._Designator()

        self._notifyListeners("outNonTerminal")


    # ---------------------------------------------------------------
    # Expression = ["+"|"-"] Term {("+"|"-") Term} .
    # Term = Factor {("*"|"DIV"|"MOD") Factor} .
    # Factor = integer | Designator | "(" Expression ")" .
    # ---------------------------------------------------------------


    def _Expression (self):
        self._notifyListeners("inNonTerminal", "Expression")

        if self._checkToken(["+", "-"]):
            self._matchToken(["+", "-"])

        self._Term()

        while self._checkToken(["+", "-"]):
            self._matchToken(["+", "-"])
            self._Term()

        self._notifyListeners("outNonTerminal")


    def _Term (self):
        self._notifyListeners("inNonTerminal", "Term")

        self._Factor()

        while self._checkToken(["*", "DIV", "MOD"]):
            self._matchToken(["*", "DIV", "MOD"])
            self._Factor()

        self._notifyListeners("outNonTerminal")


    def _Factor (self):
        self._notifyListeners("inNonTerminal", "Factor")

        if self._checkToken(["integer"]):
            self._matchToken(["integer"]) # move on
        
        elif self._checkToken(["identifier"]):
            self._Designator()
        
        elif self._checkToken(["("]):
            self._matchToken(["("])
            
            self._Expression()
            
            if not self._matchToken([")"]):
                self._weakError("expected ')' to close the expression defined")
        else:
            raise KExcept.TokenMismatchException(
                "expected an integer or identifier or '(' or read the syntax first."
            )

        self._notifyListeners("outNonTerminal")


    # ---------------------------------------------------------------
    # Type = identifier | "ARRAY" Expression "OF" Type |
    #   "RECORD" {IdentifierList ":" Type ";"} "END" .
    # ---------------------------------------------------------------


    def _Type (self):
        self._notifyListeners("inNonTerminal", "Type")

        if self._checkToken(["identifier"]):
            self._matchToken(["identifier"])
        
        elif self._checkToken(["ARRAY"]):
            self._matchToken(["ARRAY"])
            
            self._Expression()
            
            if not self._matchToken(["OF"]):
                raise KExcept.TokenMismatchException("expected 'OF' in ARRAY declaration")
            self._Type()
        
        elif self._checkToken(["RECORD"]):
            self._matchToken(["RECORD"])
            
            while self._checkToken("identifier"):
                self._IdentifierList()
                
                if not self._matchToken([":"]):
                    raise KExcept.TokenMismatchException("expected ':' in RECORD identifier declaration")
                
                self._Type()
                
                if not self._matchToken([";"]):
                    self._weakError("expected ';' in RECORD declaration in TYPE")
            
            if not self._matchToken(["END"]):
                self._weakError("expected 'END' at the end of RECORD declaration in TYPE")
        
        else: # if Type gets nothing from above
            raise KExcept.TokenMismatchException("identifier|ARRAY|RECORD")

        self._notifyListeners("outNonTerminal")


    # ---------------------------------------------------------------
    # Declarations = { ConstDecl | TypeDecl | VarDecl } .
    # ConstDecl = "CONST" {identifier "=" Expression ";"} .
    # TypeDecl = "TYPE" {identifier "=" Type ";"} .
    # VarDecl = "VAR" {IdentifierList ":" Type ";"} .
    # ---------------------------------------------------------------

    def _Declarations (self):
        self._notifyListeners("inNonTerminal", "Declarations")

        # check current token to be of type CONST TYPE VAR
        while self._checkToken (["CONST", "TYPE", "VAR"]):
            if self._checkToken(["CONST"]):
                self._ConstDecl()
            elif self._checkToken(["TYPE"]):
                self._TypeDecl()
            elif self._checkToken(["VAR"]):
                self._VarDecl()

        self._notifyListeners("outNonTerminal")


    def _ConstDecl (self):
        self._notifyListeners("inNonTerminal", "ConstDecl")

        self._matchToken(["CONST"]) # now match

        while self._checkToken(["identifier"]):
            self._matchToken(["identifier"])
            
            if not self._matchToken(["="]):
                raise KExcept.TokenMismatchException("expected '=' in CONST declaration")
            
            self._Expression()
            
            if not self._matchToken([";"]):
                self._weakError("expected ';' at the end of CONST declaration")

        self._notifyListeners("outNonTerminal")


    def _TypeDecl (self):
        self._notifyListeners("inNonTerminal", "TypeDecl")

        self._matchToken(["TYPE"])

        while self._checkToken(["identifier"]):
            self._matchToken(["identifier"])
            if not self._matchToken(["="]):
                raise KExcept.TokenMismatchException("expected '=' in TYPE declaration")
            self._Type()
            if not self._matchToken([";"]):
                self._weakError("expected ';' at the end of TYPE declaration")

        self._notifyListeners("outNonTerminal")


    def _VarDecl (self):
        self._notifyListeners("inNonTerminal", "VarDecl")

        self._matchToken(["VAR"])

        while self._checkToken(["identifier"]):
            self._IdentifierList()
            if not self._matchToken([":"]):
                raise KExcept.TokenMismatchException(
                    "expected ':' in VAR declaration OR "
                    "check your instruction declaration"
                )
            self._Type()
            if not self._matchToken([";"]):
                self._weakError("expected ';' at the end of VAR declaration")

        self._notifyListeners("outNonTerminal")


    # ------------------------------------------------------------------
    def parse (self):

        # catch mismatch exception here
        try:
            self._Program()
        except KExcept.TokenMismatchException, e:
            self._nonWeakError(e)
        
        self._matchToken(["eof"])

