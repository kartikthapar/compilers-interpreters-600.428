# Assignment 2

# List of files:

	./kexception.py
	./kparser.py
	./kscanner.py
	./ktoken.py
	./listeners
	./listeners/__init__.py
	./listeners/kdotlistener.py
	./listeners/kerrorlistener.py
	./listeners/klistener.py
	./listeners/ktextlistener.py
	./README
	./sc

# How to run:

`sc` is the binary that needs to be executed.

	> ./sc -s # for scanner
	# requires standard input
	
	> ./sc -s filename # for scanner
	# requires filename to be a valid file

	> ./sc -c # for parser; also generates CST
	# requires standard input

	> ./sc -c filename # for parser; also generates CST
	# requires filename to be valid

	> ./sc -cg # for parser; also generates graph output
	# requires standard input

	> ./sc -cg filename # for parser; also generates graph output
	# requires filename to be valid

A pointer about checking text/graph output ---

1. It is possible that the error doesn't match with a simple diff (or other sophisticated tools) as I use tabs etc.

2. According to diff manual:

		-b  --ignore-space-change
			Ignore changes in the amount of white space.

		-w  --ignore-all-space
			Ignore all white space.

		-B  --ignore-blank-lines
			Ignore changes whose lines are all blank.

	Use `-b` to ignore space change; can also use `-w` as mentioned above.

	
# Notes:

- README is `this` file.
- `./listeners/__init__.py` ---> empty (used as Python complains about 'no modules' stuff)
- `./listeners/*listener.py` are observers that do specific tasks.
	- Text Listener: outputs concrete syntax tree (`sc -c`)
	- Dot Listener: outputs graph out (as required) (`sc -cg`)
	- Error Listener: outputs errors in the input code; errors won't produce CST or graph output.
	