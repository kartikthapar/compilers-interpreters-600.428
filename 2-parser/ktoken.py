#!/usr/bin/python
#
# token.py
#
# Created by Kartik Thapar on 02/12/2013 at 22:34:00
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import types as PType

"""

Different types of tokens possible:

	- One kind of tokens are keywords such as IF and WHILE which 
	don't have any additional "semantic" information attached.
	- Another kind of token are identifiers such as super and label1 
	for which you need to store (a) the fact that they are identifiers, 
	and (b) the actual string. 
	- For numbers such as 64738 you should store their integer value.

	So how many kinds: IF|WHILE|labels & integers

"""

class Token:


	def __init__(self, kind, startPosition, endPosition, value = None):
		"""
		object = Token(kind, startPosition, endPosition, value = None):

		kind: kind of token - identifiers, integers, keywords, symbols
		start_position: start position
		end_position: end position
		value: None | valid if integers or identifiers of symbols
		"""

		# Improvements: sposition, eposition methods to be dealt by another class?
		
		assert type(kind) is PType.StringType
		assert type(startPosition) is PType.IntType
		assert type(endPosition) is PType.IntType

		# value is either None

		assert \
		(type(value) is PType.StringType and (kind is "identifier" or kind is "symbol" or kind is "keyword" )) \
		or (type(value) is (PType.IntType or PType.LongType) and kind is "integer")\
		or (type(value) is PType.NoneType and kind is "eof")

		self.__kind = kind # kind?
		self.__startPosition = startPosition # start position
		self.__endPosition = endPosition # end position

		# do all tokens have a value? only identifiers & integers

		self.__value = value

		# self.logToken()


	def logToken (self):

		lstr = "Token(%s, %s, %s, %s)" % \
		(self.kind(), self.startPosition(), self.endPosition(), self.value())

		return lstr

	#http://stackoverflow.com/questions/3691101/what-is-the-purpose-of-str-and-repr-in-python


	def __str__ (self):

		rs = ""

		if self.kind() in ["keyword", "symbol"]:
			rs = "%s@(%s, %s)" % (self.value(), self.startPosition(), self.endPosition())
		elif self.kind() in ["identifier", "integer"]:
			rs = "%s<%s>@(%s, %s)" % (self.kind(), self.value(), self.startPosition(), self.endPosition())
		elif self.kind() in ["eof"]:
			rs = "%s@(%s, %s)" % (self.kind(), self.startPosition(), self.endPosition())

		# assert rs is
		return rs

	def __repr__ (self):
		# official representation
		rs = self.logToken()
		return rs

	def kind (self):
		"""
		Return: kind of token
		"""

		return self.__kind


	def startPosition (self):
		"""
		Return: start position of token
		"""

		return self.__startPosition


	def endPosition (self):
		"""
		Return: end position of token
		"""

		return self.__endPosition


	def value (self):
		"""
		Return: value of the token
		"""

		# what will it return if the value is not an integer?
		return self.__value


