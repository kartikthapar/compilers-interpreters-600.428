#
# kdotvisitor.py
#
# Created by Kartik Thapar on 03/12/2013 at 18:46:30
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

from kvisitor import Visitor
IDENT_VAL = 2

class DotVisitor (Visitor):
  """
  Implements DOT output (graph output) for the Symbol Table.
  Visits Constants, Variables, Integers, Arrays, Records, Scopes to analyze the symbol table data structure.
  """
  
  def __init__ (self):
    self.__dotOutput = ""
    self.__traversed = {}

  def _makeIndent (self):
    """Calculates and returns the tabbed indent for a particular ..."""
    return " " * IDENT_VAL * self.__indentationLevel

  def _makeDot (self, value):
    self.__dotOutput += value + "\n"

  def visitConstant (self, constant):
    idvalue = id(constant) # get id
    if idvalue in self.__traversed: # if id exists, return, else create
      return self.__traversed[idvalue]
    else:
      etype = constant.elementType().accept(self) # get element type first
      anchor = "_anchor_" + str(idvalue) # make anchor with Id
      self._makeDot(anchor + " " + "[label=\"%s\",shape=diamond]" % (constant.value()))
      self.__traversed[idvalue] = anchor # add to list after you traverse it
      self._makeDot(anchor + " -> " + etype) # add to output
      return anchor

  def visitVariable (self, variable):
    idvalue = id(variable)
    if idvalue in self.__traversed:
      return self.__traversed[idvalue]
    else:
      etype = variable.elementType().accept(self)
      anchor = "_anchor_" + str(idvalue)
      self._makeDot(anchor + " " + "[label=\"\",shape=circle]")
      self.__traversed[idvalue] = anchor
      self._makeDot(anchor + " -> " + etype)
      return anchor

  def visitInteger (self, integer):
    idvalue = id(integer)
    if idvalue in self.__traversed:
      return self.__traversed[idvalue]
    else:
      anchor = "_anchor_" + str(idvalue)
      self._makeDot(anchor + " " + "[label=\"Integer\",shape=box,style=rounded]")
      self.__traversed[idvalue] = anchor
      return anchor

  def visitArray (self, array):
    idvalue = id(array)
    if idvalue in self.__traversed:
      return self.__traversed[idvalue]
    else:
      etype = array.elementType().accept(self)
      anchor = "_anchor_" + str(idvalue)
      self._makeDot(anchor + " " + "[label=\"Array\\nlength: %s\",shape=box,style=rounded]" % array.length())
      self.__traversed[idvalue] = anchor
      self._makeDot(anchor + " -> " + etype)
      return anchor

  def visitRecord (self, record):
    idvalue = id(record)
    if idvalue in self.__traversed:
      return self.__traversed[idvalue]
    else:
      etype = record.scope().accept(self)
      anchor = "_anchor_" + str(idvalue)
      self._makeDot(anchor + " " + "[label=\"Record\",shape=box,style=rounded]")
      self.__traversed[idvalue] = anchor
      self._makeDot(anchor + " -> " + etype)
      return anchor

  def visitScope (self, scope):
    idvalue = id(scope)
    if idvalue in self.__traversed:
      return self.__traversed[idvalue]
    else:
      # consider everything in scope and draw it -> nested scopes are fine
      # recursive works
      st = scope.symbolTable()
      for identifer in sorted(st):
        st[identifer].accept(self)

      # do_scope
      anchor = "_anchor_" + str(idvalue)
      self._makeDot("subgraph cluster_%s {" % idvalue)
      for identifier in sorted(st):
        self._makeDot("%s_%s [label=\"%s\",shape=box,color=white,fontcolor=black]" % (identifier, idvalue, identifier))
      self._makeDot(anchor + " " + "[label=\"\",style=invis]")
      self._makeDot("}")
      for identifier in sorted(st):
        self._makeDot("%s_%s -> %s" % (identifier, idvalue, self.__traversed[id(st[identifier])]))
      self.__traversed[idvalue] = anchor
      return anchor


  def dotOutput (self):
    """Returns DOT output of the symbol table (from the parser)."""
    self.__dotOutput = "strict digraph CST {\n" + self.__dotOutput + "}"
    return self.__dotOutput
      