#!/usr/bin/python
#
# kerrorlistener.py
#
# Created by Kartik Thapar on 02/26/2013 at 21:57:21
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

from klistener import Listener as KListener

class ErrorListener (KListener):
  """Error Listener: observes error events."""

  def __init__(self):
    self.__errorOutput = []

  def addError (self, error):
    """Add an error to the error output list."""
    self.__errorOutput.append(error)

  def errorOutput (self):
    """Returns the error list to the driver"""
    return self.__errorOutput

  def isError (self):
    """Public method to check if errors exist."""
    return True if self.__errorOutput else False


class ParserErrorListener (ErrorListener):
  """Error listener for Parser errors. Only syntactic errors recorded."""
  
  def handleError (self, error, errorType):
    """Handle parser errors that only relate to syntactic parsing."""
    if errorType == "parser_error":
      self.addError(error)
      return


class SymbolTableErrorListener (ErrorListener):
  """Error listener for Parser --- Symbol Table erors. Only semantic errors are recorded."""
  
  def handleError (self, error, errorType):
    """Handle symbol table (type) errors in the Parser."""
    if errorType == "table_error":
      self.addError(error)
      return


class AbstractSyntaxTreeErrorListener (ErrorListener):
  """Error listener for Parser --- Symbol Table erors. Only semantic errors are recorded."""

  def handleError (self, error, errorType):
    """Handle abstract syntax tree errors in the Parser."""
    if errorType == "tree_error":
      self.addError(error)
      return

  