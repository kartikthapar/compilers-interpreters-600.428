#!/usr/bin/python
#
# ktextlistener.py
#
# Created by Kartik Thapar on 02/24/2013 at 23:20:40
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

from klistener import Listener
IDENT_VAL = 2

class TextListener(Listener):
  """Text Listener: observes events related to parse tree output."""

  def __init__(self):
    """Set indentation level and create text output string"""
    self.__indentationLevel = 0
    self.__textOutput = ""

  def textOutput (self):
    """Strips new line from the output and return."""
    return self.__textOutput.rstrip("\n")

  def inNonTerminal (self, nterm):
    """
    When the execution enters a non terminal execution block.
    Accumulate the value of the of the nterminal and increase the 
    indentation level.
    """
    self.__textOutput += self._makeIndent() + nterm + "\n"
    self.__indentationLevel += 1

  def outNonTerminal (self):
    """When the execution exits a non terminal execution block."""
    self.__indentationLevel -= 1

  def matchTerminal (self, term):
    """When the code matches a token --- terminal value."""
    self.__textOutput += self._makeIndent() + str(term) + "\n"

  def _makeIndent (self):
    """
    Calculates and returns the tabbed indent for a particular
    terminal/non-terminal discovery.
    """
    return " " * IDENT_VAL * self.__indentationLevel
