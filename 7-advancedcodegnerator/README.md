@Author: [Kartik Thapar](mailto:Kartik.Thapar@jhu.edu)

# Assignment 7 - Code Generator

# List of files:

	./kcodegen.py
	./kexception.py
	./kparser.py
	./kscanner.py
	./ksymboltable.py
	./ktoken.py
	./ktree.py
	./listeners
	./listeners/__init__.py
	./listeners/kdotlistener.py
	./listeners/kerrorlistener.py
	./listeners/klistener.py
	./listeners/ktextlistener.py
	./README.md
	./sc
	./visitors
	./visitors/__init__.py
	./visitors/abstract_syntax_tree
	./visitors/abstract_syntax_tree/__init__.py
	./visitors/abstract_syntax_tree/kdotvisitor.py
	./visitors/abstract_syntax_tree/kinterpretervisitor.py
	./visitors/abstract_syntax_tree/ktextvisitor.py
	./visitors/abstract_syntax_tree/kvisitor.py
	./visitors/symbol_table
	./visitors/symbol_table/__init__.py
	./visitors/symbol_table/kdotvisitor.py
	./visitors/symbol_table/kenvironment.py
	./visitors/symbol_table/ktextvisitor.py
	./visitors/symbol_table/kvisitor.py


# How to run:

`sc` is the binary that needs to be executed.

    > ./sc -s # for scanner
    # requires standard input

    > ./sc -s filename # for scanner
    # requires filename to be a valid file

    > ./sc -c # for parser; also generates CST
    # requires standard input

    > ./sc -c filename # for parser; also generates CST
    # requires filename to be valid

    > ./sc -cg # for parser; also generates graph output
    # requires standard input

    > ./sc -cg filename # for parser; also generates graph output
    # requires filename to be valid

    > ./sc -t # for parser; also generates symbol table text output
    # requires standard input

    > ./sc -t filename # for parser; also generates symbol table text output
    # requires filename to be valid

    > ./sc -tg # for parser; also generates symbol table graph output
    # requires standard input

    > ./sc -tg filename # for parser; also generates symbol table graph output
    # requires filename to be valid

    > ./sc -a # for parser; also generates abstract syntax tree text output
    # requires standard input

    > ./sc -a filename # for parser; also generates abstract syntax tree text output
    # requires filename to be valid

    > ./sc -ag # for parser; also generates abstract syntax tree graph output
    # requires standard input

    > ./sc -ag filename # for parser; also generates abstract syntax tree graph output
    # requires filename to be valid

    > ./sc -i # for interpreter;
    # requires standard input

    > ./sc -i filename # for interpreter;
    # requires filename to be valid

    > ./sc
    # requires standard input
    
    > ./sc filename
    # requires filename to be valid

# Architecture

**Target architecture: MIPS**

This is the advanced code generator which implements most of the functionality required in Assignment 7. Will qualify all test cases for Option A and B.