#!/usr/bin/python
#
# kcodegen.py
#
# Created by Kartik Thapar on 04/13/2013 at 23:16:53
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys
import ksymboltable as KST
import ktree as KAST
from visitors.symbol_table.kvisitor import Visitor as KSTVisitor # symbol table
from visitors.abstract_syntax_tree.kvisitor import Visitor as KASTVisitor # abstrac syntax tree

ERROR_STR = "error: "
SIZE_OF_INT = 4 # for MIPS, int main(){printf("Size of %d\n",sizeof(int));}
BASE_OFFSET = 0
width = "       "
registers = ['$t9','$t8','$t7','$t6','$t5','$t4','$t3','$t2','$t1','$t0','$3','$2'] # and so on....
zero = '$0'

# ---------------- CREATE STORAGE ------------------

class StorageVisitor (KSTVisitor):
  """Visitor class that operates on the symbol table data structure."""

  def __init__ (self):
    self.__size = SIZE_OF_INT
    self.__offset = BASE_OFFSET
    self.__totalSize = 0

  def totalSize (self):
    """Returns the total size of the variables"""
    return self.__totalSize

  def visitVariable (self, variable):
    logt ("__variable__")
    return variable.elementType().accept(self)
    
  def visitInteger (self, integer):
    logt ("__integer__")
    return SIZE_OF_INT # MIPS: 4B

  def visitArray (self, array):
    logt ("__array__")
    v = array.elementType().accept(self)
    size = array.length() * v # no of elements * elementType.size()
    array.setSize(size)
    return size

  def visitRecord (self, record):
    logt ("__record__")
    record.scope().accept(self)
    numberOfKeys = record.scope().numberOfKeys() # no of elements in a record
    elementType = record.scope().symbolTable().items()[0][1] # get the first element type
    size = elementType.accept(self) * numberOfKeys
    record.setSize(size)
    return numberOfKeys * elementType.accept(self)

  def visitScope (self, scope):
    logt ("__scope__")
    symbolTable = scope.symbolTable()
    offset = 0
    for identifer in sorted(symbolTable):
      element = symbolTable[identifer]
      if isinstance(element, KST.Type): # if it's a type, calculate size
        size = element.accept(self) # get size
        # self.__totalSize += size # keep adding to the total size
        element.setSize(size) # set size of element
      if isinstance(element, KST.Variable): # if it's a variable, set off set
        size = element.accept(self) # get the size of the variable
        self.__totalSize += size
        element.setOffset(offset) # set offset to current set
        offset = offset + size # increment offset by size

  def testStorage(self, scope):
    """Tests the storage of the code generator (types and variables)."""
    for (key,val) in sorted(scope.symbolTable().items()):
      if isinstance(val, KST.Variable): # if variable
        print "<Var: <name:%s>, <offset:%s>>" % (key, val.offset())
      elif isinstance(val, KST.Type):
        print "<Type: <name:%s>, <size:%s>>" % (key, val.size())


# -------------- CODE GENERATOR -------------


class AssemblyVisitor(KASTVisitor):
  """Code generator for SIMPLE."""

  def __init__(self, totalSize, scope):
    self.__totalSize = totalSize
    self.__scope = scope
    self.__nvars = 0 # no of variables in this
    self.__assembly = ""
    self.__available = [x for x in registers] # full of available registers at start
    self.__result = [] # cleared at start
    self.__counter = 1 # start counter for jump blocks
    
    self.__grow = 100
    self.__maxRange = 1000
    self.__lowRange = 0
    self.__counter = [x for x in range(self.__maxRange, self.__lowRange, -1)] # 1000 labels

  # --------------------- REGISTER FUNCTIONS -------------------------
  
  def _reloadAvailable (self):
    self.__available = [r for r in registers] # copy registers

  def _isRegisterAvailable (self):
    return False if len(self.__available) > 0 else True

  def _freeRegister (self, val):
    if val in self.__available:
      assert False
    else:
      self.__available.append(val)

  def _allocRegister (self):
    return self.__available.pop()

  def _reloadResult (self):
    self.__result = []

  def _isResult (self):
    return False if len(self.__result) > 0 else True

  def _loadResult (self, val):
    if val in self.__result:
      assert False
    else:
      self.__result.append(val)

  def _result (self):
    return self.__result.pop()

  def _allocCounter (self):
    if self.__counter:
      return self.__counter.pop()
    else:
      self._reloadCounter() # reload labels list
      self.__counter.pop() # get a label

  def _reloadCounter (self):
    self.__lowRange = self.__maxRange # set lower bounds
    self.__maxRange = self.__maxRange + self.__grow # upper bounds
    self.__counter = [x for x in range(self.__maxRange, self.__lowRange, -1)] # generate labels


  # -------------- ASSEMBLY ---------------

  def assembly (self):
    return self.__assembly

  def _mipsStoreInto (self, source, destination):
    self.__assembly += "%s sw      %s,0(%s)\n" % (width, source, destination)

  def _mipsLoadImmediate (self, value, destination):
    self.__assembly += "%s li      %s,%s\n" % (width, destination, value)

  def _mipsDereferenceLocation (self, register):
    self.__assembly += "%s lw      %s,0(%s)\n" % (width, register, register)

  def _mipsMove (self, source, destination):
    # self.__assembly += "%s add     %s,%s,%s\n" % (width, destination, source, zero)
    self.__assembly += "%s move    %s,%s\n" % (width, destination, source)

  def _mipsNOPS (self):
    self.__assembly += "%s nop\n" % (width)

  def createMetaData (self):
    """Create meta data for the assembly code."""
    self.__assembly += "# .author: Kartik Thapar\n"
    self.__assembly += "# .file:   SIMPLE code generator\n"

  def createProlog (self):
    """
    Creates MIPS prolog.
    http://msdn.microsoft.com/en-us/library/ms254248(v=vs.80).aspx
    """
    self.__assembly += "\n%s # PROLOG initialized\n\n" % (width)
    self.__assembly += "%s .file   1 \"no.c\"\n" % (width)
    self.__assembly += "%s .section .mdebug.abi32\n" % (width)
    self.__assembly += "%s .previous\n" % (width)
    self.__assembly += "%s .gnu_attribute 4, 1\n" % (width)
    self.__assembly += "%s .abicalls\n" % (width)
    self.__assembly += "\n"

    self.__assembly += "%s # storage allocation\n" % (width)
    self.__assembly += "vars:\n"
    cmt = "%s # .space total size\n" % ("\t"*4)
    self.__assembly += "%s .space  %s %s\n" % (width, self.__totalSize, cmt)

    self.__assembly += "%s .rdata\n" % (width)
    self.__assembly += "%s .align  2\n" % (width)
    
    self.__assembly += "$LC0:\n" # --------------------------------------------
    cmt = "%s # '%%d\\012\\000' used for __printf__" % ("\t"*3)
    self.__assembly += "%s .ascii  \"%%d\\012\\000\" %s\n" % (width, cmt)
    self.__assembly += "%s .align  2\n" % (width)
    self.__assembly += "$LC1:\n" # --------------------------------------------
    cmt = "%s # '%%d\\000' used for __scanf__" % ("\t"*3)
    self.__assembly += "%s .ascii  \"%%d\\000\" %s\n" % (width, cmt)
    self.__assembly += "%s .text\n" % (width)
    self.__assembly += "%s .align  2\n" % (width)
    self.__assembly += "%s .globl  main\n" % (width)
    self.__assembly += "%s .set    nomips16\n" % (width)
    self.__assembly += "%s .ent    main\n" % (width)
    self.__assembly += "%s .type   main, @function\n" % (width)
    self.__assembly += "main:\n" # --------------------------------------------
    cmt = "%s # use a stack of size: '32'" % ("\t"*3)
    self.__assembly += "%s .frame  $fp,32,$31 %s\n" % (width, cmt)
    self.__assembly += "%s .set    noreorder\n" % (width)
    self.__assembly += "%s .set    nomacro\n" % (width)
    self.__assembly += "\n" # just a break
    self.__assembly += "%s la      $s7, vars\n" % (width)
    self.__assembly += "%s addiu   $sp,$sp,-32\n" % (width)
    self.__assembly += "%s sw      $31,28($sp)\n" % (width) # store $ra on stack
    self.__assembly += "%s sw      $fp,24($sp)\n" % (width) # store $ra on stack
    self.__assembly += "%s move    $fp,$sp\n" % (width) # save $sp in $fp
    self.__assembly += "%s lui     $28,%%hi(__gnu_local_gp)\n" % (width)
    self.__assembly += "%s addiu   $28,$28,%%lo(__gnu_local_gp)\n" % (width)
    self.__assembly += "%s .cprestore  16\n" % (width)
    self.__assembly += "\n%s # --------------- __PROGRAM_START__ ---------------\n\n" % (width)
    
  def createEpilog (self):
    self.__assembly += "\n%s # --------------- __PROGRAM_END__ ---------------\n\n" % (width)
    self.__assembly += "%s # EPILOG initialized\n\n" % (width)
    self.__assembly += "%s move    $2,%s\n" % (width, zero) # push 0 into eval register
    self.__assembly += "%s move    $sp,$fp\n" % (width) # retrieve $sp in saved $fp
    self.__assembly += "%s lw      $31,28($sp)\n" % (width) # load $fp from stack
    self.__assembly += "%s lw      $fp,24($sp)\n" % (width) # load $fp from stack
    self.__assembly += "%s addiu   $sp,$sp,32\n" % (width) # retrieve stack
    self.__assembly += "%s j       $31\n" % (width) # retrieve stack
    self.__assembly += "%s nop\n" % (width)
    self.__assembly += "\n" # just a break # --------------------------------------------
    self.__assembly += "%s .set    macro\n" % (width)
    self.__assembly += "%s .set    reorder\n" % (width)
    self.__assembly += "%s .end    main\n" % (width)
    self.__assembly += "%s .size   main, .-main\n" % (width)

  def visitInstructions (self, instruction):
    instruction.accept(self) # for all instructions

  def visitAssignment (self, assignment):
    """
    Visits Assignment node in the AST.
    - First deal with the expression and load the value in a particular register.
    - Using MIPS store command, store the value in the destination register.
    """
    self.__assembly += "\n%s #  -> __ASSIGNMENT__ <- \n\n" % (width)
    assignment.expression().accept(self)
    expression = self._result() # get source
    assignment.location().accept(self)
    location = self._result() # get the location (destination)

    # --------- INTEGER ---------
    if isinstance(assignment.location().expressionType(), KST.Integer):
      c = assignment.location().expressionType().__class__
      self.__assembly += "\n%s # %s assignment initialized\n\n" % (width, c)
      if isinstance(assignment.expression(), KAST.Location):
        self._mipsDereferenceLocation(register=expression) # dereference expression (location)
      if expression in registers: # if it's a variable
        self._mipsStoreInto(source=expression, destination=location) # store value in destination
        self._freeRegister(expression) # free register
      else:
        reg = self._allocRegister() # if not, allocate a register
        self._mipsLoadImmediate(value=expression, destination=reg) # load value in register
        self._mipsStoreInto(source=reg, destination=location) # store value in destination regsiter
        self._freeRegister(reg) # free register
      self._freeRegister(location) # now that you've stored, free this location
   
    # --------- ARRAY|RECORD ---------

    else:
      c = assignment.location().expressionType().__class__
      self.__assembly += "\n%s # %s assignment initialized\n\n" % (width, c)
      size = assignment.location().expressionType().size()
      self._mipsMove(source=location, destination="$4") # for memcpy
      self._mipsMove(source=expression, destination="$5") # for memcpy
      self._freeRegister(location)
      self._freeRegister(expression)
      cmt = "%s # number of words to copy = '%s'" % ("\t"*4, size)
      self.__assembly += "%s li      $6,%s %s\n" % (width, size, cmt)
      reg = self._allocRegister() # get available register
      cmt = "%s # memcpy call to assign %s with addresses in $4 and $5" % ("\t", c)
      self.__assembly += "%s lw      %s,%%call16(memcpy)($28) %s\n" % (width, reg, cmt)
      self.__assembly += "%s nop\n" % (width)
      self.__assembly += "%s move    $25,%s\n" % (width, reg)
      self._freeRegister(reg) # we are done using
      self.__assembly += "%s jalr    $25\n" % (width)
      self.__assembly += "%s nop\n" % (width)
      cmt = "%s # __memcpy__ end" % ("\t"*3)
      self.__assembly += "%s lw      $28,16($fp) %s\n" % (width, cmt)
    
    if assignment.next(): # do next...
      assignment.next().accept(self)

  def visitIf (self, ifInstruction):
    self.__assembly += "\n%s # -> __IF__ <-\n\n" % (width)
    ifInstruction.condition().accept(self) # get assembly for condition
    condition = self._result() # get condition: either bool result or jump location
    if type(condition) is bool: # if we have a direct result----
      if condition: # if true, do true
        ifInstruction.trueInstruction().accept(self) # true
      else: # if false, check if false exists, do...
        if ifInstruction.falseInstruction():
          ifInstruction.falseInstruction().accept(self)
    else: # now we have a jump offset inside of condition
      ifInstruction.trueInstruction().accept(self) # write instructions for true
      jmpEnd = self._allocCounter() # get offset counter to jump to end
      self.__assembly += "%s b       $L%s\n" % (width, jmpEnd) # jump to the end of the if instruction
      self.__assembly += "%s nop\n" % (width)
      self.__assembly += "$L%s:\n" % (condition) # start of false instruction
      if ifInstruction.falseInstruction():
        ifInstruction.falseInstruction().accept(self)
      self.__assembly += "\n"
      self.__assembly += "$L%s:\n" % (jmpEnd) # start of false instruction
    if ifInstruction.next(): # NEXT...
      ifInstruction.next().accept(self)

  def visitRepeat (self, repeat):
    self.__assembly += "\n%s # -> __REPEAT__ <-\n\n" % (width)
    j = self._allocCounter() # get label
    self.__assembly += "$L%s:\n" % (j)
    repeat.condition().accept(self) # get condition
    condition = self._result() # get condition: either bool result or jump location    
    if condition is True: # without variables, will always be false
      jmp = self._allocCounter() # get a label to work on for repeat
      self.__assembly += "$L%s:\n" % (jmp) # create label as starting point
      repeat.instruction().accept(self) # do what repeat says...
      self.__assembly += "%s b       $L%s\n" % (width, jmp) # jump to start
      self.__assembly += "%s nop\n" % (width)
    else:
      outjmp = self._allocCounter()
      self.__assembly += "%s b       $L%s # issue?\n" % (width, outjmp)
      self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      self.__assembly += "$L%s:\n" % (condition)
      repeat.instruction().accept(self) #
      self.__assembly += "%s b       $L%s\n" % (width, j)
      self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      self.__assembly += "$L%s: # issue?\n" % (outjmp)

    if repeat.next(): # NEXT...
      repeat.next().accept(self)


  def visitCondition (self, condition):
    condition.leftExpression().accept(self) # --- LEFT ---
    left = self._result() # get left expression
    condition.rightExpression().accept(self) # --- RIGHT ---
    right = self._result() # get right expression
    relation = condition.relation() # get relation
    irel = None
    if isinstance(condition.leftExpression(), KAST.Location): # only possible when registers
      self._mipsDereferenceLocation(register=left) # dereference expression (location)
    if isinstance(condition.rightExpression(), KAST.Location):
      self._mipsDereferenceLocation(register=right) # dereference expression (location)
    
    if (left not in registers) and (right not in registers): # both are numbers, make a decision and send result
      if relation == "=":
        irel = (left == right)
      elif relation == "#":
        irel = (left != right)
      elif relation == "<":
        irel = (left < right)
      elif relation == ">":
        irel = (left > right)
      elif relation == "<=":
        irel = (left <= right)
      elif relation == ">=":
        irel = (left >= right)
      else:
        assert False
      self._loadResult(irel) # basically just get the result of the instruction
    else: # now if any of them are vars (so basically registers...)
      regLeft = left
      regRight = right
      if left not in registers: # if this is a num
        regLeft = self._allocRegister() # make a register for it
        self._mipsLoadImmediate(value=left, destination=regLeft) # load the number into this register
      elif right not in registers:
        regRight = self._allocRegister()
        self._mipsLoadImmediate(value=right, destination=regRight)

      # ------------ RELATION ------------
      if relation == "=":
        jmp = self._allocCounter() # get an offset number
        self._loadResult(jmp) # this has the offset for else instruction (for example)
        cmt = "%s # branches to '$L%s' if not equal" % ("\t"*3, jmp)
        self.__assembly += "%s bne     %s,%s,$L%s %s\n" % (width, regLeft, regRight, jmp, cmt)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      elif relation == "#":
        jmp = self._allocCounter() # get an offset number
        cmt = "%s # branches to '$L%s' if equal" % ("\t"*3, jmp)
        self._loadResult(jmp) # this has the offset for else instruction (for example)
        self.__assembly += "%s beq     %s,%s,$L%s %s\n" % (width, regLeft, regRight, jmp, cmt)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      elif relation == "<":
        jmp = self._allocCounter() # get an offset number
        self._loadResult(jmp) # this has the offset location
        cmt = "%s # set register '%s' to 1 if '%s' is less than '%s'" % ("\t"*3, regLeft, regLeft, regRight)
        self.__assembly += "%s slt     %s,%s,%s %s\n" % (width, regLeft, regLeft, regRight, cmt)
        cmt = "%s # branches to '$L%s' if '%s' is equal to zero" % ("\t"*3, jmp, regLeft)
        self.__assembly += "%s beq     %s,%s,$L%s %s\n" % (width, regLeft, zero, jmp, cmt)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      elif relation == ">":
        jmp = self._allocCounter() # get an offset number
        self._loadResult(jmp) # this has the offset location
        cmt = "%s # set register '%s' to 1 if '%s' is less than '%s'" % ("\t"*3, regLeft, regRight, regLeft)
        self.__assembly += "%s slt     %s,%s,%s %s\n" % (width, regLeft, regRight, regLeft, cmt)
        cmt = "%s # branches to '$L%s' if '%s' is equal to zero" % ("\t"*3, jmp, regLeft)
        self.__assembly += "%s beq     %s,%s,$L%s\n" % (width, regLeft, zero, jmp)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      elif relation == "<=":
        jmp = self._allocCounter() # get an offset number
        self._loadResult(jmp) # this has the offset location
        cmt = "%s # set register '%s' to 1 if '%s' is less than '%s'" % ("\t"*3, regLeft, regRight, regLeft)
        self.__assembly += "%s slt     %s,%s,%s %s\n" % (width, regLeft, regRight, regLeft, cmt)
        cmt = "%s # branches to '$L%s' if '%s' is not equal to zero" % ("\t"*3, jmp, regLeft)
        self.__assembly += "%s bne     %s,%s,$L%s %s\n" % (width, regLeft, zero, jmp, cmt)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      elif relation == ">=":
        jmp = self._allocCounter() # get an offset number
        self._loadResult(jmp) # this has the offset location
        cmt = "%s # set register '%s' to 1 if '%s' is less than '%s'" % ("\t"*3, regLeft, regLeft, regRight)
        self.__assembly += "%s slt     %s,%s,%s %s\n" % (width, regLeft, regLeft, regRight, cmt)
        cmt = "%s # branches to '$L%s' if '%s' is not equal to zero" % ("\t"*3, jmp, regLeft)
        self.__assembly += "%s bne     %s,%s,$L%s\n" % (width, regLeft, zero, jmp)
        self.__assembly += "%s nop\n" % (width) # m*****f****** NOP
      else:
        assert False
      self._freeRegister(regLeft)
      self._freeRegister(regRight)

  def visitWrite (self, write):
    logc("__write__")
    self.__assembly += "\n%s #  -> __WRITE__ <- \n" % (width)
    self.__assembly += "%s # printf initialized\n\n" % (width)
    reg = self._allocRegister() # get what's available
    cmt = "%s # loading .ascii from $LCO" % ("\t"*3)
    self.__assembly += "%s lui     %s,%%hi($LC0) %s\n" % (width, reg, cmt) # $LCO
    self.__assembly += "%s addiu   %s,%s,%%lo($LC0)\n" % (width, reg, reg)
    cmt = "%s # first argument to printf" % ("\t"*3 + "\t")
    self.__assembly += "%s move    $4,%s %s\n" % (width, reg, cmt)
    self._freeRegister(reg) # we are done using the register
    write.expression().accept(self)
    expression = self._result() # get value to print
    if isinstance(write.expression(), KAST.Location):
      self._mipsDereferenceLocation(expression) # get the value out of the location
    if expression in registers:
      self._mipsMove(source=expression, destination="$5")
      self._freeRegister(expression) # make it available
    else:
      self._mipsLoadImmediate(value=expression, destination="$5")
    reg = self._allocRegister() # get available register
    cmt = "%s # printf call to print contents of $5" % ("\t")
    self.__assembly += "%s lw      %s,%%call16(printf)($28) %s\n" % (width, reg, cmt)
    self.__assembly += "%s nop\n" % (width)
    self.__assembly += "%s move    $25,%s\n" % (width, reg)
    self._freeRegister(reg) # we are done using
    self.__assembly += "%s jalr    $25\n" % (width)
    self.__assembly += "%s nop\n" % (width)
    cmt = "%s # __printf__ end" % ("\t"*3)
    self.__assembly += "%s lw      $28,16($fp) %s\n" % (width, cmt)
    if write.next(): # NEXT...
      write.next().accept(self)

  def visitRead (self, read):
    logc("__read__")
    reg = self._allocRegister() # get what's available
    self.__assembly += "\n%s # readf initialized\n\n" % (width)
    self.__assembly += "%s lui     %s,%%hi($LC1)\n" % (width, reg) # $LCO
    self.__assembly += "%s addiu   %s,%s,%%lo($LC1)\n" % (width, reg, reg)
    self.__assembly += "%s move    $4,%s\n" % (width, reg)
    self._freeRegister(reg) # we are done using the register
    read.location().accept(self) # get the location
    location = self._result()
    self._mipsMove(source=location, destination="$5")
    reg = self._allocRegister()
    self.__assembly += "%s lw      %s,%%call16(__isoc99_scanf)($28)\n" % (width, reg)
    self.__assembly += "%s nop\n" % (width)
    self.__assembly += "%s move    $25,%s\n" % (width, reg)
    self._freeRegister(reg) # we are done using
    self.__assembly += "%s jalr    $25\n" % (width)
    self.__assembly += "%s nop\n" % (width)
    self.__assembly += "%s lw      $28,16($fp)\n" % (width)
    if read.next(): # NEXT...
      read.next().accept(self)

  def visitNumber (self, number):
    logc("__number__")
    n = number.constant().value() # get the value of the number node
    if n >= 2**31: # bounds checking
      reportError("constant value out of bounds at location '%d'." % (number.position()))
    self._loadResult(n) # simply push the constant on the result stack

  def visitVariable (self, variable):
    logc("__variable__")
    reg = self._allocRegister() # get something you want to store the variable in
    offset = variable.variable().offset() # get the offset of the variable
    cmt = "%s # applying offset '%s' for '%s'" % ("\t"*3, offset, variable.variable().name())
    self.__assembly += "%s add     %s,$s7,%s %s\n" % (width, reg, offset, cmt) # reach to the variable location
    self._loadResult(reg) # this register is occupied

  def visitIndex (self, index):
    index.location().accept(self)
    location = self._result() # get location
    index.expression().accept(self)
    expression = self._result() # get expression

    if expression in registers:
      if isinstance(index.expression(), KAST.Variable):
        self._mipsDereferenceLocation(register=expression)
      self.__assembly += "%s sll     %s,%s,2\n" % (width, expression, expression) # shift left
      self.__assembly += "%s add     %s,%s,%s\n" % (width, location, location, expression)
    else:
      if type(expression) is int and expression >= index.location().expressionType().length(): # bounds checking
        reportError("index out of bounds at location '%d'." % (index.position()))
      l = index.location().expressionType().length() # get length
      s = index.location().expressionType().size() # get size
      offset = (s/l) * expression # get the offset
      cmt = "%s # applying offset '%s'" % ("\t"*3, offset)
      self.__assembly += "%s add     %s,%s,%s %s\n" % (width, location, location, offset, cmt)
    self._loadResult(location)

  def visitField (self, field):
    field.location().accept(self) # do {location} location...
    location = self._result() # for field, pop this location and apply field to get further...
    variable = field.variable() # get field {field...}
    offset = variable.variable().offset() # get the offset of that thing
    cmt = "%s # applying offset '%s' for variable '%s'" % ("\t"*3, variable.variable().name(), offset)
    self.__assembly += "%s add     %s,%s,%s %s\n" % (width, location, location, offset, cmt)
    self._loadResult(location)

  def visitBinary (self, binary):
    logc("__binary__")
    binary.leftExpression().accept(self)
    left = self._result()
    if isinstance(binary.leftExpression(), KAST.Location):
      self._mipsDereferenceLocation(register=left) # if location, dereference
    binary.rightExpression().accept(self)
    right = self._result()
    if isinstance(binary.rightExpression(), KAST.Location):
      self._mipsDereferenceLocation(register=right) # if location, dereference

    immediateL = False if left in registers else True
    immediateR = False if right in registers else True
    reg = None
    operator = binary.operator() # get the operator
    
    if left in registers: # if a variable?
      reg = left
      
      if operator == "+": # ------------- ADDITION --------------
        if immediateR:
          cmt = "%s # add immediate %s to register '%s'" % ("\t"*3, right, left)
          self.__assembly += "%s addiu   %s,%s,%s %s\n" % (width, reg, reg, right, cmt)
        else:
          cmt = "%s # addition variables in registers '%s' and '%s'" % ("\t"*3, left, right)
          self.__assembly += "%s addu    %s,%s,%s %s\n" % (width, reg, reg, right, cmt)
          self._freeRegister(right)
      
      elif operator == "-": # ------------- SUB --------------
        if immediateR:
          cmt = "%s # subtracting %s from register '%s" % ("\t"*3, right, left)
          self.__assembly += "%s addiu   %s,%s,-%s %s\n" % (width, reg, reg, right, cmt)
        else:
          cmt = "%s # subtracting variable in register '%s' from '%s'" % ("\t"*3, right, left)
          self.__assembly += "%s subu    %s,%s,%s %s\n" % (width, reg, reg, right, cmt)
          self._freeRegister(right)
      
      elif operator == "*": # ------------- MULTIPLICATION --------------
        if immediateR: # you have the value of this thing
          reg2 = self._allocRegister() # get a register
          cmt = "%s # loading int in register '%s'" % ("\t"*3, reg2)
          self.__assembly += "%s li      %s,%s %s\n" % (width, reg2, right, cmt)
          cmt = "%s # multiplying values in registers '%s' and '%s'" % ("\t"*3, reg, reg2)
          self.__assembly += "%s mult    %s,%s %s\n" % (width, reg, reg2, cmt)
          cmt = "%s # moving result from $LO to register '%s'" % ("\t"*3, reg)
          self.__assembly += "%s mflo    %s %s\n" % (width, reg, cmt)
          self._freeRegister(reg2) # make it available
        else:
          cmt = "%s # multiplying values in registers '%s' and '%s'" % ("\t"*3, left, right)
          self.__assembly += "%s mult    %s,%s %s\n" % (width, reg, right, cmt)
          cmt = "%s # moving result from $LO to register '%s'" % ("\t"*3, reg)
          self.__assembly += "%s mflo    %s %s\n" % (width, reg, cmt)
          self._freeRegister(right)

      elif operator == "DIV" or operator == "MOD": # -------------- DIVISION / MODULUS --------------
        if immediateR:
          reg2 = self._allocRegister() # get a register
          self._mipsLoadImmediate(value=right, destination=reg2)
          self.__assembly += "%s div     %s,%s\n" % (width, reg, reg2) # div $left/$right
          if operator == "DIV":
            self.__assembly += "%s mflo    %s\n" % (width, reg)
          else:
            self.__assembly += "%s mfhi    %s\n" % (width, reg)
          self._freeRegister(reg2)
        else:
          self.__assembly += "%s div     %s,%s\n" % (width, reg, right) # div $left/$right
          if operator == "DIV": # for divide
            self.__assembly += "%s mflo    %s\n" % (width, reg)
          else: # for modulus
            self.__assembly += "%s mfhi    %s\n" % (width, reg)
          self._freeRegister(right)

    elif right in registers: # if a variable?
      reg = right
      
      if operator == "+": # ------------- ADDITION --------------
        if immediateL:
          self.__assembly += "%s addiu   %s,%s,%s\n" % (width, reg, reg, left)
        else:
          self.__assembly += "%s addu    %s,%s,%s\n" % (width, reg, reg, left)
          self._freeRegister(left)
      
      elif operator == "-": # ------------- SUB --------------
        if immediateL:
          reg2 = self._allocRegister() # get register to hold immediate value
          self._mipsLoadImmediate(value=left, destination=reg2)
          self.__assembly += "%s subu    %s,%s,%s\n" % (width, reg, reg2, reg)
          self._freeRegister(reg2) # free register
        else:
          self.__assembly += "%s subu    %s,%s,%s\n" % (width, reg, left, reg)
          self._freeRegister(left)
      
      elif operator == "*": # ------------ MULTIPLY ------------
        if immediateL: # you have the value of this thing
          reg2 = self._allocRegister() # get a register
          self.__assembly += "%s li      %s,%s\n" % (width, reg2, left)
          self.__assembly += "%s mult    %s,%s\n" % (width, reg, reg2)
          self.__assembly += "%s mflo    %s\n" % (width, reg)
          self._freeRegister(reg2) # make it available
        else:
          self.__assembly += "%s mult    %s,%s\n" % (width, reg, left)
          self.__assembly += "%s mflo    %s\n" % (width, reg)
          self._freeRegister(left)
      
      elif operator == "DIV" or operator == "MOD": # -------------- DIVISION --------------
        if immediateL:
          reg2 = self._allocRegister() # get a register
          self._mipsLoadImmediate(value=left, destination=reg2)
          self.__assembly += "%s div     %s,%s\n" % (width, reg2, reg) # div $left/$right
          if operator == "DIV":
            self.__assembly += "%s mflo    %s\n" % (width, reg)
          else:
            self.__assembly += "%s mfhi    %s\n" % (width, reg)
          self._freeRegister(reg2) # free this
        else:
          self.__assembly += "%s div     %s,%s\n" % (width, left, reg) # div $left/$right
          if operator == "DIV":
            self.__assembly += "%s mflo    %s\n" % (width, reg)
          else:
            self.__assembly += "%s mfhi    %s\n" % (width, reg)
          self._freeRegister(left)
      else:
        assert False
    else:
      assert False
    self._loadResult(reg)

def logt(v):
  # print v
  pass

def logc (v):
  # print v
  pass

def reportError (err):
  sys.stderr.write(ERROR_STR + err + "\n")
  exit()
