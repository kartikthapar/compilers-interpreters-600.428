#!/usr/bin/python
#
# kvisitor.py
#
# Created by Kartik Thapar on 03/26/2013 at 00:54:51
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

class Visitor(object):
  """Visitor class that operates on the abstract syntax tree data structure."""
  
  def visitInstructions (self, instruction):
    pass

  def visitAssignment (self, assignment):
    pass

  def visitIf (self, ifInstruction):
    pass

  def visitRepeat (self, repeat):
    pass

  def visitRead (self, read):
    pass

  def visitWrite (self, write):
    pass

  def visitNumber (self, numeber):
    pass

  def visitVariable (self, variable):
    pass

  def visitIndex (self, index):
    pass

  def visitField (self, field):
    pass

  def visitBinary (self, binary):
    pass

  def visitCondition (self, condition):
    pass

  def visitProcedureCall (self, call):
    pass

  def visitFunctionCall (self, call):
    pass


