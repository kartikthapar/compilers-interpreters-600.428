#!/usr/bin/python
#
# ktextvisitor.py
#
# Created by Kartik Thapar on 04/01/2013 at 04:20:57
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class TextVisitor (Visitor)
"""

from kvisitor import Visitor
from visitors.symbol_table import kvisitor as KV, ktextvisitor as KTV

IDENT_VAL = 2

class TextVisitor (Visitor):

  def __init__ (self):
    self.__indentationLevel = 0 # in_out
    self.__textOutput = ""

  def textOutput (self):
    """Method to retrieve the entire text output for the AST."""
    return "instructions =>\n" + self.__textOutput

  def _in (self):
    """Enter a node."""
    self.__indentationLevel += 1

  def _out (self):
    """Exit a node."""
    self.__indentationLevel -= 1

  def _makeIndent (self):
    """Calculates and returns the tabbed indent for a particular ..."""
    return " " * IDENT_VAL * self.__indentationLevel
  
  def _makeText (self, value):
    """Append to the output."""
    self.__textOutput += self._makeIndent() + value + "\n"

  def visitInstructions (instruction):
    """Instruction node"""
    instruction.accept(self)

  def visitAssignment (self, assignment):
    """Assignment node"""
    self._in()
    self._makeText("Assign:") # start
    self._makeText("location =>") # get location --- int var, array, record
    assignment.location().accept(self)
    self._makeText("expression =>") # get expression value to assign to location
    assignment.expression().accept(self)
    self._out()

    # do next...
    assignment.next().accept(self) if assignment.next() else None

  def visitIf (self, ifInstruction):
    """If node"""
    self._in()
    self._makeText("If:") # start
    self._makeText("condition =>") # get condition for if
    ifInstruction.condition().accept(self)
    self._makeText("true =>") # true instruction
    ifInstruction.trueInstruction().accept(self)
    if ifInstruction.falseInstruction(): # if false instruction exits, get false instruction
      self._makeText("false =>")
      ifInstruction.falseInstruction().accept(self)
    self._out()

    # do next...
    ifInstruction.next().accept(self) if ifInstruction.next() else None

  def visitRepeat (self, repeat):
    """Repeat node"""
    self._in()
    self._makeText("Repeat:") # start
    self._makeText("condition =>") # get condition
    repeat.condition().accept(self)
    self._makeText("instructions =>") # get instruction
    repeat.instruction().accept(self)
    self._out()

    # do next...
    repeat.next().accept(self) if repeat.next() else None

  def visitRead (self, read):
    """Read node"""
    self._in()
    self._makeText("Read:") # start
    self._makeText("location =>") # get location where read will store eventually
    read.location().accept(self)
    self._out()

    # do next...
    read.next().accept(self) if read.next() else None

  def visitWrite (self, write):
    """Write node"""
    self._in()
    self._makeText("Write:") # start
    self._makeText("expression =>") # find the value of the expression
    write.expression().accept(self)
    self._out()

    # do next...
    write.next().accept(self) if write.next() else None

  def visitNumber (self, number):
    """Number node"""
    self._in()
    self._makeText("Number:") # start

    # get constants ---------------------------------
    self._makeText("value =>")
    tv = KTV.TextVisitor() # get a reference to the text visitor of the symbol table
    tv.setIndentCustom(self.__indentationLevel) # supply indentation to it
    number.constant().accept(tv)
    self.__textOutput += tv.textOutput()
    self._out()

  def visitVariable (self, variable):
    """Variable node"""
    self._in()
    self._makeText("Variable:")
    self._makeText("variable =>")

    # get variables
    tv = KTV.TextVisitor() # get a reference to the text visitor of the symbol table
    tv.setIndentCustom(self.__indentationLevel) # supply indentation to it
    variable.variable().accept(tv) # do variables
    self.__textOutput += tv.textOutput() # get output from text visitor of 'symbol table'
    self._out()

  def visitIndex (self, index):
    """Index node"""
    self._in()
    self._makeText("Index:") # start
    self._makeText("location =>") # get array location
    index.location().accept(self)
    self._makeText("expression =>") # get index off of array
    index.expression().accept(self)
    self._out()

  def visitField (self, field):
    """Field node"""
    self._in()
    self._makeText("Field:") # start
    self._makeText("location =>") # get location
    field.location().accept(self)
    self._makeText("variable =>") # get variable out of location
    field.variable().accept(self)
    self._out()

  def visitBinary (self, binary):
    """Binary node"""
    self._in()
    self._makeText("Binary (%s):" % (binary.operator())) # start
    self._makeText("left =>") # do left expression
    binary.leftExpression().accept(self)
    self._makeText("right =>") # do right expression
    binary.rightExpression().accept(self)
    self._out()

  def visitCondition (self, condition):
    """Condition node"""
    self._in()
    self._makeText("Condition (%s):" % (condition.relation())) # start
    self._makeText("left =>") # do left expression
    condition.leftExpression().accept(self)
    self._makeText("right =>") # do right expression
    condition.rightExpression().accept(self)
    self._out()

  def visitProcedureCall (self, call):
    """ProcedureCall node."""
    self._in()
    self._makeText("ProcedureCall:")
    procedure = call.procedure() # get the procedure
    if procedure.hasScope(): # ---------- if SCOPE ----------
      self._makeText("scope =>")
      tv = KTV.TextVisitor()
      tv.setIndentCustom(self.__indentationLevel)
      procedure.scope().accept(tv)
      self.__textOutput += tv.textOutput()
    if procedure.hasInstruction():
      self._makeText("instructions =>")
      procedure.instruction().accept(self)
    self._out()
    call.next().accept(self) if call.next() else None

  def visitFunctionCall (self, call):
    """FunctionCall node."""
    self._in()
    self._makeText("FunctionCall:")
    procedure = call.procedure() # get the procedure
    if procedure.hasScope(): # ---------- if SCOPE ----------
      self._makeText("scope =>")
      tv = KTV.TextVisitor()
      tv.setIndentCustom(self.__indentationLevel)
      procedure.scope().accept(tv)
      self.__textOutput += tv.textOutput()
    if procedure.hasInstruction():
      self._makeText("instructions =>")
      procedure.instruction().accept(self)
    if procedure.hasReturnValue(): # this will never be the case ---> function call
      self._makeText("return =>")
      procedure.returnValue().accept(self)
    self._out()
    call.next().accept(self) if call.next() else None
