#!/usr/bin/python
#
# ktree.py
#
# Created by Kartik Thapar on 03/24/2013 at 18:27:27
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import ksymboltable as KST

relators = ['=', '#', '<', '>', '<=', '>='] # see assignment 4
operators = ['+', '-', '*', 'DIV', 'MOD'] # see assignment 4
negators = {
  "=" : "#",
  "#" : "=",
  "<" : ">=",
  ">=" : "<",
  ">" : "<=",
  "<=" : ">"
}

# ------------ BASE NODE -------------

class Node:
  """Base class for all symbol table entries."""

  def __init__ (self, position):
    self._position = position # get position for errors in interpreter, etc.

  def position (self):
    """Returns position of the AST node."""
    return self._position

  def accept (self, visitor):
    pass

# ----------- INSTRUCTIONS ------------

class Instruction (Node):
  """
  Instructions = Instruction | Instruction Instructions .
  Instruction = Assign | If | Repeat | Read | Write .
  Instruction = Assign | If | Repeat | Read | Write | Call .
  """

  def __init__ (self, position):
    Node.__init__(self, position)
    self.__next = None # for ordering

  def setNext (self, instruction):
    """For the calling object, set the instruction as the next instruction in the sequence."""
    assert isinstance(instruction, Instruction) or instruction is None
    self.__next = instruction

  def next (self):
    """For the calling instruction, return the next instruction in the instruction sequence."""
    return self.__next

# ------------------- CALL -------------------

class ProcedureCall (Instruction):

  def __init__ (self, procedure, actuals, position):
    Instruction.__init__(self, position)
    self.__procedure = procedure
    self.__actuals = actuals # list of actuals

  def accept (self, visitor):
    return visitor.visitProcedureCall(self)

  def procedure (self):
    return self.__procedure

  def hasActuals (self):
    """Returns true if the function has actuals."""
    return True if self.__actuals else False
  
  def actuals (self):
    """Returns the actuals of this call."""
    return self.__actuals

  def __str__ (self):
    return "<ProcedureCall: <procedure:%s>, <actuals:%s>, <position:%s>>" % \
           (self.__procedure, self.__actuals, self._position)

class Assign (Instruction):
  """Assign = Location Expression ."""

  def __init__ (self, location, expression, position):
    Instruction.__init__(self, position)
    assert isinstance(location, Location) or location is None
    assert isinstance(expression, Expression) or expression is None
    if isinstance(expression, Expression) and isinstance(location, Location):
      assert expression.expressionType() is location.expressionType()
    self.__location = location
    self.__expression = expression

  def accept (self, visitor):
    return visitor.visitAssignment(self)

  def location (self):
    """Returns location of the variable to which the assignment is made."""
    return self.__location

  def expression (self):
    """Returns expression that is to be evaluated for the location."""
    return self.__expression

  def __str__ (self):
    return "<Assign: <location:%s>, <expression:%s>, <position:%s>>" % \
            (self.__location, self.__expression, self._position)

class If (Instruction):
  """If = Condition Instructions_true [Instructions_false] ."""

  def __init__ (self, condition, trueInstruction, falseInstruction, position):
    Instruction.__init__(self, position)
    assert isinstance(condition, Condition) or condition is None
    assert isinstance(trueInstruction, Instruction) or instruction is None
    assert falseInstruction is None or isinstance(falseInstruction, Instruction) # conditional [_false]
    self.__condition = condition
    self.__trueInstruction = trueInstruction
    self.__falseInstruction = falseInstruction

  def accept (self, visitor):
    return visitor.visitIf(self)

  def condition (self):
    """Returns the condition in case of IF, REPEAT, WHILE."""
    return self.__condition

  def trueInstruction (self):
    """True instruction for IF."""
    return self.__trueInstruction

  def falseInstruction (self):
    """False instruction sequence for IF."""
    return self.__falseInstruction

  def __str__ (self):
    return "<If: <condition:%s>, <true:%s>, <false:%s>, <position:%s>>" % \
            (self.__condition, self.__trueInstruction, self.__falseInstruction, self._position)

class Repeat (Instruction):
  """Repeat = Condition Instructions ."""

  def __init__ (self, condition, instruction, position):
    Instruction.__init__(self, position)
    assert isinstance(condition, Condition) or condition is None
    assert isinstance(instruction, Instruction) or instruction is None
    self.__condition = condition
    self.__instruction = instruction

  def accept (self, visitor):
    return visitor.visitRepeat(self)

  def condition (self):
    """Returns condition for repeat statement."""
    return self.__condition

  def instruction (self):
    """Returns instruction for repeat call."""
    return self.__instruction

  def __str__ (self):
    return "<Repeat: <condition:%s>, <instruction:%s>, <position:%s>>" % \
            (self.__condition, self.__instruction, self._position)

class Read (Instruction):
  """Read = Location ."""

  def __init__ (self, location, position):
    Instruction.__init__(self, position)
    assert isinstance(location, Location) or location is None
    if location:
      assert isinstance(location.expressionType(), KST.Integer)
    self.__location = location

  def location (self):
    """Returns the location for the variable to which read writes to."""
    return self.__location

  def accept (self, visitor):
    return visitor.visitRead(self)

  def __str__ (self):
    return "<Read: <location:%s>, <position:%s>>" % \
            (self.__location, self._position)

class Write (Instruction):
  """Write = Expression ."""

  def __init__ (self, expression, position):
    Instruction.__init__(self, position)
    assert isinstance(expression, Expression) or expression is None
    if expression:
      assert isinstance(expression.expressionType(), KST.Integer)
    self.__expression = expression

  def accept (self, visitor):
    return visitor.visitWrite(self)

  def expression(self):
    """Returns the expression to be evaluated and printed to stdout."""
    return self.__expression

  def __str__ (self):
    return "<Write: <expression:%s> <position:%s>>" % (self.__expression, self._position)

# ----------- EXPRESSIONS ------------

class Expression (Node):
  """Expression = Number | Location | Binary ."""

  def __init__ (self, position):
    Node.__init__(self, position)
    self._expressionType = None

  def expressionType (self):
    """Returns the type of the expression"""
    return self._expressionType

class Number (Expression):
  """Number ."""

  def __init__ (self, constant, position):
    Expression.__init__(self, position)
    assert isinstance (constant, KST.Constant)
    self.__constant = constant # entry is a constant
    self._expressionType = constant.elementType()

  def constant (self):
    """Returns the entry (which is of type Constant)."""
    return self.__constant

  def accept (self, visitor):
    return visitor.visitNumber(self)

  def __str__ (self):
    return "<Number: <constant:%s>, <type:%s>, <position:%s>>" % \
            (self.__constant, self._expressionType, self._position)

class Location (Expression):
  """Location = Variable | Index | Field ."""

  def __init__ (self, position):
    Expression.__init__(self, position)

class Variable (Location):
  """Variable ."""

  def __init__ (self, variable, position):
    Location.__init__(self, position)
    assert isinstance(variable, KST.Variable)
    self.__variable = variable
    self._expressionType = variable.elementType()

  def accept (self, visitor):
    return visitor.visitVariable(self)

  def variable (self):
    """Returns the variable."""
    return self.__variable

  def __str__ (self):
    return "<Variable: <variable:%s>, <type: %s>, <position:%s>>" % \
            (self.__variable, self._expressionType, self._position)

class Index (Location):
  """Index = Location Expression ."""

  def __init__ (self, location, expression, position):
    Location.__init__(self, position)
    assert isinstance(location, Location)
    assert isinstance(expression, Expression)
    self.__location = location
    self.__expression = expression
    self._expressionType = location.expressionType().elementType() # use location; do not reference self.__location

  def accept (self, visitor):
    return visitor.visitIndex(self)

  def location (self):
    """Returns the location."""
    return self.__location

  def expression (self):
    """Returns the expression that needs to be evaluated for the location."""
    return self.__expression

  def __str__ (self):
    return "<Index: <location:%s>, <expression:%s>, <type:%s>, <position:%s>>" % \
            (self.__location, self.__expression, self._expressionType, self._position)

class Field (Location):
  """Field = Location Variable ."""

  def __init__ (self, location, variable, position):
    Location.__init__(self, position)
    assert isinstance(location, Location)
    assert isinstance(variable, Variable)
    self.__location = location
    self.__variable = variable
    self._expressionType = variable.expressionType()

  def accept (self, visitor):
    return visitor.visitField(self)

  def location (self):
    """Returns the location associated with the record."""
    return self.__location

  def variable (self):
    """Returns the variable that needs to be retrieved off of the record."""
    return self.__variable

  def __str__ (self):
    return "<Field: <location:%s>, <variable:%s>, <type:%s>, <position:%s>>" % \
            (self.__location, self.__variable, self._expressionType, self._position)

class Binary (Expression):
  """Binary = Operator Expression_left Expression_right ."""

  def __init__ (self, operator, leftExpression, rightExpression, position):
    Expression.__init__(self, position)
    assert operator in operators
    assert isinstance(leftExpression, Expression)
    assert isinstance(rightExpression, Expression)
    self.__operator = operator
    self.__leftExpression = leftExpression
    self.__rightExpression = rightExpression
    self._expressionType = rightExpression.expressionType() # assign any

  def accept (self, visitor):
    return visitor.visitBinary(self)

  def operator (self):
    """Returns the operator assocated with the binary node (operation)."""
    return self.__operator

  def leftExpression (self):
    """Returns the left expression associated with the operation."""
    return self.__leftExpression

  def rightExpression (self):
    """Returns the right expression associated with the operation."""
    return self.__rightExpression

  def __str__ (self):
    return "<Binary: <operation:%s>, <left:%s>, <right:%s>, <type:%s>, <position:%s>>" % \
        (self.__operator,  self.__leftExpression, self.__rightExpression, self._expressionType, self._position)

class FunctionCall (Expression, Instruction):
  def __init__ (self, procedure, actuals, position):
    Expression.__init__(self, position)
    Instruction.__init__(self, position)
    self.__procedure = procedure
    self.__actuals = actuals
    self._expressionType = procedure.returnType()

  def accept (self, visitor):
    return visitor.visitFunctionCall(self)

  def procedure (self):
    return self.__procedure
  def hasActuals (self):
    return True if self.__actuals else False
  def actuals (self):
    return self.__actuals
  def __str__ (self):
    return "<FunctionCall: <procedure:%s>, <actuals:%s>, <type:%s>, <position:%s>>" % \
           (self.__procedure, self.__actuals, self._expressionType, self._position)

# ----------- CONDITIONS ------------

class Condition (Node):
  """
  Condition = Relation Expression_left Expression_right .
  The relation in a condition can be one of [=, #, <, >, <=, >=].
  """

  def __init__ (self, relation, leftExpression, rightExpression, position):
    Node.__init__(self, position)
    assert relation in relators
    assert isinstance(leftExpression, Expression) or leftExpression is None
    assert isinstance(rightExpression, Expression) or rightExpression is None
    self.__relation = relation
    self.__leftExpression = leftExpression
    self.__rightExpression = rightExpression

  def relation (self):
    """Returns the condition associated with the condition."""
    return self.__relation

  def leftExpression (self):
    """Returns the left expression associated with the condition."""
    return self.__leftExpression

  def rightExpression (self):
    """Returns the left expression associated with the condition."""
    return self.__rightExpression

  def accept (self, visitor):
    return visitor.visitCondition(self)

  def __str__ (self):
    return "<Condition: <relation:%s>, <left:%s>, <right:%s>, <position:%s>>" %\
            (self.__relation, self.__leftExpression, self.__rightExpression, self._position)

# -----------WHILE------------

def While (condition, instruction, position):
  """
  While is transformed into a 'Repeat' inside an 'If' statement.
  IF condition THEN
    REPEAT instructions UNTIL not(CONDITION) END
  END
  """
  assert isinstance(condition, Condition) or condition is None
  assert isinstance(instruction, Instruction) or instruction is None
  if condition and instruction:
    relation = condition.relation() # get relation
    relation = negators[relation] # find negation of that relation

    leftExpression = condition.leftExpression() # get left expression
    rightExpression = condition.rightExpression() # get right expression

    negatedCondition = Condition(relation, leftExpression, rightExpression, position) # create a negated condition
    repeatInstruction = Repeat(negatedCondition, instruction, position) # create repeat with negated condition
    transform = If(condition, repeatInstruction, None, position) # create If instruction with the condition and repeat call
    return transform
