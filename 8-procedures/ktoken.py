#!/usr/bin/python
#
# ktoken.py
#
# Created by Kartik Thapar on 02/12/2013 at 22:34:00
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import types as PType

class Token:
  """Token is defined by the following properties: start position, end position, kind, value"""

  def __init__(self, kind, startPosition, endPosition, value = None):
    assert type(kind) is PType.StringType
    assert type(startPosition) is PType.IntType
    assert type(endPosition) is PType.IntType
    assert \
    (type(value) is PType.StringType and (kind is "identifier" or kind is "symbol" or kind is "keyword" )) \
    or (type(value) is PType.IntType or type(value) is PType.LongType and kind is "integer")\
    or (type(value) is PType.NoneType and kind is "eof")
    self.__kind = kind # kind?
    self.__startPosition = startPosition # start position
    self.__endPosition = endPosition # end position
    self.__value = value # do all tokens have a value? only identifiers & integers
  
  def __str__ (self):
    #http://stackoverflow.com/questions/3691101/what-is-the-purpose-of-str-and-repr-in-python
    rs = ""
    if self.kind() in ["keyword", "symbol"]:
        rs = "%s@(%s, %s)" % (self.value(), self.startPosition(), self.endPosition())
    elif self.kind() in ["identifier", "integer"]:
        rs = "%s<%s>@(%s, %s)" % (self.kind(), self.value(), self.startPosition(), self.endPosition())
    elif self.kind() in ["eof"]:
        rs = "%s@(%s, %s)" % (self.kind(), self.startPosition(), self.endPosition())
    return rs

  def kind (self):
    """Returns the kind of token"""
    return self.__kind

  def startPosition (self):
    """Returns the start position of token"""
    return self.__startPosition

  def endPosition (self):
    """Returns the end position of token"""
    return self.__endPosition

  def value (self):
    """Returns the value of the token"""
    return self.__value

