#!/usr/bin/python
#
# kinterpretervisitor.py
#
# Created by Kartik Thapar on 04/09/2013 at 01:02:56
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys
from copy import deepcopy
from kvisitor import Visitor
from visitors.symbol_table import kenvironment as KENV

ERROR_STR = "error: "

class InterpreterVisitor(Visitor):
  """Visitor class that operates on the abstract syntax tree data structure."""

  def __init__ (self, environment):
    self.__env = environment # env to retrieve variables from and save them back to this place, pointers
    self.__stack = []

  def visitInstructions (self, instruction):
    instruction.accept(self) # for all instructions

  def visitAssignment (self, assignment):
    assignment.location().accept(self) # get location, pointer to address where to store
    assignment.expression().accept(self) # what to store
    expression = self.__stack.pop() # this can be a INTEGER type, ARRAY type
    location = self.__stack.pop() # points to the variable
    expression = expression.boxValue() if isinstance(expression, KENV.IntegerBox) else expression
    location.setValueForBox(deepcopy(expression)) # for integer variables/arrays/records...
    assert not self.__stack # always check this; the stack must be empty after every instruction
    if assignment.next(): # NEXT...
      assignment.next().accept(self)

  def visitIf (self, ifInstruction):
    ifInstruction.condition().accept(self) # get condition
    condition = self.__stack.pop()
    if condition: # if true, do true
      ifInstruction.trueInstruction().accept(self) # true
    else: # if false, check if false exists, do...
      if ifInstruction.falseInstruction():
        ifInstruction.falseInstruction().accept(self)
    assert not self.__stack
    if ifInstruction.next(): # NEXT...
      ifInstruction.next().accept(self)

  def visitRepeat (self, repeat):
    repeat.condition().accept(self) # get condition
    condition = self.__stack.pop()
    while condition is False:
      repeat.instruction().accept(self) # do instruction
      repeat.condition().accept(self) # get condition
      condition = self.__stack.pop()
    assert not self.__stack
    if repeat.next(): # NEXT...
      repeat.next().accept(self)

  def visitRead (self, read):
    read.location().accept(self) # get location
    location = self.__stack.pop()
    value = None
    try:
      value = sys.stdin.readline() # get input from stdin
      value = int(value)
      location.setValueForBox(value)
    except ValueError, e:
      if not value:
        location.setValueForBox(0) # set default zero, if ctrl+D
      else:
        reportError("expected an integer value at location %d." % (read.position()))
    except KeyboardInterrupt, e:
      exit()
    assert not self.__stack
    if read.next(): # NEXT...
      read.next().accept(self)

  def visitWrite (self, write):
    write.expression().accept(self) # solve expression
    content = self.__stack.pop() # get the value off of stack
    print content if type(content) is int else content.boxValue() # content is either int or int variable
    assert not self.__stack
    if write.next(): # NEXT...
      write.next().accept(self)

  def visitNumber (self, number):
    n = number.constant().value()
    self.__stack.append(n) # push number on stack

  def visitVariable (self, variable):
    var = self.__env[variable.variable().name()] # you simply get the location from the environment
    self.__stack.append(var) # push it on the stack

  def visitIndex (self, index):
    index.location().accept(self)
    index.expression().accept(self)
    expression = self.__stack.pop() # pop expression first
    expression = expression.boxValue() if isinstance(expression, KENV.IntegerBox) else expression
    location = self.__stack.pop() # pop location of array variable
    if location.canIndexForIndex(expression):
      self.__stack.append(location.boxAtIndex(expression)) # push the new box on stack
    else:
      reportError("index out of bounds at location '%d'." % (index.position()))

  def visitField (self, field):
    field.location().accept(self) # do {location} location...
    location = self.__stack.pop() # for field, pop this location and apply field to get further...
    variable = field.variable() # get field {field...}
    variable = location.retrieveVariable(variable.variable().name())
    self.__stack.append(variable)

  def visitBinary (self, binary):
    binary.leftExpression().accept(self)
    binary.rightExpression().accept(self)
    right = self.__stack.pop() # get right expression
    left = self.__stack.pop() # get left expression
    right = right.boxValue() if isinstance(right, KENV.IntegerBox) else right # this is either int or int var
    left = left.boxValue() if isinstance(left, KENV.IntegerBox) else left
    operator = binary.operator() # get operator
    try:
      if operator == "+":
        self.__stack.append(left + right)
      elif operator == "-":
        self.__stack.append(left - right)
      elif operator == "*":
        self.__stack.append(left * right)
      elif operator == "DIV":
        self.__stack.append(left / right)
      elif operator == "MOD":
        self.__stack.append(left % right)
    except ZeroDivisionError, e:
      reportError("division or modulo by zero is not allowed; at location %d." % (binary.position()))
      
  def visitCondition (self, condition):
    condition.leftExpression().accept(self)
    condition.rightExpression().accept(self)
    right = self.__stack.pop() # get right expression
    left = self.__stack.pop() # get left expression
    relation = condition.relation() # get relation
    right = right.boxValue() if isinstance(right, KENV.IntegerBox) else right # this is either int or int variable
    left = left.boxValue() if isinstance(left, KENV.IntegerBox) else left
    irel = None
    if relation == "=":
      irel = (left == right)
    elif relation == "#":
      irel = (left != right)
    elif relation == "<":
      irel = (left < right)
    elif relation == ">":
      irel = (left > right)
    elif relation == "<=":
      irel = (left <= right)
    elif relation == ">=":
      irel = (left >= right)
    else:
      assert False
    self.__stack.append(irel)

  def visitProcedureCall (self, call):
    procedure = call.procedure() # get procedure
    instruction = procedure.instruction() # get instructions
    
    scope = procedure.scope() # get scope of procedure
    actuals = call.actuals() # get the actuals that need to be fed into scope

    # print scope
    # for val in actuals:
    #   print val.expressionType().__class__
    # exit()
    
    environmentVisitor = KENV.EnvironmentVisitor() # create a new environment
    environment = scope.accept(environmentVisitor)
    save_ = self.__env # save state of environment
    self.__env = environment

    instruction.accept(self) # do instructions...
    self.__env = save_
    if call.next():
      call.next().accept(self)

  # def visitFunctionCall (self, call):
  #   procedure = call.procedure() # get procedure
  #   actuals = call.actuals() # get the actuals that need to be fed into scope
  #   instruction = procedure.instruction() # get instructions
  #   scope = procedure.scope() # get scope of procedure
  #   environmentVisitor = KENV.EnvironmentVisitor() # create a new environment
  #   environment = scope.accept(environmentVisitor)
  #   if call.next():
  #     call.next().accept(self)


def reportError (err):
  sys.stderr.write(ERROR_STR + err + "\n")
  exit()
