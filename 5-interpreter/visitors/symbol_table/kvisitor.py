#
# kvisitor.py
#
# Created by Kartik Thapar on 03/09/2013 at 17:54:25
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

class Visitor:
  """
  Visitor class that operates on the symbol table data structure.
  """

  def visitConstant (self, constant):
    pass

  def visitVariable (self, variable):
    pass

  def visitInteger (self, integer):
    pass

  def visitArray (self, array):
    pass

  def visitRecord (self, record):
    pass

  def visitScope (self, scope):
    pass
