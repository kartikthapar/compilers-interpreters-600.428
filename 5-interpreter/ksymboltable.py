#
# ksymboltable.py
#
# Created by Kartik Thapar on 03/09/2013 at 06:41:16
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import types as PType
from kexception import *
from visitors.symbol_table.kvisitor import Visitor

# ---------- SCOPE -----------

class Scope():
  
  def __init__(self, outerScope = None): # outer = None for top most
    """Creates Scope objects. In some cases, the parent scope is also provided if it exists."""
    assert isinstance(outerScope, Scope) or outerScope is None # default
    self.__outerScope = outerScope # need parent scope for all scopes
    self.__symbolTable = {} # need a hash type data stucture

  def outerScope (self):
    """Returns the outer scope for the current scope"""
    return self.__outerScope

  def setOuterScope (self, scope):
    """Set the outer scope for the current scope."""
    assert isinstance(scope, Scope) or scope is None
    self.__outerScope = scope

  def insert (self, entryName, entry):
    """Insert value in the symbol table in the current scope."""
    assert isinstance (entry, Entry)
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0
    if entryName in self.__symbolTable:
      raise SymbolTableWriteException("you already declared '%s'" % (entryName))
    else:
      self.__symbolTable[entryName] = entry

  def retrieve (self, entryName):
    """Retrieve values from the symbol table in the current scope."""
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0
    if entryName in self.__symbolTable: # find in current scope
      return self.__symbolTable[entryName]
    elif self.outerScope() != None: # find in outer scope(s)
      return self.outerScope().retrieve(entryName)
    else:
      raise SymbolTableReadException("you never declared '%s'" % (entryName))

  def accept (self, visitor):
    return visitor.visitScope(self)

  def symbolTable (self):
    """Returns the symbol table of the Scope object."""
    return self.__symbolTable

  def __str__( self ):
    string = ""
    for v in self.__symbolTable:
      string += v + " => " + str(self.__symbolTable[v]) + "\n"
    return string

# -------------- ENTRY --------------

class Entry:
  """Base class for all symbol table entries."""

  def __init__ (self, position):
    # need to record positions for errors
    assert type(position) is PType.IntType
    self.__position = position

  def position (self):
    """Returns the position of the entry."""
    return self.__position

  def accept (self, visitor):
    """For visitior."""
    assert isinstance(visitor, Visitor)
    pass


class Constant (Entry):
  """Constants in SIMPLE."""

  def __init__ (self, ktype, value, position):
    assert isinstance(ktype, Type)
    assert type(value) is PType.IntType or type(value) is PType.LongType
    Entry.__init__(self, position)
    self.__elementType = ktype # get type of the constant value (right now they are INTEGER types)
    self.__value = value # get value (this is set as DEVIL)

  def elementType (self):
    """Returns the type of Constant object."""
    return self.__elementType

  def value (self):
    """Returns the value of the Constant object"""
    return self.__value
  
  def __str__ (self):
    return "CONST<type: %s; value: %d>" % (str(self.__elementType), self.__value)

  def accept (self, visitor):
    return visitor.visitConstant(self)

class Variable (Entry):
  """Variables in SIMPLE"""

  def __init__ (self, name, elementType, position):
    assert type(name) is str
    assert len(name) > 0
    assert isinstance(elementType, Type)
    self.__name = name
    Entry.__init__(self, position)
    self.__elementType = elementType # get the type of Variable

  def elementType (self):
    """Returns the type of the variable."""
    return self.__elementType

  def name (self):
    """Returns the name of the variable"""
    return self.__name

  def __str__ (self):
    return "VAR<type:%s>, <name:%s>" % (str(self.__elementType), self.__name)

  def accept (self, visitor):
    return visitor.visitVariable(self)

# ------------ TYPE --------------

class Type (Entry):
  """Types in SIMPLE."""

  def __init__ (self, position):
    Entry.__init__(self, position)

  def __str__ (self):
    return "Type<>"


class Integer (Type):
  """Base Integer Class in SIMPLE. This is a singleton (only one instance)."""
  
  def __init__ (self):
    Type.__init__(self, 0) # for Integer, I don't know

  def __str__ (self):
    return "INTEGER<>"

  def accept (self, visitor):
    return visitor.visitInteger(self)


class Array (Type):
  """Arrays in SIMPLE."""

  def __init__ (self, elementType, length, position):
    assert isinstance(elementType, Type)
    assert type(length) is PType.IntType and length > 0
    Type.__init__(self, position)
    self.__elementType = elementType # get the type for Array elements
    self.__length = length # also record the length (for symbol table: set as DEVIL)

  def __str__ (self):
    return "ARRAY<type: %s; length: %d>" % (str(self.__elementType), self.__length)

  def size (self):
    """Returns the size of the array."""
    return self.__length

  def elementType (self):
    """Returns the type of the Array object."""
    return self.__elementType

  def accept (self, visitor):
    return visitor.visitArray(self)


class Record (Type):
  """Records in SIMPLE."""

  def __init__ (self, scope, position):
    assert isinstance(scope, Scope)
    Type.__init__(self, position)
    self.__scope = scope # get the scope

  def __str__ (self):
    return "RECORD<%s>" % (str(self.__scope))

  def scope (self):
    """Returns the scope of the Record object."""
    return self.__scope

  def accept (self, visitor):
    return visitor.visitRecord(self)


class InvalidType (Type):
  """
  InvalidType is a hack-type. In cases where there are variable use-before-declaration
  errors, we simply use InvalidType as the type of those variables.
  """

  def __init__ (self, position):
    Type.__init__(self, position)
