#
# ksymboltable.py
#
# Created by Kartik Thapar on 03/09/2013 at 06:41:16
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Implements various aspects of the Symbol Table.
"""

import types as PType
from kexception import *
from visitors.kvisitor import Visitor

class Scope():
    """
    Define a context/scope for variables, constants and types
    """
    
    def __init__(self, outerScope = None): # outer = None for top most
        """
        Creates Scope objects. In some cases, the parent scope is also provided if
        it exists.
        """
        assert isinstance(outerScope, Scope) or outerScope is None # default

        # need parent scope for all scopes
        self.__outerScope = outerScope

        # define a dictionary data for the symbol table --- why bother!
        self.__symbolTable = {}

    # outerScope is a private variable --- so it needs it's setters and getters
    # why I made it private? Absolutely no idea; but it's ok!

    def outerScope (self):
        """
        Return the outer scope for the current scope
        """
        return self.__outerScope


    def setOuterScope (self, scope):
        """
        Setter for scope
        """
        assert isinstance(scope, Scope) or scope is None
        self.__outerScope = scope


    def insert (self, entryName, entry):
        """
        Insert value in the symbol table
        """

        assert isinstance (entry, Entry)
        assert type(entryName) is PType.StringType
        assert len(entryName) > 0

        if entryName in self.__symbolTable:
            raise SymbolTableWriteException("you already declared '%s'" % (entryName))
        else:
            self.__symbolTable[entryName] = entry


    def retrieve (self, entryName):
        """
        Retrieve values from the symbol table.
        """

        assert type(entryName) is PType.StringType
        assert len(entryName) > 0

        if entryName in self.__symbolTable: # find in current scope
            return self.__symbolTable[entryName]
        elif self.outerScope() != None: # find in outer scope(s)
            return self.outerScope().retrieve(entryName)
        else:
            raise SymbolTableReadException("you never declared '%s'" % (entryName))


    def accept (self, visitor):
        """
        Found a Scope type in the symbol table.
        """
        return visitor.visitScope(self)


    def symbolTable (self):
        """
        Returns the symbol table of the Scope object.
        """
        return self.__symbolTable


    def __str__( self ):
        # official log function

        string = ""
        for v in self.__symbolTable:
            string += v + " => " + str(self.__symbolTable[v]) + "\n"
        return string

# ------------------------------------------------------------------------------

class Entry:
    """
    Base class for all symbol table entries.
    """

    def startPosition (self):
        """
        Returns the start position of the entry.
        """
        return self.__startPosition


    def endPosition (self):
        """
        Returns the end position of the entry.
        """
        return self.__endPosition


    def __init__ (self, startPosition, endPosition):
        """
        """

        assert type(startPosition) is PType.IntType
        assert type(endPosition) is PType.IntType

        # need to record positions for errors
        self.__startPosition = startPosition
        self.__endPosition = endPosition


    def accept (self, visitor):
        """
        """
        assert isinstance(visitor, Visitor)
        pass


class Constant (Entry):
    """
    Constants in SIMPLE.
    """

    def __init__ (self, ktype, value, startPosition, endPosition):
        assert isinstance(ktype, Type)
        assert type(value) is PType.IntType # for now consts are ints?
        
        # get start and end positions
        Entry.__init__(self, startPosition, endPosition)

        # get type of the constant value (right now they are INTEGER types)
        # get value (this is set as DEVIL)
        self.__type = ktype
        self.__value = value


    def elementType (self):
        """
        Returns the type of Constant object.
        """
        return self.__type


    def constantValue (self):
        """
        Returns the value of the Constant object
        """
        return self.__value

    
    def __str__ (self):
        """
        Display for constant objects.
        """
        return "CONST<type: %s; value: %d>" % (str(self.__type), self.__value)


    def accept (self, visitor):
        """
        Found a Constant type in the symbol table.
        """
        return visitor.visitConstant(self)



class Variable (Entry):
    """
    Variables in SIMPLE
    """
    def __init__ (self, ktype, startPosition, endPosition):
        assert isinstance(ktype, Type)

        # get start and end positions
        Entry.__init__(self, startPosition, endPosition)

        # get the type of Variable
        self.__type = ktype

        # add more variables for Variable


    def __str__ (self):
        """
        Display for Variable objects.
        """
        return "VAR<type: %s>" % (str(self.__type ))


    def elementType (self):
        return self.__type


    def accept (self, visitor):
        """
        Found a Variable type in the symbol table.
        """
        return visitor.visitVariable(self)



class Type (Entry):
    """
    Types in SIMPLE.
    """
    def __init__ (self, startPosition, endPosition):

        # get start and end positions
        Entry.__init__(self, startPosition, endPosition)


# NEVER create more than 1 instance of the Integer class: OK.
class Integer (Type):
    """
    Integers in SIMPLE
    """
    def __init__ (self):
        Type.__init__(self, 0, 0) # for Integer, I don't know


    def __str__ (self):
        """
        Display for Integer objects.
        """
        return "INTEGER<>"


    def accept (self, visitor):
        """
        Found an Integer type in the symbol table.
        """
        return visitor.visitInteger(self)


class Array (Type):
    """
    Arrays in SIMPLE.
    """
    def __init__ (self, etype, length, startPosition, endPosition):
        assert isinstance(etype, Type)
        assert type(length) is PType.IntType and length > 0

        Type.__init__(self, startPosition, endPosition)

        # get the type for Array elements
        # also record the length (for symbol table: set as DEVIL)
        self.__type = etype;
        self.__length = length;


    def __str__ (self):
        """
        Display for Array objects.
        """
        return "ARRAY<type: %s; length: %d>" % (str(self.__type), self.__length)


    def arrayLength (self):
        """
        Returns the length of the Array object.
        """
        return self.__length


    def elementType (self):
        """
        Returns the type of the Array object.
        """
        return self.__type


    def accept (self, visitor):
        """
        Found an Array type in the symbol table.
        """
        return visitor.visitArray(self)


class Record (Type):
    """
    Records in SIMPLE.
    """

    # for pointers we need to store a pointer to the scope object
    def __init__ (self, scope, startPosition, endPosition):
        assert isinstance(scope, Scope)

        Type.__init__(self, startPosition, endPosition)
        # get the scope
        self.__scope = scope


    def __str__ (self):
        """
        Display for Record objects.
        """
        return "RECORD<%s>" % (str(self.__scope))


    def scope (self):
        """
        Returns the scope of the Record object.
        """
        return self.__scope


    def accept (self, visitor):
        """
        Found a Record type in the symbol table.
        """
        return visitor.visitRecord(self)


class InvalidType (Type):
    """
    InvalidType is a hack-type. In cases where there are variable use-before-declaration
    errors, we simply use InvalidType as the type of those variables.
    """

    def __init__ (self, etype, length, startPosition, endPosition):
        assert isinstance(etype, Type)

        Type.__init__(self, startPosition, endPosition)

    # error cannot generate any output
    # def accept (self, visitor):
    #     return visitor.visitInvalidT(self)

