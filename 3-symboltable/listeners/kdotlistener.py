#!/usr/bin/python
#
# kdotlistener.py
#
# Created by Kartik Thapar on 02/25/2013 at 15:54:57
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class DotListener (Listener)
"""

from klistener import Listener

IDENT_VAL = 2

class DotListener(Listener):
    """
    Dot Listener: observes events related to parse tree dot output.
    Creates the dot output for the concrete syntax tree created by the Parser.

    Output type:

    kartiks-macbook:3-symboltable kartikthapar$ ./sc -cg
    PROGRAM X; VAR i: INTEGER; END X.
    strict digraph CST {
      L(0 [label="Program",shape=box])
      L(1 [label="PROGRAM",shape=diamond])
      L(0 -> L1)
      L(2 [label="X",shape=diamond])
      L(0 -> L2)
      L(3 [label=";",shape=diamond])
      L(0 -> L3)
      L(4 [label="Declarations",shape=box])
      L(0 -> L4)
      L(5 [label="VarDecl",shape=box])
      L(4 -> L5)
      L(6 [label="VAR",shape=diamond])
      L(5 -> L6)
      L(7 [label="IdentifierList",shape=box])
      L(5 -> L7)
      L(8 [label="i",shape=diamond])
      L(7 -> L8)
      L(9 [label=":",shape=diamond])
      L(5 -> L9)
      L(10 [label="Type",shape=box])
      L(5 -> L10)
      L(11 [label="INTEGER",shape=diamond])
      L(10 -> L11)
      L(12 [label=";",shape=diamond])
      L(5 -> L12)
      L(13 [label="END",shape=diamond])
      L(0 -> L13)
      L(14 [label="X",shape=diamond])
      L(0 -> L14)
      L(15 [label=".",shape=diamond])
      L(0 -> L15)
    }
    """

    def __init__(self):
        self.__boxStack = []
        self.__labelCounter = 0
        self.__dotOutput = ""


    def _makeIndent (self):
        """
        Creates indent using a defined indentation spec value.
        """
        return " " * IDENT_VAL


    def _generateLabel (self, value, shape):
        """
        Generate label value
        """
        leftlabel = self._makeIndent() + "L%d" % (self.__labelCounter)
        rightlabel = "[label=\"%s\",shape=%s]" % (value, shape)
        self.__dotOutput += leftlabel + " " + rightlabel + "\n"


    def _generateFlow (self):
        """
        Generate connector values in the flowchart.
        """
        leftlabel = self._makeIndent() + self.__boxStack[-1] # get top most element
        rightlabel = "L%d" % (self.__labelCounter)
        self.__dotOutput += leftlabel + " -> " + rightlabel + "\n"


    def inNonTerminal (self, nterm):
        """
        Entered a non terminal value.
        """
        if not self.__boxStack:
            self.__boxStack = ["L0"] # starts with L0
            self._generateLabel(nterm, "box")
        else:
            self._generateLabel(nterm, "box")
            self._generateFlow()
            self.__boxStack.append("L%d" % (self.__labelCounter))
        
        self.__labelCounter += 1


    def outNonTerminal (self):
        """
        Reached the end of a non terminal
        """
        self.__boxStack.pop()


    def matchTerminal (self, token):
        """
        Terminal matched ---> do leaf.
        """
        value = ""
        if token.kind() is not "eof":
            value = token.value()
        else:
            value = "eof"

        self._generateLabel(value, "diamond")
        self._generateFlow()
        self.__labelCounter += 1


    def dotOutput (self):
        """
        Returns dot output from Parser.
        """
        # change from temp X to strict digraph CST as in Piazza
        self.__dotOutput = "strict digraph CST {\n" + self.__dotOutput + "}"
        return self.__dotOutput

