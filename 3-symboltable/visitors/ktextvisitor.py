#
# ktextvisitor.py
#
# Created by Kartik Thapar on 03/09/2013 at 18:00:21
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class TextVisitor (Visitor)
"""

from kvisitor import Visitor

# change this value for a different setting on the Text Output for the Symbol Table
IDENT_VAL = 2

class TextVisitor (Visitor):
    """
    Implements text output for the symbol table.
    Visits Constants, Variables, Integers, Arrays, Records, Scopes to analyze
    the symbol table data structure.

    Output type:

    kartiks-macbook:3-symboltable kartikthapar$ more a.sim 
    PROGRAM X;
      CONST a = 47;
      VAR i: INTEGER;
      TYPE X = RECORD
        a, b: ARRAY 7 OF INTEGER;
      END;
    END X.
    kartiks-macbook:3-symboltable kartikthapar$ ./sc -t a.sim 
    SCOPE BEGIN
      X =>
        RECORD BEGIN
          SCOPE BEGIN
            a =>
              VAR BEGIN
                type:
                  ARRAY BEGIN
                    type:
                      INTEGER
                    length:
                      666
                  END ARRAY
              END VAR
            b =>
              VAR BEGIN
                type:
                  ARRAY BEGIN
                    type:
                      INTEGER
                    length:
                      666
                  END ARRAY
              END VAR
          END SCOPE
        END RECORD
      a =>
        CONST BEGIN
          type:
            INTEGER
          value:
            666
        END CONST
      i =>
        VAR BEGIN
          type:
            INTEGER
        END VAR
    END SCOPE
    """

    def __init__ (self):
        self.__indentationLevel = -1 # in_out
        self.__textOutput = ""


    def textOutput (self):
        """
        Returns the text output of the symbol table.
        """
        return self.__textOutput.rstrip("\n")


    def _in (self):
        """
        Found a Constant/Variable/Integer/etc...
        """
        self.__indentationLevel += 1


    def _out (self):
        """
        Ending a Constant/Variable/Integer/etc...
        """
        self.__indentationLevel -= 1


    def _makeIndent (self):
        """
        Calculates and returns the tabbed indent for a particular ...
        """
        return " " * IDENT_VAL * self.__indentationLevel


    def _makeText (self, value):
        self.__textOutput += self._makeIndent() + value + "\n"


    def visitConstant (self, constant):
        """
        Saw CONST<>
        """
        self._in()
        self._makeText("CONST BEGIN")
        self._in()
        self._makeText("type:")
        constant.elementType().accept(self)
        self._makeText("value:")

        # insert value
        self._in()
        self._makeText(str(constant.constantValue()))
        self._out()

        self._out() # to print END CONST with CONST
        self._makeText("END CONST")

        self._out()


    def visitVariable (self, variable):
        """
        Saw Variable<>
        """
        self._in()
        self._makeText("VAR BEGIN")
        
        self._in()
        self._makeText("type:")
        variable.elementType().accept(self)
        self._out()

        self._makeText("END VAR")
        self._out()


    def visitInteger (self, integer):
        """
        Saw Integer<>
        """
        self._in()
        self._makeText("INTEGER")
        self._out()


    def visitArray (self, array):
        """
        Saw Array<>
        """
        self._in()

        self._makeText("ARRAY BEGIN")

        self._in()
        self._makeText("type:")
        array.elementType().accept(self)
        # self._out()

        # self._in()
        self._makeText("length:")
        self._in()
        self._makeText(str(array.arrayLength()))
        self._out()
        self._out()

        self._makeText("END ARRAY")

        self._out()


    def visitRecord (self, record):
        """
        Saw Record<>
        """
        self._in()
        self._makeText("RECORD BEGIN")
        record.scope().accept(self)
        self._makeText("END RECORD")
        self._out()


    def visitScope (self, scope):
        """
        Discovered Scope<>
        """
        self._in()

        self._makeText("SCOPE BEGIN")

        st = scope.symbolTable()

        # we need this the sorted way
        for identifer in sorted(st):
            self._in()
            self._makeText("%s =>" % (identifer))
            st[identifer].accept(self)
            self._out()

        self._makeText("END SCOPE")

        self._out()
