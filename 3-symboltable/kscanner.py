#!/usr/bin/python
#
# scanner.py
#
# Created by Kartik Thapar on 02/12/2013 at 22:16:30
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#


import sys
from ktoken import Token
from kexception import *

"""
Program = "PROGRAM" identifier ";" Declarations
  ["BEGIN" Instructions] "END" identifier "." .

Declarations = { ConstDecl | TypeDecl | VarDecl } .
ConstDecl = "CONST" {identifier "=" Expression ";"} .
TypeDecl = "TYPE" {identifier "=" Type ";"} .
VarDecl = "VAR" {IdentifierList ":" Type ";"} .

Type = identifier | "ARRAY" Expression "OF" Type |
  "RECORD" {IdentifierList ":" Type ";"} "END" .

Expression = ["+"|"-"] Term {("+"|"-") Term} .
Term = Factor {("*"|"DIV"|"MOD") Factor} .
Factor = integer | Designator | "(" Expression ")" .

Instructions = Instruction {";" Instruction} .
Instruction = Assign | If | Repeat | While | Read | Write .
Assign = Designator ":=" Expression .
If = "IF" Condition "THEN" Instructions ["ELSE" Instructions] "END" .
Repeat = "REPEAT" Instructions "UNTIL" Condition "END" .
While = "WHILE" Condition "DO" Instructions "END" .
Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
Write = "WRITE" Expression .
Read = "READ" Designator .

Designator = identifier Selector .
Selector = {"[" ExpressionList "]" | "." identifier} .
IdentifierList = identifier {"," identifier} .
ExpressionList = Expression {"," Expression} .

identifier = letter {letter | digit} .
integer = digit {digit} .
letter = "a" | "b" | .. | "z" | "A" | "B" | .. | "Z" .
digit = "0" | "1" | .. | "9" .
"""

# define digits; digit = "0" | "1" | .. | "9" .
digits = "0123456789"

# define letter; letter = "a" | "b" | .. | "z" | "A" | "B" | .. | "Z" .
letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

# define symbols using the language above
symbols = ";.=:+-*()#<>[],"

# define keywords using the language above
keywords = ["PROGRAM", "BEGIN", "END", "CONST", "TYPE", "VAR",
            "ARRAY", "OF", "RECORD", "DIV", "MOD", "IF", "THEN", "ELSE",
            "REPEAT", "UNTIL", "WHILE", "DO", "WRITE", "READ"]

# LF (Line feed, '\n', 0x0A, 10 in decimal) or 
# CR (Carriage return, '\r', 0x0D, 13 in decimal)
eol = "\n\r"
whitespaces = " \t\f" # include '_', '\t', '\f'

class Scanner:
    
    """
    Implements a generic Scanner for SIMPLE programming language.
    """

    def __init__(self, code):
        """
        scanner_object = Scanner(code)

        code: input source code text either through stdin or file
        """

        self.__code = code # source code

        self.__length = len(code) # length of the code input

        self.__textPosition = 0 # at init, file is at 0 position

        self.__startPosition = 0 # start position of token
        self.__endPosition = 0 # end position of token

        # __spec__: all() cannot be called after next() until e-o-f

        self.__nextCalled = False;


    # ---------------------------------------------------------------
    # Define HELPER functions
    #
    # - (BOOL)_codeExists: is code exists
    # - (void)_counter: increments file read counter
    # - (BOOL)_isValidCode: check if part of language
    # - (char *)_character: get the char on the current position
    # ---------------------------------------------------------------

    def _codeExists (self):
        """
        Return: BOOL

        Checks if there is more code to read from the source. Returns true
        if code exists, else false.
        """
        
        # if length of code traversed is less than than self.__length, return true

        if self.__textPosition < self.__length:
            return True
        else:
            return False


    def _counter (self):
        """
        Return: Nothing

        Increments the file read counter.
        """

        self.__textPosition += 1


    def _isValidCode (self):
        """
        Return: BOOL
        
        Check if character read from the source is a valid character 
        in the language.
        """

        char = self._character()
        
        if  (char in letters) or (char in digits) or \
            (char in symbols) or (char in whitespaces):
            return True
        else:
            return False


    def _character (self):
        """
        Return: character value
        
        Reads in a character from the source code and returns it.
        """

        return (self.__code[self.__textPosition])


    # ---------------------------------------------------------------
    # Define CHECKER methods to get the type of token
    # - (Token)_identifierToken: check if token is an identifier
    # - (BOOL)_isKeyword: keyword is type(identifier)
    # - (Token)_numberToken: check if token is a number
    # - (Token)_symbolToken: check if token is a symbol
    # ---------------------------------------------------------------


    def _identifierToken (self):
        """
        Return: token

        Capture a token and check if it's an identifier and return the
        token value. A keyword is a type of an identifier.
        """

        # identifier = letter {letter | digit} .
        # check for letter is complete, now you can check for a digit as well

        value = "" # this is the string value

        # get start position
        self.__startPosition = self.__textPosition

        while self._codeExists():
            char = self._character()
            if char in letters or char in digits:
                value += char
                self._counter()
            else:
                break

        # store end position
        self.__endPosition = self.__textPosition - 1

        assert self.__endPosition >= self.__startPosition

        # invent token

        rToken = None

        if self._isKeyword (value):
            rToken = Token("keyword", 
                self.__startPosition, 
                self.__endPosition,
                value)
        else:
            rToken = Token("identifier",
                self.__startPosition,
                self.__endPosition,
                value)

        return rToken
    

    def _numberToken (self):
        """
        Return: Token

        Capture a token and check if it's a number and return the token
        value for the integer.
        """

        value = ""

        # get start position
        self.__startPosition = self.__textPosition
        
        while self._codeExists():
            char = self._character()
            if char in digits:
                value += char
                self._counter()
            else:
                break

        # get the end position

        self.__endPosition = self.__textPosition - 1

        assert self.__endPosition >= self.__startPosition
        
        # invent token
        
        # 32 bit check
        # 2**63 will shout

        rToken = Token("integer", 
            self.__startPosition, 
            self.__endPosition,
            int (value)) # convert value to int for "integer" kind

        return rToken


    def _isKeyword (self, value):
        """
        Return: BOOL

        Returns a bool value based on the value of the identifier. If the
        identifier is a keyword, this returns; otherwise false.
        """

        assert type(value) is str

        if value in keywords:
            return True
        else:
            return False


    def _symbolToken (self):

        """
        Return: Token

        Returns a token for symbols. It also takes care of special
        symbols like: '<=', '>=', ':='.
        """

        """
        Algorithm symbol token:

        if char in symbols:
            add to value
            counter
            if char is in ':' or '<' or '>'':
                add to value
                counter
            else
                not possible as the only combinations are:
                ':=', '<=', '>='
        """

        # while self._codeExists():
        #   char = self._character()
        #   if char in symbols:
        #       value += char
        #       self._counter() # counter
        #       if (value == ":") or (value == "<") or (value == ">"):
        # while will not exist here: a symbol is only 1 character except for
        # the ones mentioneed above

        # get the start position
        self.__startPosition = self.__textPosition

        value = self._character() # prechecked

        self._counter() # increment counter

        if (value == "<") or (value == ">") or (value == ":"):
            if self._codeExists():
                char = self._character()
                if char in "=":
                    value += char
                    self._counter()

        self.__endPosition = self.__textPosition - 1

        assert self.__endPosition >= self.__startPosition

        rToken = Token("symbol", 
            self.__startPosition,
            self.__endPosition,
            value)

        return rToken


    def _handleComment (self):
        """
        Return: nothing

        Handles comments in SIMPLE. Comments are of the form:
        (* some text *). Comments can be multi-line.
        """

        """
        Algorithm _handleComment
        while (code exists):
            if char = '*'
                if char = ')'
                    return
            else
                go on
            
        """

        # we are inside the comment with '(*' matched
        
        self.__startPosition = self.__textPosition

        _found = 0

        while self._codeExists():
            if self._character() in "*":
                _found = 1
            elif self._character() in ")" and _found == 1:
                return
            else:
                _found = 0
            
            self._counter()



    def next(self):
        """
        Return: Token

        Operates on the actual piece of source code and returns a
        valid token value. This takes care of whitespace, comments,
        letters, digits and symbols as defined in the language. 
        Based on the current value of the character, it requests helper 
        functions to retrieve a token.
        """

        """
        Algorithm next
        """

        self.__nextCalled = True # you cannot call all now

        rToken = None

        while self._codeExists():
            if self._character() in whitespaces + eol:
                self._counter()
            elif self._character() in "(":
                self._counter()
                if self._codeExists() and self._character() in "*": # start of comment
                    self._counter()
                    self._handleComment()
                    self._counter()
                else:
                    rToken = Token("symbol",
                        self.__textPosition - 1,
                        self.__textPosition - 1,
                        "(")
                    return rToken
            else:
                break


        # self.__startPosition = self.__textPosition - 1

        if self._codeExists():
            char = self._character() # get character
            try:
                if char in letters:
                    rToken = self._identifierToken()
                elif char in digits:
                    rToken = self._numberToken()
                elif char in symbols:
                    rToken = self._symbolToken()
                else:
                    exc_string = ("invalid character: '%s' "
                    "discovered at location '%s'; is not part of SIMPLE.") %\
                    (char, self.__textPosition)
                    raise InvalidCharacterException (exc_string)
            except InvalidCharacterException, e:
                return e;
                self._counter()

        if rToken is None:
            rToken = Token("eof",
                self.__textPosition,
                self.__textPosition,
                None)
        return rToken


    def all (self):
        """
        Return: [Token [Token...]]

        all() is not called during a next method execution.
        Don't be a part of this sin.
        """

        """
        Algorithm allTokens

            token = next()
            if token is not instance of type Token Class
                create error
                return tokenList, err
            else
                append token to tokenList


        """

        if self.__nextCalled is True:
            try:
                exc_string = "all() called after next() -- will terminate."
                raise SeekAllAfterNextException (exc_string)
            except SeekAllAfterNextException, e:
                sys.stderr.write("error: " + e)
                sys.exit(1)

        # either use the code segment above or assert
        assert self.__nextCalled is False
        
        tokenList = [] # store the token list

        tokenValue = None # token value
        
        error = None # need to establish KError class that handles error requests



        while self._codeExists():
            tokenValue = self.next() # get token

            if not isinstance(tokenValue, Token): # is token of type Token?
                error = tokenValue 
                break

            if tokenValue.kind() is not "eof":
                tokenList.append(tokenValue)


        # only if error has not occured:
        if error is None:
            tokenList.append(self.next()) # accept end-of-source token

        return tokenList, error
