# Assignment 3

# List of files:

	./kexception.py
	./kparser.py
	./kscanner.py
	./ksymboltable.py
	./ktoken.py
	./listeners
	./listeners/__init__.py
	./listeners/kdotlistener.py
	./listeners/kerrorlistener.py
	./listeners/klistener.py
	./listeners/ktextlistener.py
	./README
	./sc
	./visitors
	./visitors/__init__.py
	./visitors/kdotvisitor.py
	./visitors/ktextvisitor.py
	./visitors/kvisitor.py

# How to run:

`sc` is the binary that needs to be executed.

    > ./sc -s # for scanner
    # requires standard input
    
    > ./sc -s filename # for scanner
    # requires filename to be a valid file

    > ./sc -c # for parser; also generates CST
    # requires standard input

    > ./sc -c filename # for parser; also generates CST
    # requires filename to be valid

    > ./sc -cg # for parser; also generates graph output
    # requires standard input

    > ./sc -cg filename # for parser; also generates graph output
    # requires filename to be valid

    > ./sc -t # for parser; also generates symbol table text output
    # requires standard input

    > ./sc -t filename # for parser; also generates symbol table text output
    # requires filename to be valid

    > ./sc -tg # for parser; also generates symbol table graph output
    # requires standard input

    > ./sc -tg filename # for parser; also generates symbol table graph output
    # requires filename to be valid


A pointer about checking text/graph output ---

1. [DO NOT USE THIS] It is possible that the error doesn't match with a simple diff (or other sophisticated tools) as I use tabs etc.

2. [DO NOT USE THIS] According to diff manual:

        -b  --ignore-space-change
            Ignore changes in the amount of white space.

        -w  --ignore-all-space
            Ignore all white space.

        -B  --ignore-blank-lines
            Ignore changes whose lines are all blank.

    Use `-b` to ignore space change; can also use `-w` as mentioned above.

3. [USE THIS] In this case, don't use diff at all as different people have different ways of generates nodes/anchors whatever. Simply create the pictures using:

	    > ./sc -tg filename | dot -Tpng > program.png
	    
	    > # if you already have graph output using ./sc -tg filename, do:
	    > cat output | dot -Tpng > program.png
    

# Notes:

- README is `this` file.
- About Errors: Errors are handled using ErrorListener. 
    
    1. For '-c', you will only see syntactic errors.
    2. For '-t', you will see both syntactic and semantic errors. This is how it's supposed to be.
