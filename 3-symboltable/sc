#!/usr/bin/python
#
# sc.py
#
# Created by Kartik Thapar on 02/12/2013 at 18:07:39
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

# Invocation = "./sc" ["-" ("s"|"c"|"t"|"a"|"i")] [filename] .

import argparse
import sys

from kscanner import Scanner
from kparser import Parser

# imports for listeners
from listeners.kerrorlistener import ParserErrorListener, SymbolTableErrorListener
from listeners.ktextlistener import TextListener
from listeners.kdotlistener import DotListener

from visitors.ktextvisitor import TextVisitor
from visitors.kdotvisitor import DotVisitor

ERROR_STR = "error:"

def main():

    # create an command line argument parser object
    cmd_line_parser = argparse.ArgumentParser()

    # create a mutually exclusive group
    cmd_line_group = cmd_line_parser.add_mutually_exclusive_group()

    # add command line options to group
    cmd_line_group.add_argument("-s", "--scanner", 
        help = "implement scanner", action = "store_true")
    cmd_line_group.add_argument("-c", "--csyntax", 
        help = "output concrete syntax tree", action = "store_true")
    cmd_line_group.add_argument("-t", "--symtable", 
        help = "output symbol table", action = "store_true")
    cmd_line_group.add_argument("-a", "--abstable", 
        help = "output abstract syntax tree", action = "store_true")
    cmd_line_group.add_argument("-i", "--interpret", action = "store_true",
        help = "interpret the intermediate representation directly")

    graph_group = cmd_line_parser.add_mutually_exclusive_group()

    graph_group.add_argument("-g", "--graph",
        help = "show graph output", action = "store_true")

    filename_string = "if filename is a single dash ('-') or absent, \
    cat reads from the standard input."

    cmd_line_parser.add_argument('filename', help=filename_string, nargs='?')

    args = cmd_line_parser.parse_args()
    # ------------------------------------------------------------------
    # parsing is complete

    if args.scanner:
        doScanner(args)
    elif args.csyntax:
        doParser(args)
    elif args.symtable:
        doSymbolTable(args)
    else:
        print "Note: for now only '-s' or '-c' works!"
        print "-------------------------------------------------------"
        print cmd_line_parser.print_help()



def readFileFromArgs (args):
    """
    Generic Read File from command line arguments.
    """

    sourceCode = ""
    if args.filename is not None: # if filename is an input
        try:
            with open(args.filename) as f:
                sourceCode = f.read() # read the entire source code from filename
        except IOError as e:
           sys.stderr.write(ERROR_STR + " " + str(e) + "\n")
           sys.exit(0)

    return sourceCode


def fetchSourceCode (args):
    """
    Generic method to fetch source code from either a SIMPLE
    code file or stdin.
    """
    sourceCode = ""

    if args.filename:
        sourceCode = readFileFromArgs(args)
    else:
        try:
            sourceCode = sys.stdin.read().rstrip()
        except KeyboardInterrupt:
            sys.stderr.write("\n")
            sys.stderr.write(ERROR_STR + " " + "Interrupted by user. Bye!\n")
            sys.exit(1)

    return sourceCode


def fetchTokensFromScanner (args):
    """
    """

    sourceCode = fetchSourceCode(args) # get source code
    assert type(sourceCode) is str

    scanner = Scanner(sourceCode) # make a scanner object

    tokenList, error = scanner.all() # get all tokens from sourcecode

    return tokenList, error



def scannerErrorCheck (tokenList, error, printToken):
    """
    Performs error check for the scanner.
    """

    if printToken is True:
        for token in tokenList:
            print str(token)

    if error is not None:
        err = ERROR_STR + " " + str(error) + "\n"
        sys.stderr.write(err)
        err = ERROR_STR + " " + "cannot scan further. Terminating..." + "\n"
        sys.stderr.write(err)
        sys.exit(1)


def doScanner (args):
    """
    Runs scanner on source code provided by a SIMPLE file or stdin.
    """
    
    tokenList, error = fetchTokensFromScanner(args)

    scannerErrorCheck(tokenList, error, printToken=True)


def doParser (args):
    """
    Implements parser on tokens provided by the scanner.
    """

    tokenList, error = fetchTokensFromScanner(args)

    scannerErrorCheck (tokenList, error, printToken=False)

    # create a parser
    parser = Parser(tokenList)
    
    # create dot, text and error listeners
    dotListener = DotListener()
    textListener = TextListener() 
    parserErrorListener = ParserErrorListener()

    # register dot, text and error listeners
    parser.registerListener(dotListener)
    parser.registerListener(textListener)
    parser.registerListener(parserErrorListener)
    
    # run parser
    parser.parse()

    # if there are errors ---> report and exit
    if parserErrorListener.isError():
        errorList = parserErrorListener.errorOutput()
        for error in errorList:
            err = ERROR_STR + " " + error + "\n"
            sys.stderr.write(err)
        sys.exit(1) # error

    # else based on args ---> do stuff
    if args.graph:
        print dotListener.dotOutput()
    else:
        print textListener.textOutput()


def doSymbolTable(args):
    """
    Implements symbol table.
    """

    tokenList, error = fetchTokensFromScanner(args)
    scannerErrorCheck (tokenList, error, printToken=False)

    # create a parser + listeners, etc.
    parser = Parser(tokenList)

    # create error listeners
    parserErrorListener = ParserErrorListener()
    symbolTableErrorListener = SymbolTableErrorListener()

    # register listeners
    parser.registerListener(parserErrorListener)
    parser.registerListener(symbolTableErrorListener)

    # run parser
    parser.parse()

    # Error Check ---------------------------------------------------------------
    if parserErrorListener.isError() or symbolTableErrorListener.isError():
        errorList = parserErrorListener.errorOutput() + symbolTableErrorListener.errorOutput()
        for error in errorList:
            err = ERROR_STR + " " + error + "\n"
            sys.stderr.write(err)
        sys.exit(1) # error


    

    scope = parser.majorScope()

    if args.graph:
        dotVisitor = DotVisitor() # dot output
        scope.accept(dotVisitor) # do this
        print dotVisitor.dotOutput()
    else:
        textVisitor = TextVisitor() # text output
        scope.accept(textVisitor) # do this
        print textVisitor.textOutput()


if __name__ == '__main__':
    main()

