#!/usr/bin/python
#
# ktree.py
#
# Created by Kartik Thapar on 03/24/2013 at 18:27:27
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import ksymboltable as KST

relators = ['=', '#', '<', '>', '<=', '>='] # see assignment 4
operators = ['+', '-', '*', 'DIV', 'MOD'] # see assignment 4
negators = {
  "=" : "#",
  "#" : "=",
  "<" : ">=",
  ">=" : "<",
  ">" : "<=",
  "<=" : ">"
}


class Node:
  """
  Base class for all symbol table entries.
  """

  def accept (self, visitor):
    pass


# -----------Instructions------------

class Instruction (Node):
  """
  Instructions = Instruction | Instruction Instructions .
  Instruction = Assign | If | Repeat | Read | Write .
  """

  def __init__ (self):
    self.__next = None # for ordering


  def setNext (self, instruction):
    """
    For the calling object, set the instruction as the next instruction in the
    sequence.
    """
    assert isinstance(instruction, Instruction)
    self.__next = instruction


  def next (self):
    """
    For the calling instruction, return the next instruction in the instruction
    sequence
    """
    return self.__next


class Assign (Instruction):
  """
  Assign = Location Expression .
  """

  def __init__ (self, location, expression):
    Instruction.__init__(self)
    assert isinstance(location, Location)
    assert isinstance(expression, Expression)
    assert expression.expressionType() is location.expressionType()
    self.__location = location
    self.__expression = expression


  def accept (self, visitor):
    return visitor.visitAssignment(self)


  def location (self):
    """
    Returns location
    """
    return self.__location


  def expression (self):
    """
    Returns expression
    """
    return self.__expression


  def __str__ (self):
    """
    Display for Assign.
    """
    s = """{
  Assign:
    Location: %s,
    Expression: %s
}""" % (self.__location, self.__expression)
    
    return s


class If (Instruction):
  """
  If = Condition Instructions_true [Instructions_false] .
  """

  def __init__ (self, condition, instructionTrue, instructionFalse):
    Instruction.__init__(self)
    assert isinstance(condition, Condition)
    assert isinstance(instructionTrue, Instruction)
    assert instructionFalse is None or isinstance(instructionFalse, Instruction) # conditional [_false]
    self.__condition = condition
    self.__instructionTrue = instructionTrue
    self.__instructionFalse = instructionFalse


  def accept (self, visitor):
    return visitor.visitIf(self)


  def condition (self):
    return self.__condition


  def instructionTrue (self):
    return self.__instructionTrue


  def instructionFalse (self):
    return self.__instructionFalse


  def __str__ (self):
    """
    Display for Variable.
    """
    s = """{
  If:
    Conditon: %s,
    True: %s,
    False: %s
}""" % (self.__condition, self.__instructionTrue, self.__instructionFalse)
    
    return s


class Repeat (Instruction):
  """
  Repeat = Condition Instructions .
  """

  def __init__ (self, condition, instruction):
    Instruction.__init__(self)
    assert isinstance(condition, Condition)
    assert isinstance(instruction, Instruction)
    self.__condition = condition
    self.__instruction = instruction


  def accept (self, visitor):
    return visitor.visitRepeat(self)


  def condition (self):
    return self.__condition


  def instruction (self):
    return self.__instruction


  def __str__ (self):
    """
    Display for Repeat.
    """
    s = """{
  Repeat:
    Condition: %s,
    Instruction: %s
}""" % (self.__condition, self.__instruction)
    
    return s


class Read (Instruction):
  """
  Read = Location .
  """

  def __init__ (self, location):
    Instruction.__init__(self)
    assert isinstance(location, Location)
    assert isinstance(location.expressionType(), KST.Integer)
    self.__location = location


  def location (self):
    return self.__location


  def accept (self, visitor):
    return visitor.visitRead(self)


  def __str__ (self):
    """
    Display for Read.
    """
    s = """{
  Read:
    Location: %s,
}""" % (self.__location)
    
    return s


class Write (Instruction):
  """
  Write = Expression .
  """

  def __init__ (self, expression):
    Instruction.__init__(self)
    assert isinstance(expression, Expression)
    assert isinstance(expression.expressionType(), KST.Integer)
    self.__expression = expression


  def accept (self, visitor):
    return visitor.visitWrite(self)


  def expression(self):
    return self.__expression


  def __str__ (self):
    """
    Display for Write.
    """
    s = """{
  Write:
    Instruction: %s,
}""" % (self.__instruction)
    
    return s


# -----------Expressions------------

class Expression (Node):
  """
  Expression = Number | Location | Binary .
  """

  def __init__ (self):
    self._expressionType = None


  def expressionType (self):
    return self._expressionType


class Number (Expression):
  """
  Number .
  """

  def __init__ (self, value):
    """
    Value is an entry of type Constant.
    """
    Expression.__init__(self)
    assert isinstance (value, KST.Constant)
    self.__constant = value
    self._expressionType = value.elementType()


  def constant (self):
    """
    Returns the entry (which is of type Constant).
    """
    return self.__constant


  def accept (self, visitor):
    return visitor.visitNumber(self)


  def __str__ (self):
    """
    Display for Number.
    """
    s = """{
  Number:
    Constant: %s,
    ExpressionType: %s
}""" % (self.__constant, self._expressionType)
    
    return s


class Location (Expression):
  """
  Location = Variable | Index | Field .
  """

  def __init__ (self):
    Expression.__init__(self)


class Variable (Location):
  """
  Variable .
  """

  def __init__ (self, variable):
    Location.__init__(self)
    assert isinstance(variable, KST.Variable)
    self.__variable = variable
    self._expressionType = variable.elementType()


  def accept (self, visitor):
    return visitor.visitVariable(self)


  def variable (self):
    return self.__variable


  def __str__ (self):
    """
    Display for Variable.
    """
    s = """{
  Variable:
    Variable: %s,
    ExpressionType: %s
}""" % (self.__variable, self._expressionType)
    
    return s
    

class Index (Location):
  """
  Index = Location Expression .
  """

  def __init__ (self, location, expression):
    Location.__init__(self)
    assert isinstance(location, Location)
    assert isinstance(expression, Expression)
    self.__location = location
    self.__expression = expression
    self._expressionType = location.expressionType().elementType() # use location; do not reference self.__location


  def accept (self, visitor):
    return visitor.visitIndex(self)


  def location (self):
    return self.__location


  def expression (self):
    return self.__expression


  def __str__ (self):
    """
    Display for Index.
    """
    s = """{
  Index:
    Location: %s,
    Expression: %s,
    ExpressionType: %s
}""" % (self.__location, self.__expression, self._expressionType)
    
    return s


class Field (Location):
  """
  Field = Location Variable .
  """

  def __init__ (self, location, variable):
    Location.__init__(self)
    assert isinstance(location, Location)
    assert isinstance(variable, Variable)
    self.__location = location
    self.__variable = variable
    self._expressionType = variable.expressionType()


  def accept (self, visitor):
    return visitor.visitField(self)


  def location (self):
    return self.__location


  def variable (self):
    return self.__variable


  def __str__ (self):
    """
    Display for Field.
    """
    s = """{
  Field:
    Location: %s,
    Variable: %s,
    ExpressionType: %s
}""" % (self.__location, self.__variable, self._expressionType)
    
    return s


class Binary (Expression):
  """
  Binary = Operator Expression_left Expression_right .
  """

  def __init__ (self, operator, leftExpression, rightExpression):
    Expression.__init__(self)
    assert operator in operators
    assert isinstance(leftExpression, Expression)
    assert isinstance(rightExpression, Expression)
    self.__operator = operator
    self.__leftExpression = leftExpression
    self.__rightExpression = rightExpression
    self._expressionType = rightExpression.expressionType() # assign any


  def accept (self, visitor):
    return visitor.visitBinary(self)


  def operator (self):
    return self.__operator


  def leftExpression (self):
    """
    Returns the left expression associated with the operation.
    """
    return self.__leftExpression


  def rightExpression (self):
    """
    Returns the right expression associated with the operation.
    """
    return self.__rightExpression


  def __str__ (self):
    """
    Display for Binary.
    """
    s = """{
  Binary:
    Operator: %s,
    Left: %s,
    Right: %s,
    ExpressionType: %s
}""" % (self.__operator,  self.__leftExpression, self.__rightExpression, self._expressionType)
    
    return s


# -----------Conditions------------

class Condition (Node):
  """
  Condition = Relation Expression_left Expression_right .
  The relation in a condition can be one of [=, #, <, >, <=, >=].
  """

  def __init__ (self, relation, leftExpression, rightExpression):
    assert relation in relators
    assert isinstance(leftExpression, Expression)
    assert isinstance(rightExpression, Expression)
    self.__relation = relation
    self.__leftExpression = leftExpression
    self.__rightExpression = rightExpression


  def relation (self):
    """
    Returns the condition associated with the condition.
    """
    return self.__relation


  def leftExpression (self):
    """
    Returns the left expression associated with the condition.
    """
    return self.__leftExpression


  def rightExpression (self):
    """
    Returns the left expression associated with the condition.
    """
    return self.__rightExpression


  def accept (self, visitor):
    return visitor.visitCondition(self)


  def __str__ (self):
    """
    Display for Variable.
    """
    s = """{
  Condition:
    Relation: %s,
    Left: %s,
    Right: %s
}""" % (self.__relation, self.__leftExpression, self.__rightExpression)
    
    return s


# -----------WHILE------------

def While (condition, instruction):
  """
  While is transformed into a 'Repeat' inside an 'If' statement.

  IF condition THEN
    REPEAT instructions UNTIL not(CONDITION) END
  END
  """
  assert isinstance(condition, Condition)
  assert isinstance(instruction, Instruction)

  # before we create a negated condition, we need to reverse the relation of condition
  relation = condition.relation()
  relation = negators[relation]

  # get left/right expression
  leftExpression = condition.leftExpression()
  rightExpression = condition.rightExpression()

  # create a negated condition
  negatedCondition = Condition(relation, leftExpression, rightExpression)

  # now create the Repeat Instruction; Repeat needs the negated condition
  repeatInstruction = Repeat(negatedCondition, instruction)

  # now create the If instruction; condition and instruction=Repeat
  transform = If(condition, repeatInstruction, None)
  return transform



def test():
  a = Instruction()
  # a.__next = Instruction()
  i = Instruction()
  print i
  a.setNext(i)
  print a.next()

# test()