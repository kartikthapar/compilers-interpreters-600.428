@Author: [Kartik Thapar](mailto:Kartik.Thapar@jhu.edu)

# Assignment 4 - Abstract Syntax Tree

# List of files:

	./kexception.py
	./kparser.py
	./kscanner.py
	./ksymboltable.py
	./ktoken.py
	./ktree.py
	./listeners
	./listeners/__init__.py
	./listeners/kdotlistener.py
	./listeners/kerrorlistener.py
	./listeners/klistener.py
	./listeners/ktextlistener.py
	./README.md
	./sc
	./visitors
	./visitors/__init__.py
	./visitors/abstract_syntax_tree
	./visitors/abstract_syntax_tree/__init__.py
	./visitors/abstract_syntax_tree/kdotvisitor.py
	./visitors/abstract_syntax_tree/ktextvisitor.py
	./visitors/abstract_syntax_tree/kvisitor.py
	./visitors/symbol_table
	./visitors/symbol_table/__init__.py
	./visitors/symbol_table/kdotvisitor.py
	./visitors/symbol_table/ktextvisitor.py
	./visitors/symbol_table/kvisitor.py
	
A new seperate folder has been added for visitors. Tt has two seperate folders for the symbol table and the abstract syntax tree.

# How to run:

`sc` is the binary that needs to be executed.

    > ./sc -s # for scanner
    # requires standard input
    
    > ./sc -s filename # for scanner
    # requires filename to be a valid file

    > ./sc -c # for parser; also generates CST
    # requires standard input

    > ./sc -c filename # for parser; also generates CST
    # requires filename to be valid

    > ./sc -cg # for parser; also generates graph output
    # requires standard input

    > ./sc -cg filename # for parser; also generates graph output
    # requires filename to be valid

    > ./sc -t # for parser; also generates symbol table text output
    # requires standard input

    > ./sc -t filename # for parser; also generates symbol table text output
    # requires filename to be valid

    > ./sc -tg # for parser; also generates symbol table graph output
    # requires standard input

    > ./sc -tg filename # for parser; also generates symbol table graph output
    # requires filename to be valid

    > ./sc -a # for parser; also generates abstract syntax tree text output
    # requires standard input

    > ./sc -a filename # for parser; also generates abstract syntax tree text output
    # requires filename to be valid

    > ./sc -ag # for parser; also generates abstract syntax tree graph output
    # requires standard input

    > ./sc -ag filename # for parser; also generates abstract syntax tree graph output
    # requires filename to be valid

A pointer about checking text/graph output ---

1. [DO NOT USE THIS] It is possible that the error doesn't match with a simple diff (or other sophisticated tools) as I use tabs etc.

2. [DO NOT USE THIS] According to diff manual:

        -b  --ignore-space-change
            Ignore changes in the amount of white space.

        -w  --ignore-all-space
            Ignore all white space.

        -B  --ignore-blank-lines
            Ignore changes whose lines are all blank.

    Use `-b` to ignore space change; can also use `-w` as mentioned above.

3. [USE THIS] In this case, don't use diff at all as different people have different ways of generates nodes/anchors whatever. Simply create the pictures using:

	    > ./sc -ag filename | dot -Tpng > program.png
	    
	    > # if you already have graph output using ./sc -ag filename, do:
	    > cat output | dot -Tpng > program.png

# Notes:

- README is `this` file.
- About Errors: Errors are handled using ErrorListener. 
    
    1. For '-c', you will only see syntactic errors.
    2. For '-t', you will see both syntactic and semantic errors. This is how it's supposed to be.
    3. For '-a', you will see all the errors.

---------------------------------------------

# Error Handling Changes

Error handling has been the major task of the compiler for graduate students. I've used the observer pattern to store the errors in an error list that can be retrieved by the following public method:

    def errorOutput (self) # in kerrorlistener.py

To check if errors exist, use the following public API:
  
    def isError (self) # in kerrorlistener.py

As the error occurs in the code, a notification is sent to all the listeners; and only the Error Listener(s) really receive the notification.

Previously, the base class defined particular methods for detecting errors in the Parser relating to syntactic parsing and creation of the symbol table. But all the methods have now been consolidated into a single error handler method `handleError` that sends a notification to listeners with a particular context and now the type of the error as follows:

    def _notifyListeners (self, event = None, context = None):
      #...
      elif event is "handleParserError":
        self._notifyError(context, "parser_type")
      elif event is "handleSymbolTableError":
        self._notifyError(context, "symbol_type")

A symbol table error for example is handled using the following code:

    def handleError (self, error, errorType):
      """
      Handle symbol table (type) errors in the Parser.
      """
      if errorType == "symbol_type":
        self.addError(error)
        return
