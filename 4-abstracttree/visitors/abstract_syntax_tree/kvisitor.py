#!/usr/bin/python
#
# kvisitor.py
#
# Created by Kartik Thapar on 03/26/2013 at 00:54:51
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

class Visitor(object):
  """
  Visitor class that operates on the abstract syntax tree data structure.
  """
  
  def visitInstructions (self, instruction):
    pass


  def visitAssignment (self, _assign):
    pass


  def visitIf (self, _if):
    pass


  def visitRepeat (self, _repeat):
    pass


  def visitRead (self, _read):
    pass


  def visitWrite (self, _write):
    pass


  def visitNumber (self, _number):
    pass


  def visitVariable (self, _variable):
    pass


  def visitIndex (self, _index):
    pass


  def visitField (self, _field):
    pass


  def visitBinary (self, _binary):
    pass


  def visitCondition (self, _condition):
    pass

