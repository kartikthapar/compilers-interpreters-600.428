#!/usr/bin/python
#
# kparser.py
#
# Created by Kartik Thapar on 02/23/2013 at 02:31:40
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class Parser
"""

"""
Program = "PROGRAM" identifier ";" Declarations
  ["BEGIN" Instructions] "END" identifier "." .

Declarations = { ConstDecl | TypeDecl | VarDecl } .
ConstDecl = "CONST" {identifier "=" Expression ";"} .
TypeDecl = "TYPE" {identifier "=" Type ";"} .
VarDecl = "VAR" {IdentifierList ":" Type ";"} .

Type = identifier | "ARRAY" Expression "OF" Type |
  "RECORD" {IdentifierList ":" Type ";"} "END" .

Expression = ["+"|"-"] Term {("+"|"-") Term} .
Term = Factor {("*"|"DIV"|"MOD") Factor} .
Factor = integer | Designator | "(" Expression ")" .

Instructions = Instruction {";" Instruction} .
Instruction = Assign | If | Repeat | While | Read | Write .
Assign = Designator ":=" Expression .
If = "IF" Condition "THEN" Instructions ["ELSE" Instructions] "END" .
Repeat = "REPEAT" Instructions "UNTIL" Condition "END" .
While = "WHILE" Condition "DO" Instructions "END" .
Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
Write = "WRITE" Expression .
Read = "READ" Designator .

Designator = identifier Selector .
Selector = {"[" ExpressionList "]" | "." identifier} .
IdentifierList = identifier {"," identifier} .
ExpressionList = Expression {"," Expression} .

identifier = letter {letter | digit} .
integer = digit {digit} .
letter = "a" | "b" | .. | "z" | "A" | "B" | .. | "Z" .
digit = "0" | "1" | .. | "9" .
"""

# Abstract Grammar

import sys
from kexception import *
import types as PType
from listeners.klistener import Listener
import ksymboltable as KST
import ktree as KAT

# see @91 Piazza - for more information
strongTokens = ["CONST", "TYPE", "VAR", "IF", "REPEAT", "WHILE", "WRITE", "READ"]
strongDecl = ["CONST", "TYPE", "VAR"]
strongInstr = ["IF", "REPEAT", "WHILE", "WRITE", "READ"]

ERROR_MOVE_OVER = 8 # spec
DEVIL = 5 # anti-spec # change this to 5

class Parser:
  """
  Parser for SIMPLE.
  """
    
  def __init__(self, tokenList):
    """
    Creates Parser objects for a tokenList without any errors.
    """
    assert type(tokenList) is PType.ListType
    self.__tokenList = tokenList

    self.__tokenCounter = 0
    self.__numberOfTokens = len(tokenList)

    self.__listeners = [] # listeners for events in Parser

    # hack code --- needs to behave in future assignments
    self.__toReport = 0

    # symbol table stuff ----------------------------------------

    # create universal scope
    self.__currentScope = KST.Scope()

    # create instance of Integer and add it to the universal scope
    self.__universalInteger = KST.Integer()
    
    try:
      # you can only insert this once!
      self.__currentScope.insert("INTEGER", self.__universalInteger)
    except SymbolTableWriteException, e:
      assert False

    # abstract syntax tree stuff --------------------------------
    # create data structure for a syntax tree
    self.__syntaxTree = []
    self._hasInstructions = False


  # ---------------------------------------------------------------
  # Define Listener functions
  #
  # - (void)registerListener: register listener
  # - (void)removeListener: remove listener
  # - (void)_notifyListeners: dispatch notifications to listeners
  # - (void)_notifyInNonTerminal
  # - (void)_notifyOutNonTerminal
  # - (void)_notifyMatchTerminal
  # ---------------------------------------------------------------


  def registerListener (self, listener):
    """
    Register listener to Parser.
    """

    # Can be extended to be based on a particular event. Not required at this point

    # we have different implementations of DOT and TEXT
    # therefore we can simply have inNonTerminal, outNonTerminal, matchTerminal
    # methods that simply call the respective methods of TextListener and
    # DotListener.

    # This way we don't have to care about the differences in respondence from DOT
    # or TEXT

    assert isinstance(listener, Listener)
    assert listener not in self.__listeners
    self.__listeners.append(listener)


  def removeListener (self, listener):
    """
    Remove listener from the list of listeners for Parser.
    """
    assert isinstance(listener, Listener)
    assert listener in self.__listeners

    self.__listeners.remove(listener)


  def _notifyListeners (self, event = None, context = None):
    """
    Dispatch notifications to all listeners. Notifications are
    specific to their registration.
    """
    assert event is not None # event cannot be default

    # depending on the type of event ---> do notify
    if event is "inNonTerminal":
      self._notifyInNonTerminal(context)
    elif event is "outNonTerminal":
      self._notifyOutNonTerminal()
    elif event is "matchTerminal":
      self._notifyMatchTerminal(context)
    elif event is "handleParserError":
      self._notifyError(context, "parser_type")
    elif event is "handleSymbolTableError":
      self._notifyError(context, "table_type")
    elif event is "handleAbstractSyntaxTreeError":
      self._notifyError(context, "tree_type")
    else:
      assert False


  def _notifyInNonTerminal (self, context):
    """
    Notify listeners to execute inNonTerminal().
    """

    for listener in self.__listeners:
      listener.inNonTerminal(context)


  def _notifyOutNonTerminal (self):
    """
    Notify listeners to execute outNonTerminal().
    """
    
    for listener in self.__listeners:
      listener.outNonTerminal()


  def _notifyMatchTerminal (self, context):
    """
    Notify listeners to execute matchTerminal().
    """

    for listener in self.__listeners:
      listener.matchTerminal(context)


  def _notifyError (self, context, errorType):
    """
    Notify listeners (specifically ErrorListener) to handle error.
    """
    
    assert type(context) is str
    assert type(errorType) is str

    if errorType is "parser_type":
      for listener in self.__listeners:
        listener.handleError(context, "parser_type")
    elif errorType is "table_type":
      for listener in self.__listeners:
        listener.handleError(context, "table_type")
    elif errorType is "tree_type":
      for listener in self.__listeners:
        listener.handleError(context, "tree_type")
    else:
      assert False


  # ---------------------------------------------------------------
  # Define HELPER functions
  #
  # - (BOOL)_tokenExists: is code exists
  # - (void)_counter: increments file read counter
  # - (Token)_currentToken: check if part of language
  # ---------------------------------------------------------------


  def _tokenExists (self):
    """
    Checks if tokens exist.
    """
    if self.__tokenCounter < self.__numberOfTokens:
      return True
    else:
      return False


  def _currentToken (self):
    """
    Returns the current token from the list of tokens.
    """
    tokenValue = self.__tokenList[self.__tokenCounter] # retrieve token
    return tokenValue


  def _counter (self):
    self.__tokenCounter += 1 # increment the token counter


  # ---------------------------------------------------------------
  # Define MATCH functions
  #
  # - (void)_reportError: add error to list based on move_over
  # - (void)_weakError: reports weak error
  # - (void)_nonWeakError: reports strong error + bad stuff
  # - (BOOL)_checkToken: increments file read counter
  # - (Token/void)_matchToken: check if part of language
  # ---------------------------------------------------------------


  def _reportError (self, error):
    """
    Report parser error
    """
    if self.__toReport > 0: # will only add to output if move_over is not breached
      return

    token = self._currentToken()

    # eof should've had a value as well --- goddamn eof. change it soon.
    val = ""
    if token.kind() is "eof":
      val = "eof"
    else:
      val = token.value()

    # generate error message. Don't scare / but don't be discrete. This is a class.
    a = "token value mismatch --- "
    b = str(error) + "; "
    c = "found: '%s' at position: '%d'." % (val, token.startPosition())

    self._notifyListeners("handleParserError", a + b + c) # add to error list
    
    # after you report the error; set the counter again
    self.__toReport = ERROR_MOVE_OVER


  def _weakError (self, error):
    """
    Weak Error implementation. This simply adds the error to the list of errors.
    Major error checking is part of the match function.
    """
    self._reportError(error) # report error


  def _nonWeakError (self, error):
    """
    Non Weak Error implementation. Only called when the compiler has acknowledged
    move_over tokens (number of tokens that must be ignored in case of an error).
    Restarts parsing to the state of next Instruction OR Declaration depending on the
    type of strong token discovered.
    """
    token = self._currentToken()
    self._reportError(error)

    # while token is not EOF or is not in the strong tokens list
    #   move on and check for Decl and Instr
    try:
      while (self._currentToken().kind() is not "eof") and\
            (self._currentToken().value() not in strongTokens):
        # keep moving
        self._counter()

        # decrement report counter
        if self.__toReport > 0: # still need to count for this
          self.__toReport -= 1

        # depending on the type of token discovered, do either Declarations or Instructions
        if self._currentToken().value() in strongDecl:
          self._Declarations()
        elif self._currentToken().value() in strongInstr:
          self._Instructions()
        # else:
        #   exit()
        #   assert False
    # because this is now are code of execution stream --> errors will be here.
    except TokenMismatchException, e:
      self._nonWeakError(e)


  def _checkToken (self, expectedTokenList):
    """
    Check if token is one from the expected token list.
    """
    kind = "" 

    # temp kind value --- why did I classify so much
    # if possible, change scanner :(

    # set this straight ---> change code in future assignments :: Maybe?
    if self._currentToken().kind() == "identifier":
      kind = "identifier"
    elif self._currentToken().kind() == "integer":
      kind = "integer"
    elif self._currentToken().kind() == "symbol":
      kind = self._currentToken().value()
    elif self._currentToken().kind() == "keyword":
      kind = self._currentToken().value()
    elif self._currentToken().kind() == "eof":
      kind = "eof"
    else:
      assert False

    # look for kind in token list
    return True if kind in expectedTokenList else False


  def _matchToken (self, expectedTokenList):
    """
    Match a terminal value in the program as specified in the grammar.
    """
    if self.__tokenCounter == len(self.__tokenList):
      return # don't check if it's gone crazy

    token = self._currentToken()
    if token.value() == "BEGIN" and token.kind() == "keyword":
      self._hasInstructions = True

    # if token is fine ---> notify and move ahead
    if self._checkToken(expectedTokenList):
      if token.kind() is not "eof":
        self._notifyListeners("matchTerminal", token)
      self._counter() # increment counter after token match
      
      if self.__toReport > 0:
        self.__toReport -= 1
      
      return token
    else:
      return False


  # ---------------------------------------------------------------
  # Visitor patter implemented. Now return the main scope that
  # carries everthing
  # ---------------------------------------------------------------

  def majorScope (self):
    """
    Returns the current scope of the SIMPLE program.
    """
    return self.__currentScope


  def syntaxTree (self):
    """
    Returns the abstract tree for the SIMPLE program.
    """
    return self.__syntaxTree


  def _handleSymbolTableWriteException (self, exceptionMessage, tokenValue, startPosition, endPosition):
    """
    Handles notification to error handler in case there is a duplicate write
    to the symbol table.
    """
    rvalue = self.__currentScope.retrieve(tokenValue)
    rspos = rvalue.startPosition()
    repos = rvalue.endPosition()

    err = \
    "; previous declaration at location '%d' to '%d'"\
    "; new declaration at location '%d' to '%d'." % (rspos, repos, startPosition, endPosition)
    err = str(exceptionMessage) + err

    self._notifyListeners("handleSymbolTableError", err)


  def _handleSymbolTableReadException (self, exceptionMessage, token, generate):
    """
    Handles notification to error handler in case the entry does not exist in the
    symbol table. As the value is not available in the symbol table, it creates a new
    entry and returns it to the calling function.
    """
    assert (generate is True) or (generate is False)

    # notify error
    err = str(exceptionMessage) + "; at location '%d'." % (token.startPosition())
    self._notifyListeners("handleSymbolTableError", err)

    # create new entry
    if generate:
      newEntry = KST.InvalidType(token.startPosition(), token.endPosition()) # create new entry
      self.__currentScope.insert(token.value(), newEntry) # insert entry
      return newEntry


  # ---------------------------------------------------------------
  # Program = "PROGRAM" identifier ";" Declarations
  #   ["BEGIN" Instructions] "END" identifier "." .
  # ---------------------------------------------------------------


  def _Program (self):
    """
    Program = "PROGRAM" identifier ";" Declarations
        ["BEGIN" Instructions] "END" identifier "." .
    """

    # notify start of program
    self._notifyListeners("inNonTerminal", "Program")

    programName = ""
    bspos = 0 # start of program declaration start position
    fspos = 0 # end of program declaration start position

    # strong error if "PROGRAM" not found
    token = self._matchToken(["PROGRAM"])
    if not token:
      raise TokenMismatchException("expected 'PROGRAM' at the start of the program")
    bspos = token.startPosition() # get start position

    # create the actual program 'scope' here
    self.__currentScope = KST.Scope(self.__currentScope)

    # look for identifier --- program name
    token = self._matchToken(["identifier"])
    if not token:
      raise TokenMismatchException("expected an identifier to name the program at the start of the program")

    # should match the END X. thing
    programName = token.value()

    if not self._matchToken([";"]):
      self._weakError("expected ';' to end the program declaration statement")
    
    self._Declarations() # do symbol table

    # ["BEGIN" Instructions]
    if self._checkToken(["BEGIN"]):
      self._matchToken(["BEGIN"])
      # ------------------------------------------------------------------------
      self.__syntaxTree = self._Instructions() # big ass syntax tree, here...

    token = self._matchToken(["END"])
    if not token: 
      self._weakError("expected 'END' to end the current program")
      fspos = 'undefined'
    else:
      fspos = token.startPosition()

    token = self._matchToken(["identifier"])
    if not token:
      raise TokenMismatchException("expected identifier to match the end of the program")

    # if mismatch in PROGRAM X; END X.
    if token.value() != programName:
      err = \
      "program name '%s' at start of program (location: '%s') is not same as program name '%s' "\
      "at end of program (location: '%s')." % (programName, bspos, token.value(), fspos)
      self._notifyListeners("handleSymbolTableError", err)

    if not self._matchToken(["."]): # this is specific to PROGRAM
      self._weakError("expected '.' to end the SIMPLE program")

    self._notifyListeners("outNonTerminal")


  # ---------------------------------------------------------------
  # Instructions = Instruction {";" Instruction} .
  # Instruction = Assign | If | Repeat | While | Read | Write .
  # Assign = Designator ":=" Expression .
  # If = "IF" Condition "THEN" Instructions ["ELSE" Instructions] "END" .
  # Repeat = "REPEAT" Instructions "UNTIL" Condition "END" .
  # While = "WHILE" Condition "DO" Instructions "END" .
  # Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
  # Write = "WRITE" Expression .
  # Read = "READ" Designator .
  # ---------------------------------------------------------------


  def _Instruction (self):
    """
    Instruction = Assign | If | Repeat | While | Read | Write .
    """
    self._notifyListeners("inNonTerminal", "Instruction")
    nextInstruction = None

    if self._checkToken(["identifier"]): # all other are keywords duh!
      nextInstruction = self._Assign()
    elif self._checkToken(["IF"]): # type: "keyword"
      nextInstruction = self._If()
    elif self._checkToken(["REPEAT"]):
      nextInstruction = self._Repeat()
    elif self._checkToken(["WHILE"]):
      nextInstruction = self._While()
    elif self._checkToken(["READ"]):
      nextInstruction = self._Read()
    elif self._checkToken(["WRITE"]):
      nextInstruction = self._Write()
    else:
      raise TokenMismatchException(
        "expected one of 'assignment using an identifier', "
        "'IF', 'WHILE', 'REPEAT', 'READ', 'WRITE'"
      )

    self._notifyListeners("outNonTerminal")
    return nextInstruction


  def _Instructions (self):
    """
    Instructions = Instruction {";" Instruction} .
    """
    self._notifyListeners("inNonTerminal", "Instructions")

    # create a sequence of instructions
    # first -> first.next() -> first.next().next() -> ...
    # get all instructions: return the first instruction in the sequence

    previousInstruction = self._Instruction() # first instruction in the current instruction/scope
    if not previousInstruction:
      return None

    firstInstruction = previousInstruction # pointer to first instruction

    while self._checkToken([";"]):
        self._matchToken([";"])
        i = self._Instruction()
        if not i: # if instruction is not valid:
          return None
        previousInstruction.setNext(i) # store next instruction
        previousInstruction = previousInstruction.next() # update instruction

    self._notifyListeners("outNonTerminal")
    return firstInstruction


  def _Assign (self):
    """
    Assign = Designator ":=" Expression .
    """
    self._notifyListeners("inNonTerminal", "Assign")
    madness = False

    designator = self._Designator() # no need to check if exists, there is a type check below

    token = self._matchToken([":="])
    if not token:
      raise TokenMismatchException("expected ':=' in assignment")
    
    # needs to be of type location variable
    if not isinstance(designator, KAT.Location):
      err = "expected variable location for designator in 'ASSIGN' at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True

    expression = self._Expression()
    if not expression:
      return None # do not afford to let expressionType() operate on expression
    
    if designator.expressionType() != expression.expressionType():
      err = "expected both sides in 'assign' to be type equivalent at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True

    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.Assign(designator, expression)


  def _If (self):
    """
    If = "IF" Condition 
      "THEN" Instructions ["ELSE" Instructions] "END" .
    """
    self._notifyListeners("inNonTerminal", "If")
    madness = False

    token = self._matchToken(["IF"])
    
    condition = self._Condition()
    madness = True if not condition else madness
    
    if not self._matchToken(["THEN"]):
      raise TokenMismatchException("expected 'THEN' after condition in 'IF'")
    
    trueInstruction = self._Instructions() # this must be availablee
    madness = True if not trueInstruction else madness
    falseInstruction = None # this can be empty

    # ["ELSE" Instructions]
    if self._checkToken(["ELSE"]):
      self._matchToken(["ELSE"])
      falseInstruction = self._Instructions()
      madness = True if not falseInstruction else madness # now this must exist here

    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'IF' instruction.")

    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.If(condition, trueInstruction, falseInstruction)


  def _Repeat (self):
    """
    Repeat = "REPEAT" Instructions 
      "UNTIL" Condition "END" .
    """
    self._notifyListeners("inNonTerminal", "Repeat")
    madness = False

    self._matchToken(["REPEAT"])

    instruction = self._Instructions()
    if not instruction:
      madness = True

    if not self._matchToken(["UNTIL"]):
      raise TokenMismatchException("expected 'UNTIL' with 'REPEAT' instruction")

    condition = self._Condition()
    if not condition:
      madness = True

    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'REPEAT' instruction")

    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.Repeat(condition, instruction)


  def _While (self):
    """
    While = "WHILE" Condition
      "DO" Instructions "END" .
    """
    self._notifyListeners("inNonTerminal", "While")
    madness = False

    self._matchToken(["WHILE"])
    
    condition = self._Condition()
    madness = True if not condition else madness # check here
    
    if not self._matchToken(["DO"]):
      raise TokenMismatchException("expected 'DO' with 'WHILE' instruction")

    instruction = self._Instructions()
    madness = True if not instruction else madness

    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'WHILE' instruction")

    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.While(condition, instruction)


  def _Condition (self):
    """
    Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
    """
    self._notifyListeners("inNonTerminal", "Condition")
    madness = False

    leftExpression = self._Expression() # retrieve left expression
    madness = True if not leftExpression else madness

    matchTokenList = ["=", "#", "<", ">", "<=", ">="]
    token = self._matchToken(matchTokenList)
    if not token:
      raise TokenMismatchException("expected one of '=', '#', '<', '>', '<=', '>=' in condition")

    rightExpression = self._Expression() # retrieve other side of the expression
    madness = True if not rightExpression else madness

    # now the only thing that's left is to generate the tree; so we can return if it's nonsense
    if madness:
      return None

    # just check the type; we can only match integers
    if not (isinstance(leftExpression.expressionType(), KST.Integer) and
            isinstance(rightExpression.expressionType(), KST.Integer)):
      err =  \
      "the expression on the left and right of relation '%s' at location %d" \
      "do not match; the expressions must be of type 'integer'." % (token.value(), token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True
    
    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.Condition(token.value(), leftExpression, rightExpression)


  def _Write (self):
    """
    Write = "WRITE" Expression .
    """
    self._notifyListeners("inNonTerminal", "Write")
    madness = False

    token = self._matchToken(["WRITE"])

    expression = self._Expression()
    if not expression:
      return None # we can simply return here as there is no point in checking for type

    if not isinstance(expression.expressionType(), KST.Integer):
      err = "expected expression of type 'integer' in 'WRITE' at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True
    
    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.Write(expression)


  def _Read (self):
    """
    Read = "READ" Designator .
    """
    self._notifyListeners("inNonTerminal", "Read")
    madness = False

    token = self._matchToken(["READ"])

    designator = self._Designator()
    if not designator:
      return None

    # this should be of type location
    if not isinstance(designator, KAT.Location):
      err = "expected a variable location in 'READ' at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True

    # now it can only read an Integer value
    if not isinstance(designator.expressionType(), KST.Integer):
      err = "expected a variable location of type 'integer' in 'READ' at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      madness = True

    self._notifyListeners("outNonTerminal")
    return None if madness else KAT.Read(designator)


  # ---------------------------------------------------------------
  # Expression = ["+"|"-"] Term {("+"|"-") Term} .
  # Term = Factor {("*"|"DIV"|"MOD") Factor} .
  # Factor = integer | Designator | "(" Expression ")" .
  # ---------------------------------------------------------------


  def _Expression (self):
    """
    Expression = ["+"|"-"] Term {("+"|"-") Term} .
    operator = None
    if it is + or -:
      operator = token.value
    previous = self._Term()
    if operator == '-':
      const_0 = Constant(0)
      previous = constant_folding( const_0 - previous )

    while:
      operator = match()
      next = self._Term()
      previous = constant_folding(previous operator next )
    """
    self._notifyListeners("inNonTerminal", "Expression")
    madness = False
    npos = 0

    operator = None
    if self._checkToken(["+", "-"]):
      token = self._matchToken(["+", "-"])
      operator = token.value()
      npos = token.startPosition()

    previous = self._Term()
    if not previous:
      return None

    # do negation stuff
    if operator == "-":
      previous = KAT.Number(KST.Constant(self.__universalInteger, 0 - previous.constant().value(), npos, npos))

    while self._checkToken(["+", "-"]):
      token = self._matchToken(["+", "-"])
      spos = token.startPosition()
      epos = token.endPosition()
      operator = token.value()
      next = self._Term() # get the next term
      if not next:
        return None
      
      # constant folding bullshit?
      if isinstance(previous, KAT.Number) and isinstance(next, KAT.Number):
        l = previous.constant().value()
        r = next.constant().value()
        
        if token.value() == "+":
          previous = KAT.Number(KST.Constant(self.__universalInteger, l + r, spos, epos))
        elif token.value() == "-":
          previous = KAT.Number(KST.Constant(self.__universalInteger, l - r, spos, epos))
        else:
          assert False
      
      else: # if constants not the case ---> oh well! # check if integer types
        if not (isinstance(previous.expressionType(), KST.Integer) and
                isinstance(next.expressionType(), KST.Integer)):
          err = "operation at location %d requires operands to be of type integers." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          madness = True

        if not madness:
          previous = KAT.Binary(token.value(), previous, next)

    return None if madness else previous


  def _Term (self):
    """
    Term = Factor {("*"|"DIV"|"MOD") Factor} .
    """
    self._notifyListeners("inNonTerminal", "Term")
    madness = False
    
    previous = self._Factor() # get factor
    if not previous:
      return None

    while self._checkToken(["*", "DIV", "MOD"]):
      token = self._matchToken(["*", "DIV", "MOD"])
      next = self._Factor() # get the next factor
      if not next:
        return None

      # constant folding bullshit...
      if isinstance(previous, KAT.Number) and isinstance(next, KAT.Number):
        l = previous.constant().value() # get left operand
        r = next.constant().value() # get right operand

        try:
          if token.value() == "*":
            previous = KAT.Number(KST.Constant(self.__universalInteger, l * r, 0, 0))
          elif token.value() == "DIV":
            previous = KAT.Number(KST.Constant(self.__universalInteger, l / r, 0, 0))
          elif token.value() == "MOD":
            previous = KAT.Number(KST.Constant(self.__universalInteger, l % r, 0, 0))
          else:
            assert False
        except ZeroDivisionError, e:
            err = str(e) + " at location %d." % (token.startPosition())
            self._notifyListeners("handleAbstractSyntaxTreeError", err)
            return None
      
      else:
        # we have dealt with constant values;
        # now if they are something else (like vars?), just check their types
        # they can only be integer types; and not like arrays or something else
        if not (isinstance(previous.expressionType(), KST.Integer) or
                isinstance(next.expressionType(), KST.Integer)):
          err = "the expression at location %d is only valid for integers" % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          return None

        previous = KAT.Binary(token.value(), previous, next)

    self._notifyListeners("outNonTerminal")

    return None if madness else previous


  def _Factor (self):
    """
    Factor = integer | Designator | "(" Expression ")" .
    """
    self._notifyListeners("inNonTerminal", "Factor")
    
    node = None
    if self._checkToken(["integer"]):
      token = self._matchToken(["integer"]) # move on
      spos = token.startPosition()
      epos = token.endPosition()
      node = KAT.Number(KST.Constant(self.__universalInteger, token.value(), spos, epos)) # create node
    elif self._checkToken(["identifier"]):
      node = self._Designator() # if None, will travel back anyway
    elif self._checkToken(["("]):
      self._matchToken(["("])
      node = self._Expression() # if None, will travel back anyway
      if not self._matchToken([")"]):
        self._weakError("expected ')' to close the expression defined")
    else:
      raise TokenMismatchException("expected an integer or identifier or '(' or read the syntax first")

    self._notifyListeners("outNonTerminal")
    return node


  # ---------------------------------------------------------------
  # Type = identifier | "ARRAY" Expression "OF" Type |
  #   "RECORD" {IdentifierList ":" Type ";"} "END" .
  # ---------------------------------------------------------------


  def _Type (self):
    """
    Type = identifier | "ARRAY" Expression "OF" Type |
      "RECORD" {IdentifierList ":" Type ";"} "END" .
    """
    self._notifyListeners("inNonTerminal", "Type")

    typeValue = None

    # 'identifier' (lonely) ----------------------------------------------

    if self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"])
      tokenValue = token.value()
      spos = token.startPosition()
      epos = token.endPosition()

      # get value from symbol table and handle exception
      try:
        entry = self.__currentScope.retrieve(tokenValue)
        if isinstance(entry, KST.Type):
          typeValue = entry
        else:
          err = "'%s' is not of type 'TYPE'; at location '%d'." % (tokenValue, spos)
          self._notifyListeners("handleSymbolTableError", err)

          # something with the same name is already in the symbol table
          # can't help it! # do I need this? WTH!
          typeValue = None

      except SymbolTableReadException, e:
        # now that the identifier doesn't exist in the symbol table
        # we need to create something that can be used later
        typeValue = self._handleSymbolTableReadException(e, token, generate=True)

    # array case -------------------------------------------------------
    
    elif self._checkToken(["ARRAY"]):
      madness = False
      spos = 0
      epos = 0
      
      token = self._matchToken(["ARRAY"])
      spos = token.startPosition()

      expression = self._Expression() # get length of array
      
      # size needs to be of type constant;
      if not isinstance(expression, KAT.Number):
        err = "expected a constant value for size of array at location %d." % (spos)
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        madness = True

      # also size needs to be greater than zero; ObvioUS!
      if expression.constant().value() <= 0:
        err = "expected constant 'greater' than 0 for a legal array size at location %d." % (spos)
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        madness = True
      
      if not self._matchToken(["OF"]):
        raise TokenMismatchException("expected 'OF' in ARRAY declaration")
      
      # get element type
      elementType = self._Type()

      # it is possible that elementType is None # based on the received Type

      if not madness:
        if not isinstance(elementType, KST.Entry):
          typeValue = None
        else:
          epos = elementType.endPosition()
          typeValue = KST.Array(elementType, expression.constant().value(), spos, epos)
    
    # record case -----------------------------------------------------

    elif self._checkToken(["RECORD"]):
      token = self._matchToken(["RECORD"])
      spos = token.startPosition()
      epos = token.startPosition()

      # create new scope for Record
      self.__currentScope = KST.Scope(self.__currentScope)

      while self._checkToken("identifier"):        
        identifierList = self._IdentifierList() # get identifier list

        token = self._matchToken([":"])
        if not token:
          raise TokenMismatchException("expected ':' in RECORD identifier declaration")
          # self._weakError("expected ':' in RECORD identifier declaration")
          # making this a weak error could be helpful in adding values to the symbol
          # table even if there were inherent issues with the syntax.
        else:
          epos = token.endPosition() # get epos incase the next epos has error
        
        # type of identifiers
        elementType = self._Type()

        token = self._matchToken([";"])
        if not token:
          self._weakError("expected ';' in RECORD declaration in TYPE")
        else:
          epos = token.endPosition() # get end position

        # now error checking is complete, make variables and add them to list
        # identifier in the loop below is a token
        
        # issue with the returned elementType
        # GET RID OF THIS HACK -----------
        if elementType is None:
          if not self._matchToken(["END"]):
            self._weakError("expected 'END' at the end of RECORD declaration in TYPE")
          return None
          break

        for tokenId in identifierList:
          # create a variable
          v = KST.Variable(tokenId.value(), elementType, spos, epos)

          # insert value in symbol table (duplicate possible)
          try:
            self.__currentScope.insert(tokenId.value(), v)
          except SymbolTableWriteException, e:
            self._handleSymbolTableWriteException(e, tokenId.value(), spos, epos)

      # end of record type --- symbol table stuff
      tscope = self.__currentScope
      self.__currentScope = self.__currentScope.outerScope()
      tscope.setOuterScope(None) # break connection for RECORD
      typeValue = KST.Record(tscope, spos, epos)

      if not self._matchToken(["END"]):
        self._weakError("expected 'END' at the end of RECORD declaration in TYPE")

    else: # if Type gets nothing from above
      raise TokenMismatchException("expected one of identifier, ARRAY, RECORD")

    self._notifyListeners("outNonTerminal")

    # return the type
    return typeValue


  # ---------------------------------------------------------------
  # Declarations = { ConstDecl | TypeDecl | VarDecl } .
  # ConstDecl = "CONST" {identifier "=" Expression ";"} .
  # TypeDecl = "TYPE" {identifier "=" Type ";"} .
  # VarDecl = "VAR" {IdentifierList ":" Type ";"} .
  # ---------------------------------------------------------------


  def _Declarations (self):
    """
    Declarations = { ConstDecl | TypeDecl | VarDecl } .
    """
    self._notifyListeners("inNonTerminal", "Declarations")

    # check current token to be of type CONST TYPE VAR
    while self._checkToken (["CONST", "TYPE", "VAR"]):
      if self._checkToken(["CONST"]):
        self._ConstDecl()
      elif self._checkToken(["TYPE"]):
        self._TypeDecl()
      elif self._checkToken(["VAR"]):
        self._VarDecl()
      else:
        assert False

    # end declarations
    self._notifyListeners("outNonTerminal")


  def _ConstDecl (self):
    """
    ConstDecl = "CONST" {identifier "=" Expression ";"} .
    """
    self._notifyListeners("inNonTerminal", "ConstDecl")

    # will match CONST (just incrementing the counter)
    token = self._matchToken(["CONST"])
    spos = token.startPosition()

    # assume you found: CONST con = 47 (example)
    # also assume 5 instead of 47; don't care about the parsed expression
    while self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"])
      epos = token.endPosition()
      name = token.value() # get the name of the CONST declaration

      token = self._matchToken(["="])
      if not token:
        raise TokenMismatchException("expected '=' in CONST declaration")
      epos = token.endPosition() # in case further epos fail
      
      expression = self._Expression()
      
      token = self._matchToken([";"])
      if not token:
        self._weakError("expected ';' at the end of CONST declaration")
      else:
        epos = token.endPosition() # get the end position of symbol ';'

      # expression needs to be of type number; otherwise you can't store the value in
      # the symbol table
      if not isinstance(expression, KAT.Number):
        err = "expected a constant in constant declaration at location %d." % (spos)
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        return None
      else:
        try: # insert value in symbol table (duplicate possible)
          c = KST.Constant(self.__universalInteger, expression.constant().value(), spos, epos)
          self.__currentScope.insert(name, c)
        except SymbolTableWriteException, e:
          self._handleSymbolTableWriteException(e, name, spos, epos)

    self._notifyListeners("outNonTerminal")


  def _TypeDecl (self):
    """
    TypeDecl = "TYPE" {identifier "=" Type ";"} .
    """
    self._notifyListeners("inNonTerminal", "TypeDecl")

    token = self._matchToken(["TYPE"])
    spos = token.startPosition()
    epos = 0

    while self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"])
      name = token.value()

      token = self._matchToken(["="])
      if not token:
        raise TokenMismatchException("expected '=' in TYPE declaration")
      epos = token.endPosition() # in case the next epos fails
      
      elementType = self._Type()

      token = self._matchToken([";"])
      if not token:
        self._weakError("expected ';' at the end of TYPE declaration")
      else:
        epos = token.endPosition() # get end position

      if elementType is None:
        return None
      else:
        # if everything is good, add to symbol table
        try:
          self.__currentScope.insert(name, elementType)
        except SymbolTableWriteException, e:
          self._handleSymbolTableWriteException(e, name, spos, epos)

    self._notifyListeners("outNonTerminal")


  def _VarDecl (self):
    """
    VarDecl = "VAR" {IdentifierList ":" Type ";"} .
    """
    self._notifyListeners("inNonTerminal", "VarDecl")

    token = self._matchToken(["VAR"])
    spos = token.startPosition()
    epos = token.startPosition() # let it be this in case we don't have ';'

    while self._checkToken(["identifier"]):

      # get the identifier list
      identifierList = self._IdentifierList()
      
      token = self._matchToken([":"])
      if not token:
        raise TokenMismatchException("expected ':' in VAR declaration OR check your instruction declaration")
      
      epos = token.endPosition() # get new end position

      elementType = self._Type() # return element type
      
      token = self._matchToken([";"])
      if not token:
        self._weakError("expected ';' at the end of VAR declaration")
      else:
        epos = token.endPosition()

      if elementType is None:
        return None

      for tokenId in identifierList:
        v = KST.Variable(tokenId.value(), elementType, spos, epos)
        
        # insert value in the symbol table (duplicate possible)
        try:
            self.__currentScope.insert(tokenId.value(), v)
        except SymbolTableWriteException, e:
            self._handleSymbolTableWriteException(e, tokenId.value(), spos, epos)

    # done with variables
    self._notifyListeners("outNonTerminal")


  # ---------------------------------------------------------------
  # Designator = identifier Selector .
  # Selector = {"[" ExpressionList "]" | "." identifier} .
  # IdentifierList = identifier {"," identifier} .
  # ExpressionList = Expression {"," Expression} .
  # ---------------------------------------------------------------


  def _Designator (self):
    """
    Designator = identifier Selector .
    """
    self._notifyListeners("inNonTerminal", "Designator")

    token = self._matchToken(["identifier"]) # identifier exists
    
    # look up 'token.value' in symbol table
    # we don't know about the selector; but we need to make an AST
    # now each of the things that we call needs to return an AST
    entry = None
    try:
      entry = self.__currentScope.retrieve(token.value()) # get the value from the symbol table
    except SymbolTableReadException, e:
      self._handleSymbolTableReadException(e, token, generate=False) # do not generate new value

    # check the type of entry: can only be a variable or a constant (no types)
    node = None
    if isinstance(entry, KST.Variable):
      node = KAT.Variable(entry)
    elif isinstance(entry, KST.Constant):
      node = KAT.Number(KST.Constant(self.__universalInteger, entry.value(), 0, 0))
      # node = KAT.Number(entry)
    else:
      err = "expected declaration to be of type 'variable' or 'constant' at location %d." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      return None

    self._notifyListeners("outNonTerminal")
    return self._Selector(node) # won't go here if constant

  
  def _Selector (self, selector):
    """
    Selector = {"[" ExpressionList "]" | "." identifier} .
    """
    self._notifyListeners("inNonTerminal", "Selector")
    madness = False
    selected = selector # (just so that we maintain a copy outside the scope below)

    while (self._checkToken(["["]) or self._checkToken(["."])):
      # "[" ExpressionList "]" -------------------------------------------
      if self._checkToken(["["]):
        token = self._matchToken(["["])
        
        #has to be array type # do array stuff here--- array_variable[index_variable_expression]
        if not isinstance (selected.expressionType(), KST.Array):
          err = "expected an array type for indexing at location %d." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)

        # get expressions from expression list
        expressionList = self._ExpressionList()

        # now index expression checking
        for expression in expressionList:
          if expression: # if the expression is not None; i.e. it's valid
            if not isinstance(expression.expressionType(), KST.Integer):
              err = "expected an integer value as an index to an array at location %d." % (token.startPosition())
              self._notifyListeners("handleAbstractSyntaxTreeError", err)
              madness = True
            else:
              selected = KAT.Index(selected, expression)

        if not self._matchToken(["]"]):
          self._weakError("expected ']' to close the expression list")

      # "." identifier --------------------------------------------------
      elif self._checkToken(["."]):
        token = self._matchToken(["."])

        # now you need to make sure that the selector (expression type) is a record type
        if not isinstance(selected.expressionType(), KST.Record):
          err = "expected a record type to retrieve a field value at location %d." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          madness = True

        token =  self._matchToken(["identifier"])
        if not token:
          raise TokenMismatchException("expected identifier in the selector")
        
        # check if variable exits in the scope of the record
        variable = None
        try:
          variable = selected.expressionType().scope().retrieve(token.value())
        except SymbolTableReadException, e:
          self._handleSymbolTableReadException(e, token, generate=False)

        # now if the field is in the symbol table, check if it is of type variable
        if variable:
          if not isinstance(variable, KST.Variable):
            err = "expected a variable type for a record field at location %d." % (token.startPosition())
            self._notifyListeners("handleAbstractSyntaxTreeError", err)
            return None
          else:
            selected = KAT.Field(selected, KAT.Variable(variable))

      # not possible ---------------------------------------------------
      else:
        assert False

    self._notifyListeners("outNonTerminal")
    return None if madness else selected


  def _IdentifierList (self):
    """
    IdentifierList = identifier {"," identifier} .

    Returns a list of identifiers in the current context.
    """
    self._notifyListeners("inNonTerminal", "IdentifierList")
    
    # create an identifier list to hold all tokens
    identifiers = []
    identifiers.append(self._matchToken(["identifier"])) # only add tokens, not token values
    
    while self._checkToken([","]):
      self._matchToken([","])
      
      token = self._matchToken(["identifier"])
      if not token:
        raise TokenMismatchException("expected an identifier to continue the list of identifiers") 
      
      identifiers.append(token)

    # end of identifier list
    self._notifyListeners("outNonTerminal")

    return identifiers # return identifier list
  

  def _ExpressionList (self):
    """
    ExpressionList = Expression {"," Expression} .
    """
    self._notifyListeners("inNonTerminal", "ExpressionList")

    expressions = []
    expressions.append(self._Expression())

    while self._checkToken([","]):
      self._matchToken([","])

      expressions.append(self._Expression())

    self._notifyListeners("outNonTerminal")

    return expressions # return a list of ASTs for several expressions in a row


  # --------------- do parse ---------------
  def parse (self):
    """
    Public method to perform parsing using the Parser.
    """
    # catch mismatch exception here
    try:
      self._Program()
    except TokenMismatchException, e:
      self._nonWeakError(e)
    
    self._matchToken(["eof"])

