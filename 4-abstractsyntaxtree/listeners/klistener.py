#!/usr/bin/python
#
# klistener.py
#
# Created by Kartik Thapar on 02/24/2013 at 23:15:35
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

class Listener(object):
  """
  Listener: generic listener class.
  """

  def inNonTerminal (self, nterm):
    """
    When the execution enters a non terminal execution block.
    """
    pass


  def outNonTerminal (self):
    """
    When the execution exits a non terminal execution block.
    """
    pass


  def matchTerminal (self, token):
    """
    When the code matches a token --- terminal value.
    """
    pass


  def handleError (self, error, errorType):
    """
    Error handler for the Parser.
    """
    pass
