#!/usr/bin/python
#
# kerrorlistener.py
#
# Created by Kartik Thapar on 02/26/2013 at 21:57:21
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Implements class error listener for the Parser.

class ErrorListener (Listener)
class ParserErrorListener (ErrorListener)
class SymbolTableErrorListener (ErrorListener)
"""

from klistener import Listener as KListener

class ErrorListener (KListener):
  """
  Error Listener: observes error events.
  """

  def __init__(self):
    self.__errorOutput = []


  def addError (self, error):
    """
    Add an error to the error output list.
    """
    self.__errorOutput.append(error)


  def errorOutput (self):
    """
    Returns the error list to the driver
    """
    return self.__errorOutput


  def isError (self):
    """
    Public method to check if errors exist.
    """
    if self.__errorOutput:
      return True
    else:
      return False


class ParserErrorListener (ErrorListener):
  """
  Error listener for Parser errors. Only syntactic errors recorded.
  """
  
  def handleError (self, error, errorType):
    """
    Handle parser errors that only relate to syntactic parsing.
    """
    if errorType == "parser_type":
      self.addError(error)
      return


class SymbolTableErrorListener (ErrorListener):
  """
  Error listener for Parser --- Symbol Table erors. Only semantic errors are recorded.
  """
  
  def handleError (self, error, errorType):
    """
    Handle symbol table (type) errors in the Parser.
    """
    if errorType == "table_type":
      self.addError(error)
      return


class AbstractSyntaxTreeErrorListener (ErrorListener):
  """
  Error listener for Parser --- Symbol Table erors. Only semantic errors are recorded.
  """

  def handleError (self, error, errorType):
    """
    Handle abstract syntax tree errors in the Parser.
    """
    if errorType == "tree_type":
      self.addError(error)
      return

  