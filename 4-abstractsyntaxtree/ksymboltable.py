#
# ksymboltable.py
#
# Created by Kartik Thapar on 03/09/2013 at 06:41:16
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Implements various aspects of the Symbol Table.
"""

import types as PType
from kexception import *
from visitors.symbol_table.kvisitor import Visitor

class Scope():
  """
  Define a context/scope for variables, constants and types
  """
  
  def __init__(self, outerScope = None): # outer = None for top most
    """
    Creates Scope objects. In some cases, the parent scope is also provided if
    it exists.
    """
    assert isinstance(outerScope, Scope) or outerScope is None # default

    # need parent scope for all scopes
    self.__outerScope = outerScope

    # define a dictionary data for the symbol table --- why bother!
    self.__symbolTable = {}

  # outerScope is a private variable --- so it needs it's setters and getters
  # why I made it private? Absolutely no idea; but it's ok!

  def outerScope (self):
    """
    Return the outer scope for the current scope
    """
    return self.__outerScope


  def setOuterScope (self, scope):
    """
    Setter for scope
    """
    assert isinstance(scope, Scope) or scope is None
    self.__outerScope = scope


  def insert (self, entryName, entry):
    """
    Insert value in the symbol table
    """
    assert isinstance (entry, Entry)
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0

    if entryName in self.__symbolTable:
      raise SymbolTableWriteException("you already declared '%s'" % (entryName))
    else:
      self.__symbolTable[entryName] = entry


  def retrieve (self, entryName):
    """
    Retrieve values from the symbol table.
    """
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0

    if entryName in self.__symbolTable: # find in current scope
      return self.__symbolTable[entryName]
    elif self.outerScope() != None: # find in outer scope(s)
      return self.outerScope().retrieve(entryName)
    else:
      raise SymbolTableReadException("you never declared '%s'" % (entryName))


  def accept (self, visitor):
    """
    Found a Scope type in the symbol table.
    """
    return visitor.visitScope(self)


  def symbolTable (self):
    """
    Returns the symbol table of the Scope object.
    """
    return self.__symbolTable


  def __str__( self ):
    # official log function

    string = ""
    for v in self.__symbolTable:
      string += v + " => " + str(self.__symbolTable[v]) + "\n"
    return string

# ------------------------------------------------------------------------------

class Entry:
  """
  Base class for all symbol table entries.
  """

  def __init__ (self, position):
    # need to record positions for errors
    assert type(position) is PType.IntType
    self.__position = position


  def position (self):
    """
    Returns the position of the entry.
    """
    return self.__position


  def accept (self, visitor):
    """
    For visitior.
    """
    assert isinstance(visitor, Visitor)
    pass


class Constant (Entry):
  """
  Constants in SIMPLE.
  """

  def __init__ (self, ktype, value, position):
    assert isinstance(ktype, Type)
    assert type(value) is PType.IntType # for now consts are ints?
    Entry.__init__(self, position)
    self.__elementType = ktype # get type of the constant value (right now they are INTEGER types)
    self.__value = value # get value (this is set as DEVIL)


  def elementType (self):
    """
    Returns the type of Constant object.
    """
    return self.__elementType


  def value (self):
    """
    Returns the value of the Constant object
    """
    return self.__value

  
  def __str__ (self):
    """
    Display for constant objects.
    """
    return "CONST<type: %s; value: %d>" % (str(self.__elementType), self.__value)


  def accept (self, visitor):
    """
    Found a Constant type in the symbol table.
    """
    return visitor.visitConstant(self)



class Variable (Entry):
  """
  Variables in SIMPLE
  """
  def __init__ (self, name, elementType, position):
    assert type(name) is str
    assert len(name) > 0
    assert isinstance(elementType, Type)
    self.__name = name
    Entry.__init__(self, position)

    self.__elementType = elementType # get the type of Variable
  

  def name (self):
    """
    Returns the name of the variable
    """
    return self.__name


  def __str__ (self):
    """
    Display for Variable objects.
    """
    return "VAR<type: %s>" % (str(self.__elementType ))


  def elementType (self):
    """
    Public method to return the type of the element.
    """
    return self.__elementType


  def accept (self, visitor):
    """
    Found a Variable type in the symbol table.
    """
    return visitor.visitVariable(self)



class Type (Entry):
  """
  Types in SIMPLE.
  """
  def __init__ (self, position):
    Entry.__init__(self, position)

  def __str__ (self):
    return "Type<>"


class Integer (Type):
  """
  Integers in SIMPLE.
  # NEVER create more than 1 instance of the Integer class: OK.
  """
  def __init__ (self):
    Type.__init__(self, 0) # for Integer, I don't know


  def __str__ (self):
    """
    Display for Integer objects.
    """
    return "INTEGER<>"


  def accept (self, visitor):
    """
    Found an Integer type in the symbol table.
    """
    return visitor.visitInteger(self)


class Array (Type):
  """
  Arrays in SIMPLE.
  """
  def __init__ (self, elementType, length, position):
    assert isinstance(elementType, Type)
    assert type(length) is PType.IntType and length > 0

    Type.__init__(self, position)
    
    self.__elementType = elementType # get the type for Array elements
    self.__length = length # also record the length (for symbol table: set as DEVIL)


  def __str__ (self):
    """
    Display for Array objects.
    """
    return "ARRAY<type: %s; length: %d>" % (str(self.__elementType), self.__length)


  def arrayLength (self):
    """
    Returns the length of the Array object.
    """
    return self.__length


  def elementType (self):
    """
    Returns the type of the Array object.
    """
    return self.__elementType


  def accept (self, visitor):
    """
    Found an Array type in the symbol table.
    """
    return visitor.visitArray(self)


class Record (Type):
  """
  Records in SIMPLE.
  """

  # for pointers we need to store a pointer to the scope object
  def __init__ (self, scope, position):
    assert isinstance(scope, Scope)
    Type.__init__(self, position)
    self.__scope = scope # get the scope


  def __str__ (self):
    """
    Display for Record objects.
    """
    return "RECORD<%s>" % (str(self.__scope))


  def scope (self):
    """
    Returns the scope of the Record object.
    """
    return self.__scope


  def accept (self, visitor):
    """
    Found a Record type in the symbol table.
    """
    return visitor.visitRecord(self)


class InvalidType (Type):
  """
  InvalidType is a hack-type. In cases where there are variable use-before-declaration
  errors, we simply use InvalidType as the type of those variables.
  """

  def __init__ (self, position):
    Type.__init__(self, position)

  # error cannot generate any output
  # def accept (self, visitor):
  #     return visitor.visitInvalidT(self)

