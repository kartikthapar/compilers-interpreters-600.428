#!/usr/bin/python
#
# kdotvisitor.py
#
# Created by Kartik Thapar on 04/01/2013 at 04:20:45
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

from kvisitor import Visitor
IDENT_VAL = 2

class DotVisitor (Visitor):

  def __init__ (self):
    self.__indentationLevel = -1
    self.__dotOutput = ""


  def _dot (self):
    """
    Generate label here.
    """
    self.__indentationLevel += 1
    return "label" + str(self.__indentationLevel)


  def dotOutput (self):
    """
    Returns DOT output of the symbol table (from the parser).
    """
    self.__dotOutput = "digraph X {\n" + self.__dotOutput + "}"
    return self.__dotOutput
      

  def visitInstructions (self, instruction):
    pass


  def visitInstruction(self, instruction, dot):
    if instruction.next():
      n = instruction.next().accept(self)
      self.__dotOutput += dot + " -> " + n + " [label=next]" + ";\n"
      self.__dotOutput += "{rank=same; " + dot + " " + n + "}\n"


  def visitAssignment (self, _assign):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\":=\",shape=box]" + ";\n"

    e = _assign.location().accept(self)
    f = _assign.expression().accept(self)

    self.__dotOutput += dot + " -> " + e + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + f + " [label=expression]" + ";\n"

    self.visitInstruction(_assign, dot)

    return dot


  def visitIf (self, _if):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"If\",shape=box]" + ";\n"

    c = _if.condition().accept(self)
    self.__dotOutput += dot + " -> " + c + " [label=condition]" + ";\n"
    t = _if.instructionTrue().accept(self)
    self.__dotOutput += dot + " -> " + t + " [label=true]" + ";\n"
    if _if.instructionFalse():
      f = _if.instructionFalse().accept(self)
      self.__dotOutput += dot + " -> " + f + " [label=false]" + ";\n"

    self.visitInstruction(_if, dot)

    return dot


  def visitRepeat (self, _repeat):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Repeat\",shape=box]" + ";\n"

    c = _repeat.condition().accept(self)
    self.__dotOutput += dot + " -> " + c + " [label=condition]" + ";\n"
    i = _repeat.instruction().accept(self)
    self.__dotOutput += dot + " -> " + i + " [label=instructions]" + ";\n"

    self.visitInstruction(_repeat, dot)

    return dot


  def visitRead (self, _read):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Read\",shape=box]" + ";\n"

    e = _read.location().accept(self)
    self.__dotOutput += dot + " -> " + e + " [label=location]" + ";\n"

    self.visitInstruction(_read, dot)

    return dot


  def visitWrite (self, _write):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Write\",shape=box]" + ";\n"

    e = _write.expression().accept(self)
    self.__dotOutput += dot + " -> " + e + " [label=expression]" + ";\n"

    self.visitInstruction(_write, dot)

    return dot


  def visitNumber (self, _number):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Number\",shape=box]" + ";\n"
    x = self._dot()
    self.__dotOutput += x + \
      " [label=\"" + str(_number.constant().value()) + "\",shape=diamond]" + ";\n"
    self.__dotOutput += dot + " -> " + x + " [label=ST]" + ";\n"

    return dot


  def visitVariable (self, _variable):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Variable\",shape=box]" + ";\n"
    x = self._dot()
    self.__dotOutput += x + \
      " [label=\"" + str(_variable.variable().name()) +  "\",shape=circle]" + ";\n"
    self.__dotOutput += dot + " -> " + x + " [label=ST]" + ";\n"

    return dot
    

  def visitIndex (self, _index):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Index\",shape=box]" + ";\n"

    left = _index.location().accept(self)
    right = _index.expression().accept(self)

    self.__dotOutput += dot + " -> " + left + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=expression]" + ";\n"

    return dot


  def visitField (self, _field):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"Field\",shape=box]" + ";\n"

    left = _field.location().accept(self)
    right = _field.variable().accept(self)

    self.__dotOutput += dot + " -> " + left + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=variable]" + ";\n"

    return dot


  def visitBinary (self, _binary):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"" + _binary.operator() + "\",shape=box]" + ";\n"

    left = _binary.leftExpression().accept(self)
    right = _binary.rightExpression().accept(self)

    self.__dotOutput += dot + " -> " + left + " [label=left]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=right]" + ";\n"

    return dot


  def visitCondition (self, _condition):
    dot = self._dot()

    self.__dotOutput += dot + " [label=\"" + _condition.relation() + "\",shape=box]" + ";\n"

    l = _condition.leftExpression().accept(self)
    r = _condition.rightExpression().accept(self)

    self.__dotOutput += dot + " -> " + l + " [label=left]" + ";\n"
    self.__dotOutput += dot + " -> " + r + " [label=right]" + ";\n"

    return dot

