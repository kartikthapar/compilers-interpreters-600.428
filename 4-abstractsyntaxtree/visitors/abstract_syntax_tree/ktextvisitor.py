#!/usr/bin/python
#
# ktextvisitor.py
#
# Created by Kartik Thapar on 04/01/2013 at 04:20:57
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class TextVisitor (Visitor)
"""

from kvisitor import Visitor
from visitors.symbol_table import kvisitor as KV, ktextvisitor as KTV


IDENT_VAL = 2

class TextVisitor (Visitor):

  def __init__ (self):
    self.__indentationLevel = 0 # in_out
    self.__textOutput = ""


  def textOutput (self):
    """
    Method to retrieve the entire text output for the AST.
    """
    return "instructions =>\n" + self.__textOutput


  def _in (self):
    """
    Enter a node.
    """
    self.__indentationLevel += 1


  def _out (self):
    """
    Exit a node.
    """
    self.__indentationLevel -= 1


  def _makeIndent (self):
    """
    Calculates and returns the tabbed indent for a particular ...
    """
    return " " * IDENT_VAL * self.__indentationLevel

  
  def _makeText (self, value):
    """
    Append to the output.
    """
    self.__textOutput += self._makeIndent() + value + "\n"


  def visitInstructions (instruction):
    """
    Saw Instruction
    """
    instruction.accept(self)


  def visitAssignment (self, assignment):
    """
    Saw Assignment
    """
    self._in()
    self._makeText("Assign:")
    
    self._makeText("location =>")
    assignment.location().accept(self)

    self._makeText("expression =>")
    assignment.expression().accept(self)
    
    self._out()

    # do next...
    if assignment.next():
      assignment.next().accept(self)


  def visitIf (self, _if):
    """
    Saw If
    """
    self._in()
    self._makeText("If:")

    self._makeText("condition =>")
    _if.condition().accept(self)

    self._makeText("true =>")
    _if.instructionTrue().accept(self)

    if _if.instructionFalse():
      self._makeText("false =>")
      _if.instructionFalse().accept(self)

    self._out()

    if _if.next():
      _if.next().accept(self)


  def visitRepeat (self, _repeat):
    """
    Saw Repeat
    """
    self._in()
    self._makeText("Repeat:")

    self._makeText("condition =>")
    _repeat.condition().accept(self)

    self._makeText("instructions =>")
    _repeat.instruction().accept(self)

    self._out()

    if _repeat.next():
      _repeat.next().accept(self)


  def visitRead (self, _read):
    """
    Saw Read
    """
    self._in()
    self._makeText("Read:")

    self._makeText("location =>")
    _read.location().accept(self)

    self._out()

    if _read.next():
      _read.next().accept(self)


  def visitWrite (self, _write):
    """
    Saw Write
    """
    self._in()
    self._makeText("Write:")
    
    self._makeText("expression =>")
    _write.expression().accept(self)
    
    self._out()

    if _write.next():
      _write.next().accept(self)


  def visitNumber (self, _number):
    """
    Saw Number
    """
    self._in()
    self._makeText("Number:")

    # get constants
    self._makeText("value =>")
    tv = KTV.TextVisitor()
    tv.setIndentCustom(self.__indentationLevel)
    _number.constant().accept(tv)

    self.__textOutput += tv.textOutput()

    self._out()


  def visitVariable (self, _variable):
    """
    Saw Variable
    """
    self._in()
    self._makeText("Variable:")

    self._makeText("variable =>")

    # get variables
    tv = KTV.TextVisitor()
    tv.setIndentCustom(self.__indentationLevel)
    _variable.variable().accept(tv)
    self.__textOutput += tv.textOutput()
    
    self._out()


  def visitIndex (self, _index):
    """
    Saw Index
    """
    self._in()
    self._makeText("Index:")

    self._makeText("location =>")
    _index.location().accept(self)

    self._makeText("expression =>")
    _index.expression().accept(self)
    self._out()


  def visitField (self, _field):
    """
    Saw Field
    """
    self._in()
    self._makeText("Field:")

    self._makeText("location =>")
    _field.location().accept(self)

    self._makeText("variable =>")
    _field.variable().accept(self)

    self._out()


  def visitBinary (self, _binary):
    """
    Saw Binary
    """
    self._in()
    self._makeText("Binary (%s):" % (_binary.operator()))

    self._makeText("left =>")
    _binary.leftExpression().accept(self)

    self._makeText("right =>")
    _binary.rightExpression().accept(self)

    self._out()


  def visitCondition (self, _condition):
    """
    Saw Condition
    """
    self._in()
    self._makeText("Condition (%s):" % (_condition.relation()))

    self._makeText("left =>")
    _condition.leftExpression().accept(self)

    self._makeText("right =>")
    _condition.rightExpression().accept(self)

    self._out()

