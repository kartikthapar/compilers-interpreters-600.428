(Created on `04/08/2013` at `10:19:57`)

- This is a rewrite of the abstract syntax tree assignment that can be found in the folder `../4-abstracttree`.

- The parser was simply copied from a solid write in the symbol table assignment which is hard to crash. No significant philosophical differences in the code.