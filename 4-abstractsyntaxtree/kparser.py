#!/usr/bin/python
#
# kparser.py
#
# Created by Kartik Thapar on 02/23/2013 at 02:31:40
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

"""
Module for:
class Parser
"""

import sys
from kexception import *
import types as PType
from listeners.klistener import Listener
import ksymboltable as KST
import ktree as KAST

# see @91 Piazza - for more information
strongTokens = ["CONST", "TYPE", "VAR", "IF", "REPEAT", "WHILE", "WRITE", "READ"]
strongDecl = ["CONST", "TYPE", "VAR"]
strongInstr = ["IF", "REPEAT", "WHILE", "WRITE", "READ"]

ERROR_MOVE_OVER = 8 # spec
DEVIL = 5 # anti-spec # change this to 5

class Parser:
  """
  Parser for SIMPLE.
  """ 
  
  def __init__(self, tokenList):
    """
    Creates Parser objects for a tokenList without any errors.
    """
    assert type(tokenList) is PType.ListType
    self.__tokenList = tokenList
    self.__tokenCounter = 0
    self.__numberOfTokens = len(tokenList)

    self.__listeners = [] # listeners for events in Parser
    self.__toReport = 0 # counter for error reporting

    # symbol table stuff ----------------------------------------
    self.__currentScope = KST.Scope() # create universal scope
    self.__universalInteger = KST.Integer() # create instance of Integer and add it to the universal scope
    
    try:
      self.__currentScope.insert("INTEGER", self.__universalInteger) # you can only insert this once!
    except SymbolTableWriteException, e:
      assert False

    # AST stuff ------------------------------------------------
    self.__tree = []


  # ---------------------------------------------------------------
  # Define Listener functions
  #
  # - (void)registerListener: register listener
  # - (void)removeListener: remove listener
  # - (void)_notifyListeners: dispatch notifications to listeners
  # - (void)_notifyInNonTerminal
  # - (void)_notifyOutNonTerminal
  # - (void)_notifyMatchTerminal
  # ---------------------------------------------------------------


  def registerListener (self, listener):
    """
    Register listener to Parser.
    """

    # Can be extended to be based on a particular event. Not required at this point

    # we have different implementations of DOT and TEXT
    # therefore we can simply have inNonTerminal, outNonTerminal, matchTerminal
    # methods that simply call the respective methods of TextListener and
    # DotListener.

    # This way we don't have to care about the differences in respondence from DOT
    # or TEXT

    assert isinstance(listener, Listener)
    assert listener not in self.__listeners
    self.__listeners.append(listener)


  def removeListener (self, listener):
    """
    Remove listener from the list of listeners for Parser.
    """
    assert isinstance(listener, Listener)
    assert listener in self.__listeners

    self.__listeners.remove(listener)


  def _notifyListeners (self, event = None, context = None):
    """
    Dispatch notifications to all listeners. Notifications are
    specific to their registration.
    """
    assert event is not None # event cannot be default

    # depending on the type of event ---> do notify
    if event is "inNonTerminal":
      self._notifyInNonTerminal(context)
    elif event is "outNonTerminal":
      self._notifyOutNonTerminal()
    elif event is "matchTerminal":
      self._notifyMatchTerminal(context)
    elif event is "handleParserError":
      self._notifyError(context, "parser_type")
    elif event is "handleSymbolTableError":
      self._notifyError(context, "table_type")
    elif event is "handleAbstractSyntaxTreeError":
      self._notifyError(context, "tree_type")
    else:
      assert False


  def _notifyInNonTerminal (self, context):
    """
    Notify listeners to execute inNonTerminal().
    """

    for listener in self.__listeners:
      listener.inNonTerminal(context)


  def _notifyOutNonTerminal (self):
    """
    Notify listeners to execute outNonTerminal().
    """
    
    for listener in self.__listeners:
      listener.outNonTerminal()


  def _notifyMatchTerminal (self, context):
    """
    Notify listeners to execute matchTerminal().
    """
    for listener in self.__listeners:
      listener.matchTerminal(context)


  def _notifyError (self, context, errorType):
    """
    Notify listeners (specifically ErrorListener) to handle error.
    """

    assert type(context) is str
    assert type(errorType) is str

    if errorType is "parser_type":
      for listener in self.__listeners:
        listener.handleError(context, "parser_type")
    elif errorType is "table_type":
      for listener in self.__listeners:
        listener.handleError(context, "table_type")
    elif errorType is "tree_type":
      for listener in self.__listeners:
        listener.handleError(context, "tree_type")
    else:
      assert False


  # ---------------------------------------------------------------
  # Define HELPER functions
  #
  # - (BOOL)_tokenExists: is code exists
  # - (void)_counter: increments file read counter
  # - (Token)_currentToken: check if part of language
  # ---------------------------------------------------------------


  def _tokenExists (self):
    """
    Checks if tokens exist.
    """
    return True if self.__tokenCounter < self.__numberOfTokens else False
    

  def _currentToken (self):
    """
    Returns the current token from the list of tokens.
    """
    return self.__tokenList[self.__tokenCounter] # retrieve token


  def _counter (self):
    self.__tokenCounter += 1 # increment the token counter


  # ---------------------------------------------------------------
  # Define MATCH functions
  #
  # - (void)_reportError: add error to list based on move_over
  # - (void)_weakError: reports weak error
  # - (void)_nonWeakError: reports strong error + bad stuff
  # - (BOOL)_checkToken: increments file read counter
  # - (Token/void)_matchToken: check if part of language
  # ---------------------------------------------------------------


  def _reportError (self, error):
    """
    Report parser error
    """

    if self.__toReport > 0: # will only add to output if move_over is not breached
      return

    # depending upon the kind of token; value can be eof or token.value()
    token = self._currentToken()
    val = "eof" if token.kind() is "eof" else token.value()
    
    # generate error message. Don't scare / but don't be discrete. This is a class.
    a = "token value mismatch --- "
    b = str(error) + "; "
    c = "found: '%s' at position: '%d'." % (val, token.startPosition())
    self._notifyListeners("handleParserError", a + b + c) # add to error list
    self.__toReport = ERROR_MOVE_OVER # after you report the error; set the counter again    


  def _weakError (self, error):
    """
    Weak Error implementation. This simply adds the error to the list of errors.
    Major error checking is part of the match function.
    """
    self._reportError(error) # report error


  def _nonWeakError (self, error):
    """
    Non Weak Error implementation. Only called when the compiler has acknowledged
    move_over tokens (number of tokens that must be ignored in case of an error).
    Restarts parsing to the state of next Instruction OR Declaration depending on the
    type of strong token discovered.
    """
    token = self._currentToken()
    self._reportError(error)

    # depending on the type of wrong token --- do Decl or Instr
    # while token is not EOF or is not in the strong tokens list
    #   move on and check for Decl and Instr
    try:
      while (self._currentToken().kind() is not "eof") and\
            (self._currentToken().value() not in strongTokens):
        self._counter()
        if self.__toReport > 0: # still need to count for this
          self.__toReport -= 1
        if self._currentToken().value() in strongDecl:
          self._Declarations()
        elif self._currentToken().value() in strongInstr:
          self._Instructions()
    except TokenMismatchException, e:
      self._nonWeakError(e)


  def _checkToken (self, expectedTokenList):
    """
    Check if token is one from the expected token list.
    """
    kind = "" 

    # set this straight ---> change code in future assignments :: Maybe?
    if self._currentToken().kind() == "identifier":
      kind = "identifier"
    elif self._currentToken().kind() == "integer":
      kind = "integer"
    elif self._currentToken().kind() == "symbol":
      kind = self._currentToken().value()
    elif self._currentToken().kind() == "keyword":
      kind = self._currentToken().value()
    elif self._currentToken().kind() == "eof":
      kind = "eof"
    else:
      assert False

    return True if kind in expectedTokenList else False # look for kind in token list
    

  def _matchToken (self, expectedTokenList):
    """
    Match a terminal value in the program as specified in the grammar.
    """

    if self.__tokenCounter == len(self.__tokenList):
      return # don't check if it's gone mad

    token = self._currentToken()

    # if token is fine ---> notify and move ahead
    if self._checkToken(expectedTokenList):
      if token.kind() is not "eof":
        self._notifyListeners("matchTerminal", token)
      self._counter() # increment counter after token match
      
      if self.__toReport > 0:
        self.__toReport -= 1
      return token
    else:
      return False


  # ---------------------------------------------------------------
  # Visitor patter implemented. Now return the main scope that
  # carries everthing
  # ---------------------------------------------------------------

  def majorScope (self):
    """
    Returns the current scope of the SIMPLE program.
    """
    return self.__currentScope


  def syntaxTree (self):
    return self.__tree


  def _handleSymbolTableWriteException (self, e, tokenValue, position):
    """
    Handles notification to error handler in case there is a duplicate write
    to the symbol table.
    """
    rvalue = self.__currentScope.retrieve(tokenValue)
    err = "; previous declaration at location '%d'; new declaration at location '%d'." % (rvalue.position(), position)
    err = str(e) + err

    self._notifyListeners("handleSymbolTableError", err)


  def _handleSymbolTableReadException (self, e, token, listener, generate=False):
    """
    Handles notification to error handler in case the entry does not exist in the
    symbol table. As the value is not available in the symbol table, it creates a new
    entry and returns it to the calling function.
    """
    assert (generate is True) or (generate is False) # check for generate
    assert (listener is "tree") or (listener is "symbol")

    err = str(e) + "; at location '%d'." % (token.startPosition())
    if listener == "tree":
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
    else:
      self._notifyListeners("handleSymbolTableError", err)

    if generate: # now we need to generate these values some times
      newEntry = KST.Type(token.startPosition()) # create new entry
      self.__currentScope.insert(token.value(), newEntry) # insert entry
      return newEntry


  # ---------------------------------------------------------------
  # Program = "PROGRAM" identifier ";" Declarations
  #   ["BEGIN" Instructions] "END" identifier "." .
  # ---------------------------------------------------------------


  def _Program (self):
    """
    Program = "PROGRAM" identifier ";" Declarations
        ["BEGIN" Instructions] "END" identifier "." .
    """

    # notify start of program
    self._notifyListeners("inNonTerminal", "Program")

    # strong error if "PROGRAM" not found
    if not self._matchToken(["PROGRAM"]):
      raise TokenMismatchException("expected 'PROGRAM' at the start of the program")

    # create the actual program 'scope' here
    self.__currentScope = KST.Scope(self.__currentScope)

    # look for identifier --- program name
    token = self._matchToken(["identifier"])
    if not token:
      raise TokenMismatchException("expected an identifier to name the program at the start of the program")
    programName = token.value() # should match name at the end
    pos = token.startPosition()

    if not self._matchToken([";"]):
      self._weakError("expected ';' to end the program declaration statement")
    
    self._Declarations()
    
    # ["BEGIN" Instructions]
    if self._checkToken(["BEGIN"]):
      self._matchToken(["BEGIN"])
      self.__tree = self._Instructions()

    # ------------- prolog -------------
    if not self._matchToken(["END"]):
      self._weakError("expected 'END' to end the current program")
    
    token = self._matchToken(["identifier"])
    if not token:
      raise TokenMismatchException("expected identifier to match the end of the program")

    # if mismatch in PROGRAM X; END X.
    if token.value() != programName:
      err = \
      "program name '%s' at start of program (location: '%s') is not same as program name '%s' "\
      "at end of program (location: '%s')." % (programName, pos, token.value(), token.startPosition())
      self._notifyListeners("handleSymbolTableError", err)

    if not self._matchToken(["."]): # this is specific to PROGRAM
      self._weakError("expected '.' to end the SIMPLE program")

    self._notifyListeners("outNonTerminal")


  # ---------------------------------------------------------------
  # Declarations = { ConstDecl | TypeDecl | VarDecl } .
  # ConstDecl = "CONST" {identifier "=" Expression ";"} .
  # TypeDecl = "TYPE" {identifier "=" Type ";"} .
  # VarDecl = "VAR" {IdentifierList ":" Type ";"} .
  # ---------------------------------------------------------------


  def _Declarations (self):
    self._notifyListeners("inNonTerminal", "Declarations")

    # check current token to be of type CONST TYPE VAR
    while self._checkToken (["CONST", "TYPE", "VAR"]):
      if self._checkToken(["CONST"]):
        self._ConstDecl()
      elif self._checkToken(["TYPE"]):
        self._TypeDecl()
      elif self._checkToken(["VAR"]):
        self._VarDecl()
      else:
        assert False # should never come here

    # end declarations
    self._notifyListeners("outNonTerminal")


  def _ConstDecl (self):
    self._notifyListeners("inNonTerminal", "ConstDecl")

    token = self._matchToken(["CONST"])

    # assume you found: CONST con = 47 (example)
    # also assume 5 instead of 47; don't care about the parsed expression
    while self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"]) # matches the const identifier
      constant = DEVIL
      if not self._matchToken(["="]):
        raise TokenMismatchException("expected '=' in CONST declaration")
      
      expressionNode = self._Expression()
      
      if not self._matchToken([";"]):
        self._weakError("expected ';' at the end of CONST declaration")
      
      if not isinstance (expressionNode, KAST.Number): # if not number
        err = "expected a constant in constant declaration at location '%d'." % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
      else:
        constant = expressionNode.constant().value()

      try: # insert value in symbol table (duplicate possible)
        c = KST.Constant(self.__universalInteger, constant, token.startPosition())
        self.__currentScope.insert(token.value(), c)
      except SymbolTableWriteException, e:
        self._handleSymbolTableWriteException(e, token.value(), token.startPosition())

    self._notifyListeners("outNonTerminal")


  def _TypeDecl (self):
    self._notifyListeners("inNonTerminal", "TypeDecl")

    token = self._matchToken(["TYPE"])
    pos = token.startPosition()

    while self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"])
      
      if not self._matchToken(["="]):
        raise TokenMismatchException("expected '=' in TYPE declaration")
      
      elementType = self._Type()

      if not self._matchToken([";"]):
        self._weakError("expected ';' at the end of TYPE declaration")

      # if elementType is BAD, just move out (but make sure to do this after the previous error check)
      if elementType is None:
        break
    
      try:
        self.__currentScope.insert(token.value(), elementType)
      except SymbolTableWriteException, e:
        self._handleSymbolTableWriteException(e, token.value(), pos)

    self._notifyListeners("outNonTerminal")


  def _VarDecl (self):
    self._notifyListeners("inNonTerminal", "VarDecl")

    token = self._matchToken(["VAR"])
    
    while self._checkToken(["identifier"]):
      identifierList = self._IdentifierList() # get the identifier list

      if not self._matchToken([":"]):
        raise TokenMismatchException("expected ':' in VAR declaration OR check your instruction declaration")
      
      elementType = self._Type()

      if not (self._matchToken([";"])):
        self._weakError("expected ';' at the end of VAR declaration")
      
      # if elementType is BAD, just move out (but make sure to do this after the previous error check)
      if elementType is None:
        break

      for tokenId in identifierList:
        v = KST.Variable(tokenId.value(), elementType, token.startPosition())
        try: # insert value in the symbol table (duplicate possible)
          self.__currentScope.insert(tokenId.value(), v)
        except SymbolTableWriteException, e:
          self._handleSymbolTableWriteException(e, tokenId.value(), token.startPosition())

    self._notifyListeners("outNonTerminal")


  # ---------------------------------------------------------------
  # Type = identifier | "ARRAY" Expression "OF" Type |
  #   "RECORD" {IdentifierList ":" Type ";"} "END" .
  # ---------------------------------------------------------------


  def _Type (self):
    self._notifyListeners("inNonTerminal", "Type")
    typeValue = None

    # if Type 'identifier' (lonely) ------------------------------------

    if self._checkToken(["identifier"]):
      token = self._matchToken(["identifier"])

      try: # get value from symbol table and handle exception
        entry = self.__currentScope.retrieve(token.value())
        if isinstance(entry, KST.Type):
          typeValue = entry
        else:
          err = "'%s' is not of type 'TYPE'; at location '%d'." % (token.value(), token.startPosition())
          self._notifyListeners("handleSymbolTableError", err)
          typeValue = None # entry with same name in symbol table is not of type Type

      except SymbolTableReadException, e:
        # now that the identifier doesn't exist in the symbol table
        # we need to create something that can be used later
        typeValue = self._handleSymbolTableReadException (e, token, "symbol", generate=True)

  
  # array case -------------------------------------------------------
    
    elif self._checkToken(["ARRAY"]):
      token = self._matchToken(["ARRAY"])
      
      arraySizeNode = self._Expression() # will use this to get length of array
      arraySize = 1
      if arraySizeNode:
        if not isinstance(arraySizeNode, KAST.Number): # if not a constant
          err = "expected a constant array size value at location '%d'." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
        elif arraySizeNode.constant().value() <= 0: # is a constant
          err = "expected size of array to be greater than 0; at location '%d'." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
        else:
          arraySize = arraySizeNode.constant().value() # get size of array

      if not self._matchToken(["OF"]):
        raise TokenMismatchException("expected 'OF' in ARRAY declaration")
      
      elementType = self._Type() # get the type of the element

      # it is possible that elementType is None # based on the received Type
      if not isinstance(elementType, KST.Entry):
        typeValue = None
      else:
        typeValue = KST.Array(elementType, arraySize, token.startPosition())
  
    # record case -----------------------------------------------------

    elif self._checkToken(["RECORD"]):
      token = self._matchToken(["RECORD"])
      
      self.__currentScope = KST.Scope(self.__currentScope) # create new scope for Record

      while self._checkToken("identifier"):
        identifierList = self._IdentifierList() # get identifier list

        if not self._matchToken([":"]):
          raise TokenMismatchException("expected ':' in RECORD identifier declaration")
        
        elementType = self._Type() # type of identifiers

        if not (self._matchToken([";"])):
          self._weakError("expected ';' in RECORD declaration in TYPE")
        
        # now error checking is complete, make variables and add them to list
        # identifier in the loop below is a token
        
        # issue with the returned elementType
        # GET RID OF THIS HACK -----------
        if elementType is None:
          if not self._matchToken(["END"]):
            self._weakError("expected 'END' at the end of RECORD declaration in TYPE")
          return None

        for tokenId in identifierList:
          v = KST.Variable(tokenId.value(), elementType, token.startPosition()) # create a variable
          try: # insert value in symbol table (duplicate possible)
            self.__currentScope.insert(tokenId.value(), v)
          except SymbolTableWriteException, e:
            self._handleSymbolTableWriteException(e, tokenId.value(), token.startPosition())

      # end of record type --- symbol table stuff
      tscope = self.__currentScope
      self.__currentScope = self.__currentScope.outerScope()
      tscope.setOuterScope(None) # break connection for RECORD
      typeValue = KST.Record(tscope, token.startPosition())

      if not self._matchToken(["END"]):
        self._weakError("expected 'END' at the end of RECORD declaration in TYPE")

    else: # if Type gets nothing from above
      raise TokenMismatchException("expected one of identifier, ARRAY, RECORD")

    self._notifyListeners("outNonTerminal")
    return typeValue # return the type


  # ---------------------------------------------------------------
  # Expression = ["+"|"-"] Term {("+"|"-") Term} .
  # Term = Factor {("*"|"DIV"|"MOD") Factor} .
  # Factor = integer | Designator | "(" Expression ")" .
  # ---------------------------------------------------------------


  def _Expression (self):
    """
    Expression = ["+"|"-"] Term {("+"|"-") Term} .
    """
    self._notifyListeners("inNonTerminal", "Expression")
    operator = None
    
    if self._checkToken(["+", "-"]):
      operator = self._matchToken(["+", "-"])

    node = self._Term()
    
    # if node and operator exist ---> do negate or not...
    if node and operator:
      const0 = KAST.Number(KST.Constant(self.__universalInteger, 0, operator.startPosition()))
      if isinstance(node, KAST.Number): # if it's number type
        num = node.constant().value() # get value of the number constant
        num = num if operator == "+" else (0 - num)
        node = KAST.Number(KST.Constant(self.__universalInteger, num, operator.startPosition()))
      else:
        # check what's this is for
        print "inside unknown territory"
        if not (isinstance(node.expressionType(), KST.Array) and 
                isinstance(node.expressionType(), KST.Record)):
          node = KAST.Binary(operator.value(), const0, node) # create a binary node
        else:
          err = "expected a number and not an array or a record at location '%d'." % (operator.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          node = None

    while self._checkToken(["+", "-"]):
      operator = self._matchToken(["+", "-"])
      next = self._Term()
      if isinstance(node, KAST.Number) and isinstance(next, KAST.Number): # if both are numbers
        p = node.constant().value()
        q = next.constant().value()
        num = (p + q) if (operator.value() == "+") else (p - q)
        node = KAST.Number(KST.Constant(self.__universalInteger, num, operator.startPosition()))
      elif node and next:
        if not (isinstance(node.expressionType(), KST.Integer) and 
                isinstance(next.expressionType(), KST.Integer)):
          err = "expected operands to be integers at location '%d'." % (operator.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          node = None
        else:
          node = KAST.Binary(operator.value(), node, next) # otherwise create a binary node

    self._notifyListeners("outNonTerminal")
    return node


  def _Term (self):
    self._notifyListeners("inNonTerminal", "Term")
    
    node = self._Factor()

    while self._checkToken(["*", "DIV", "MOD"]):
      operator = self._matchToken(["*", "DIV", "MOD"])
      next = self._Factor()
      if isinstance(node, KAST.Number) and isinstance(next, KAST.Number):
        p = node.constant().value()
        q = next.constant().value()
        num = 0
        try:
          if operator.value() == "*":
            num = p * q
          elif operator.value() == "DIV":
            num = p / q # q != 0
          elif operator.value() == "MOD":
            num = p % q # q != 0
          else:
            assert False
          node = KAST.Number(KST.Constant(self.__universalInteger, num, operator.startPosition())) # make node
        except ZeroDivisionError, e:
          err = str(e) + " at location '%d'." % (operator.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          node = None
      else:
        if node and next: # if they are valid
          if not (isinstance(node.expressionType(), KST.Integer) or
                  isinstance(next.expressionType(), KST.Integer)):
            err = "expected integers in the arithmetic expression at location '%d'." % (operator.startPosition())
            self._notifyListeners("handleAbstractSyntaxTreeError", err)
            node = None
          else:
            node = KAST.Binary(operator.value(), node, next)


    self._notifyListeners("outNonTerminal")
    return node


  def _Factor (self):
    self._notifyListeners("inNonTerminal", "Factor")

    node = None

    if self._checkToken(["integer"]):
      token = self._matchToken(["integer"])
      node = KAST.Number(KST.Constant(self.__universalInteger, token.value(), token.startPosition())) # create number node
    elif self._checkToken(["identifier"]):
      node = self._Designator()
    elif self._checkToken(["("]):
      self._matchToken(["("])
      node = self._Expression()
      if not self._matchToken([")"]):
        self._weakError("expected ')' to close the expression defined")
    else:
      raise TokenMismatchException("expected an integer or identifier or '(' or read the syntax first.")

    self._notifyListeners("outNonTerminal")
    return node # return factor node


  # ---------------------------------------------------------------
  # Instructions = Instruction {";" Instruction} .
  # Instruction = Assign | If | Repeat | While | Read | Write .
  # Assign = Designator ":=" Expression .
  # If = "IF" Condition "THEN" Instructions ["ELSE" Instructions] "END" .
  # Repeat = "REPEAT" Instructions "UNTIL" Condition "END" .
  # While = "WHILE" Condition "DO" Instructions "END" .
  # Condition = Expression ("="|"#"|"<"|">"|"<="|">=") Expression .
  # Write = "WRITE" Expression .
  # Read = "READ" Designator .
  # ---------------------------------------------------------------


  def _Instruction (self):
    self._notifyListeners("inNonTerminal", "Instruction")
    node = None
    
    if self._checkToken(["identifier"]): # all other are keywords duh!
      node = self._Assign()
    elif self._checkToken(["IF"]):
      node = self._If()
    elif self._checkToken(["REPEAT"]):
      node = self._Repeat()
    elif self._checkToken(["WHILE"]):
      node = self._While()
    elif self._checkToken(["READ"]):
      node = self._Read()
    elif self._checkToken(["WRITE"]):
      node = self._Write()
    else:
      raise TokenMismatchException(
        "expected one of 'assignment using an identifier', 'IF', 'WHILE', 'REPEAT', 'READ', 'WRITE'"
      )

    self._notifyListeners("outNonTerminal")
    return node
  

  def _Instructions (self):
    self._notifyListeners("inNonTerminal", "Instructions")

    instructionNode = self._Instruction()
    firstInstructionNode = instructionNode
    
    while self._checkToken([";"]):
      self._matchToken([";"])
      nextInstructionNode = self._Instruction() # get the next instruction
      if nextInstructionNode and isinstance(nextInstructionNode, KAST.Instruction): # if an instruction node
        instructionNode.setNext(nextInstructionNode)
        instructionNode = nextInstructionNode # update instruction node (for next)

    self._notifyListeners("outNonTerminal")
    return firstInstructionNode # return the first instruction


  def _Assign (self):
    self._notifyListeners("inNonTerminal", "Assign")

    designatorNode = self._Designator() # do designator...

    token = self._matchToken([":="])
    if not token:
      raise TokenMismatchException("expected ':=' in assignment")
    
    expressionNode = self._Expression() # do expression...
    
    if designatorNode: # if exists
      if not isinstance(designatorNode, KAST.Location): # this should always point to a variable location
        err = "expected variable for designator in 'ASSIGN' at location '%d'." % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        designatorNode = None
    if designatorNode and expressionNode: # if both exist
      if designatorNode.expressionType() != expressionNode.expressionType():
        err = "expected both sides in 'ASSIGN' to be type equivalent at location '%d'." % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        designatorNode = expressionNode = None


    self._notifyListeners("outNonTerminal")
    return KAST.Assign(designatorNode, expressionNode) # return assign node


  def _If (self):
    self._notifyListeners("inNonTerminal", "If")

    token = self._matchToken(["IF"])
    
    conditionNode = self._Condition()
    
    if not self._matchToken(["THEN"]):
      raise TokenMismatchException("expected 'THEN' in 'IF' instruction at location '%d'." % (token.startPosition()))
    
    trueNode = self._Instructions()
    falseNode = None
    
    if self._checkToken(["ELSE"]): # ["ELSE" Instructions]
      self._matchToken(["ELSE"])
      falseNode = self._Instructions()

    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'IF' instruction.")

    self._notifyListeners("outNonTerminal")
    return KAST.If(conditionNode, trueNode, falseNode) # repeat if node


  def _Repeat (self):
    self._notifyListeners("inNonTerminal", "Repeat")

    self._matchToken(["REPEAT"])

    instructionNode = self._Instructions()
    
    if not self._matchToken(["UNTIL"]):
      raise TokenMismatchException("expected 'UNTIL' with 'REPEAT' instruction")
    
    conditionNode = self._Condition()
    
    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'REPEAT' instruction")

    self._notifyListeners("outNonTerminal")
    return KAST.Repeat(conditionNode, instructionNode) # return repeat node


  def _While (self):
    self._notifyListeners("inNonTerminal", "While")

    self._matchToken(["WHILE"])
    
    conditionNode = self._Condition()
    
    if not self._matchToken(["DO"]):
      raise TokenMismatchException("expected 'DO' with 'WHILE' instruction")
    
    instructionNode = self._Instructions()
    
    if not self._matchToken(["END"]):
      self._weakError("expected 'END' at the end of 'WHILE' instruction")

    self._notifyListeners("outNonTerminal")
    return KAST.While(conditionNode, instructionNode) # this will transform


  def _Condition (self):
    self._notifyListeners("inNonTerminal", "Condition")

    leftNode = self._Expression() # do expression
    
    pos = self._currentToken().startPosition() # after expression, we need a relation

    matchTokenList = ["=", "#", "<", ">", "<=", ">="]
    token = self._matchToken(matchTokenList)
    if not token:
      raise TokenMismatchException("expected one of '=', '#', '<', '>', '<=', '>=' in condition at location '%d'" % (token.startPosition()))
    
    rightNode = self._Expression()

    if leftNode and rightNode: # if both expressions exist
      if not (isinstance(leftNode.expressionType(), KST.Integer) and
              isinstance(rightNode.expressionType(), KST.Integer)): # they need to evaluate to integers
        err = "expected left and right expressions in a condition to be of type integers; at location '%d'." \
        % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        leftNode = rightNode = None
    
    self._notifyListeners("outNonTerminal")
    return KAST.Condition(token.value(), leftNode, rightNode) # return condition


  def _Write (self):
    self._notifyListeners("inNonTerminal", "Write")

    token = self._matchToken(["WRITE"])
    
    expressionNode = self._Expression() # needs to be an integer

    if not expressionNode or not isinstance(expressionNode.expressionType(), KST.Integer):
      err = "expected expression of type 'integer' in 'WRITE' at location '%d'." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      expressionNode = None
    
    self._notifyListeners("outNonTerminal")
    return KAST.Write(expressionNode)


  def _Read (self):
    self._notifyListeners("inNonTerminal", "Read")

    token = self._matchToken(["READ"])

    designatorNode = self._Designator() # needs to be a location
    
    if not designatorNode or not isinstance(designatorNode, KAST.Location):
      err = "expected a variable location in 'READ' at location '%d'." % (token.startPosition())
      self._notifyListeners("handleAbstractSyntaxTreeError", err)
      designatorNode = None
    if designatorNode:
      if not isinstance(designatorNode.expressionType(), KST.Integer):
        err = "expected a variable location of type 'integer' in 'READ' at location '%d'." % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        designatorNode = None

    self._notifyListeners("outNonTerminal")
    return KAST.Read(designatorNode)


  # ---------------------------------------------------------------
  # Designator = identifier Selector .
  # Selector = {"[" ExpressionList "]" | "." identifier} .
  # IdentifierList = identifier {"," identifier} .
  # ExpressionList = Expression {"," Expression} .
  # ---------------------------------------------------------------


  def _Designator (self):
    self._notifyListeners("inNonTerminal", "Designator")
    
    token = self._matchToken(["identifier"])
    node = None # create an empty node
    entry = None
    
    try: # first retrieve the entry from the symbol table
      entry = self.__currentScope.retrieve(token.value())
    except SymbolTableReadException, e:
      self._handleSymbolTableReadException (e, token, listener="tree", generate=False)

    # context conditions for assign, read, factor (variable or constant)
    if entry:
      if isinstance(entry, KST.Constant): # if entry is a constant, create a number node
        node = KAST.Number(entry)
      elif isinstance(entry, KST.Variable): # if the entry is a variable, create a variable node
        node = KAST.Variable(entry)
      else: # see error
        err = "expected designator to be of type 'variable' or 'constant' at location '%d'." % (token.startPosition())
        self._notifyListeners("handleAbstractSyntaxTreeError", err)
        node = None

    node = self._Selector(node) # do selector
    self._notifyListeners("outNonTerminal")
    return node

  
  def _Selector (self, node):
    """
    Selector = {"[" ExpressionList "]" | "." identifier} .
    """
    self._notifyListeners("inNonTerminal", "Selector")
    
    while (self._checkToken(["["]) or self._checkToken(["."])):
      # if array type ----
      if self._checkToken(["["]):
        token = self._matchToken(["["])
        expressionNodes = self._ExpressionList() # get a list of expression nodes
        if not self._matchToken(["]"]):
          self._weakError("expected ']' to close the expression list")

        # after matching stuff, do AST working here ---

        if node and not isinstance(node.expressionType(), KST.Array): # if not an array ---> report
          err = "expected an array type for indexing at location '%d'." % (token.startPosition())
          self._notifyListeners("handleAbstractSyntaxTreeError", err)
          node = None
        elif node: # if valid: an array type
          for expression in expressionNodes:
            if not expression: # if any one of the expression goes mad
              node = None
              break
            if not isinstance(expression.expressionType(), KST.Integer): # if not of type integer
              err = "expected an integer value as an index to an array at location '%d'." % (token.startPosition())
              self._notifyListeners("handleAbstractSyntaxTreeError", err)
              node = None
            else:
              node = KAST.Index(node, expression) # create index node
      # if record type ----
      elif self._checkToken(["."]):
        self._matchToken(["."])
        token = self._matchToken(["identifier"])
        if not token: # if this doesn't exist
          raise TokenMismatchException("expected identifier in the selector")

        if node: # if node exists:
          if not isinstance(node.expressionType(), KST.Record): # should be a record
            err = "expected a record type to retrieve a field value at location '%d'." % (token.startPosition())
            self._notifyListeners("handleAbstractSyntaxTreeError", err)
            node = None
          else:
            variable = None
            try: # get variable from scope
              variable = node.expressionType().scope().retrieve(token.value())
            except SymbolTableReadException, e:
              self._handleSymbolTableReadException(e, token, listener="symbol", generate=False)
              node = None # if this doesn't exist, nullify node
            if variable: # if variable exists
              if not isinstance(variable, KST.Variable):
                err = "expected a variable type for a record field at location '%d'." % (token.startPosition())
                self._notifyListeners("handleAbstractSyntaxTreeError", err)
                node = None
              else:
                node = KAST.Field(node, KAST.Variable(variable))
      else:
        assert False

    self._notifyListeners("outNonTerminal")
    return node


  def _IdentifierList (self):
    """
    Scan a list of identifiers
    """
    self._notifyListeners("inNonTerminal", "IdentifierList")
    
    identifiers = [] # holds all the identifiers
    identifiers.append(self._matchToken(["identifier"])) # add token to list
    
    while self._checkToken([","]):
      self._matchToken([","])
      token = self._matchToken(["identifier"])
      if not token:
        raise TokenMismatchException("expected an identifier to continue the list of identifiers")
      identifiers.append(token)

    self._notifyListeners("outNonTerminal")
    return identifiers # return identifier list
  

  def _ExpressionList (self):
    """
    Returns a list of ASTs for expressions
    """
    self._notifyListeners("inNonTerminal", "ExpressionList")

    expressionNodes = [] # list of expressions
    expressionNodes.append(self._Expression()) # add new expression to list

    while self._checkToken([","]):
      self._matchToken([","])
      expressionNodes.append(self._Expression()) # add node to list

    self._notifyListeners("outNonTerminal")
    return expressionNodes


  # ---------------------------------------------------------------
  # parse <code>
  # ---------------------------------------------------------------


  def parse (self):
    """
    Public method to perform parsing using the Parser.
    """
    try:
      self._Program()
    except TokenMismatchException, e:
      self._nonWeakError(e)
    
    self._matchToken(["eof"])

