#
# ksymboltable.py
#
# Created by Kartik Thapar on 03/09/2013 at 06:41:16
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import types as PType
from kexception import *
from visitors.symbol_table.kvisitor import Visitor

# ---------- SCOPE -----------

class Scope():
  
  def __init__(self, outerScope = None): # outer = None for top most
    """Creates Scope objects. In some cases, the parent scope is also provided if it exists."""
    assert isinstance(outerScope, Scope) or outerScope is None # default
    self.__outerScope = outerScope # need parent scope for all scopes
    self.__symbolTable = {} # need a hash type data stucture

  def outerScope (self):
    """Returns the outer scope for the current scope"""
    return self.__outerScope

  def setOuterScope (self, scope):
    """Set the outer scope for the current scope."""
    assert isinstance(scope, Scope) or scope is None
    self.__outerScope = scope

  def insert (self, entryName, entry):
    """Insert value in the symbol table in the current scope."""
    assert isinstance (entry, Entry)
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0
    if entryName in self.__symbolTable:
      raise SymbolTableWriteException("you already declared '%s'" % (entryName))
    else:
      self.__symbolTable[entryName] = entry

  def retrieve (self, entryName):
    """Retrieve values from the symbol table in the current scope."""
    assert type(entryName) is PType.StringType
    assert len(entryName) > 0
    if entryName in self.__symbolTable: # find in current scope
      return self.__symbolTable[entryName]
    elif self.outerScope() != None: # find in outer scope(s)
      return self.outerScope().retrieve(entryName)
    else:
      raise SymbolTableReadException("you never declared '%s'" % (entryName))

  def accept (self, visitor):
    return visitor.visitScope(self)

  def symbolTable (self):
    """Returns the symbol table of the Scope object."""
    return self.__symbolTable

  def __str__( self ):
    string = ""
    for v in self.__symbolTable:
      string += v + " => " + str(self.__symbolTable[v]) + "\n"
    return string

  def numberOfKeys (self):
    return len(self.__symbolTable.keys())

# -------------- ENTRY --------------

class Entry:
  """Base class for all symbol table entries."""

  def __init__ (self, position):
    assert type(position) is PType.IntType
    self.__position = position # need to record positions for errors

  def position (self):
    """Returns the position of the entry."""
    return self.__position

  def accept (self, visitor):
    """For visitior."""
    assert isinstance(visitor, Visitor)
    pass


class Constant (Entry):
  """Constants in SIMPLE."""

  def __init__ (self, value, ktype, position):
    assert type(value) is PType.IntType or type(value) is PType.LongType
    assert isinstance(ktype, Type)
    Entry.__init__(self, position)
    self.__elementType = ktype # get type of the constant value (right now they are INTEGER types)
    self.__value = value # get value (this is set as DEVIL)

  def elementType (self):
    """Returns the type of Constant object."""
    return self.__elementType

  def value (self):
    """Returns the value of the Constant object"""
    return self.__value
  
  def __str__ (self):
    return "<CONST: <type:%s>, <value:%s>, <position:%s>>" % \
          (str(self.__elementType), self.__value, self.position())

  def accept (self, visitor):
    return visitor.visitConstant(self)


class Variable (Entry):
  """Variables in SIMPLE."""

  def __init__ (self, name, elementType, position):
    assert type(name) is str
    assert len(name) > 0
    assert isinstance(elementType, Type)
    self.__name = name
    self.__offset = 0 # address offset (for code generation)
    Entry.__init__(self, position)
    self.__elementType = elementType # get the type of Variable

  def setOffset (self, offset):
    """Sets offset for a particular variable."""
    self.__offset = offset

  def offset (self):
    """Returns the offset associated with a particular variable."""
    return self.__offset

  def elementType (self):
    """Returns the type of the variable."""
    return self.__elementType

  def name (self):
    """Returns the name of the variable"""
    return self.__name

  def __str__ (self):
    return "<VAR: <name:%s>, <type:%s>, <offset:%s>, <position:%s>>" % \
          (self.__name, str(self.__elementType), self.__offset, self.position())

  def accept (self, visitor):
    return visitor.visitVariable(self)

# ------------ TYPE --------------

class Type (Entry):
  """Types in SIMPLE."""

  def __init__ (self, position):
    Entry.__init__(self, position)
    self.__size = 0 # size of the type (for code gen)

  def size (self):
    """Returns the size of the type."""
    return self.__size

  def setSize (self, size):
    """Method to set the size of the type."""
    self.__size = size

  def __str__ (self):
    return "<Type: <size:%s>>" % (self.__size)


class Integer (Type):
  """Base Integer Class in SIMPLE. This is a singleton (only one instance)."""
  
  def __init__ (self):
    Type.__init__(self, 0) # for Integer, I don't know

  def __str__ (self):
    return "INTEGER<>"

  def accept (self, visitor):
    return visitor.visitInteger(self)


class Array (Type):
  """Arrays in SIMPLE."""

  def __init__ (self, elementType, length, position):
    assert isinstance(elementType, Type)
    assert type(length) is PType.IntType and length > 0
    Type.__init__(self, position)
    self.__elementType = elementType # get the type for Array elements
    self.__length = length # also record the length (for symbol table: set as DEVIL)

  def length (self):
    """Returns the length of the array."""
    return self.__length

  def elementType (self):
    """Returns the type of the Array object."""
    return self.__elementType

  def accept (self, visitor):
    return visitor.visitArray(self)

  def __str__ (self):
    return "<ARRAY: <type:%s>, <length:%s>, <size:%s>, <position:%s>>" % \
          (str(self.__elementType), self.__length, self.size(), self.position())


class Record (Type):
  """Records in SIMPLE."""

  def __init__ (self, scope, position):
    assert isinstance(scope, Scope)
    Type.__init__(self, position)
    self.__scope = scope # get the scope

  def __str__ (self):
    return "<RECORD: <scope:%s>, <size:%s>, <position:%s>>" % \
          (str(self.__scope), self.size(), self.position())

  def scope (self):
    """Returns the scope of the Record object."""
    return self.__scope

  def accept (self, visitor):
    return visitor.visitRecord(self)


class InvalidType (Type):
  """
  InvalidType is a hack-type. In cases where there are variable use-before-declaration
  errors, we simply use InvalidType as the type of those variables.
  """

  def __init__ (self, position):
    Type.__init__(self, position)


def test():
  a = Constant(100, Type(10), 20)
  t = Array(Integer(), 10, 30)
  v = Variable("v", t, 40)
  scope = Scope()
  scope.insert('i', Constant(10, Integer(), 30))
  t = Record(scope, 10)
  nv = Variable("m", t, 50)
  print v
  print nv

