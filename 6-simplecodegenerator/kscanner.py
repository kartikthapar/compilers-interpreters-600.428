#!/usr/bin/python
#
# scanner.py
#
# Created by Kartik Thapar on 02/12/2013 at 22:16:30
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys
from ktoken import Token
from kexception import *

digits = "0123456789" # define digits; digit = "0" | "1" | .. | "9" .
letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ" # letter = "a" | ... | "z" | "A" | ... "Z" .
symbols = ";.=:+-*()#<>[]," # define symbols using the language above

# define keywords using the language above
keywords = ["PROGRAM", "BEGIN", "END", "CONST", "TYPE", "VAR",
            "ARRAY", "OF", "RECORD", "DIV", "MOD", "IF", "THEN", "ELSE",
            "REPEAT", "UNTIL", "WHILE", "DO", "WRITE", "READ"]

# LF (Line feed, '\n', 0x0A, 10 in decimal) or 
# CR (Carriage return, '\r', 0x0D, 13 in decimal)
eol = "\n\r"
whitespaces = " \t\f" # include '_', '\t', '\f'

class Scanner:
  """Implements a generic Scanner for SIMPLE programming language."""

  def __init__(self, code):
    """Creates a scanner object for the input SIMPLE code."""
    self.__code = code # source code
    self.__length = len(code) # length of the code input
    self.__textPosition = 0 # at init, file is at 0 position
    self.__startPosition = 0 # start position of token
    self.__endPosition = 0 # end position of token
    self.__nextCalled = False; # all() or next()

  def _codeExists (self):
    """Checks if there is more code to read from the source."""
    return self.__textPosition < self.__length

  def _counter (self):
    """Increments the file read counter."""
    self.__textPosition += 1

  def _isValidCode (self):
    """Check if character read from the source is a valid character in the language."""
    character = self._character()
    if(character in letters) or (character in digits) or\
      (character in symbols) or (character in whitespaces):
      return True
    else:
      return False

  def _character (self):
    """Reads in a character from the source code and returns it."""
    return (self.__code[self.__textPosition])

  # ------------ IDENTIFY TOKENS ------------

  def _identifierToken (self):
    """
    Creates token for identifiers and keywords.
    identifier = letter {letter | digit} .
    """
    value = ""
    self.__startPosition = self.__textPosition # get start position

    while self._codeExists():
      character = self._character()
      if (character in letters) or (character in digits):
        value += character
        self._counter()
      else:
        break
    self.__endPosition = self.__textPosition - 1 # store end position
    assert self.__endPosition >= self.__startPosition
    
    rToken = None # create token (identifier)
    if self._isKeyword (value):
      rToken = Token("keyword", self.__startPosition, self.__endPosition, value)
    else:
      rToken = Token("identifier", self.__startPosition, self.__endPosition, value)
    return rToken

  def _numberToken (self):
    """Returns a number token from a set of continuous tokens."""
    value = ""
    self.__startPosition = self.__textPosition # start position
    
    while self._codeExists():
      character = self._character()
      if character in digits: # if a digit, keep making it a number
        value += character
        self._counter()
      else:
        break
    self.__endPosition = self.__textPosition - 1 # end position
    assert self.__endPosition >= self.__startPosition
    return Token("integer", self.__startPosition, self.__endPosition, int (value)) # return token

  def _isKeyword (self, value):
    """Returns true if the token value is a keyword in SIMPLE."""
    assert type(value) is str
    return value in keywords

  def _symbolToken (self):
    """Returns a symbol token for tokens in SIMPLE."""

    self.__startPosition = self.__textPosition # start position
    value = self._character() # prechecked
    self._counter() # increment counter
    if (value == "<") or (value == ">") or (value == ":"):
      if self._codeExists():
        character = self._character()
        if character in "=":
          value += character
          self._counter()

    self.__endPosition = self.__textPosition - 1
    assert self.__endPosition >= self.__startPosition
    return Token("symbol", self.__startPosition, self.__endPosition, value)

  def _handleComment (self):
    """
    Handles comments in SIMPLE. Comments are of the form:
    (* some text *). Comments can be multi-line.
    """

    # we are inside the comment with '(*' matched
    self.__startPosition = self.__textPosition
    _found = 0
    while self._codeExists():
      if self._character() in "*":
        _found = 1
      elif self._character() in ")" and _found == 1:
        return
      else:
        _found = 0
      self._counter()

  def next(self):
    """
    Returns the next token.
    Operates on the actual piece of source code and returns a
    valid token value. This takes care of whitespace, comments,
    letters, digits and symbols as defined in the language. 
    Based on the current value of the character, it requests helper 
    functions to retrieve a token.
    """

    self.__nextCalled = True # you cannot call all now
    token = None
    while self._codeExists():
      if self._character() in whitespaces + eol:
        self._counter()
      elif self._character() in "(":
        self._counter()
        if self._codeExists() and self._character() in "*": # start of comment
          self._counter()
          self._handleComment()
          self._counter()
        else:
          token = Token("symbol", self.__textPosition - 1, self.__textPosition - 1, "(")
          return token
      else:
        break

    if self._codeExists():
      character = self._character() # get character
      try:
        if character in letters:
          token = self._identifierToken()
        elif character in digits:
          token = self._numberToken()
        elif character in symbols:
          token = self._symbolToken()
        else:
          exc_string = ("invalid character: '%s' discovered at location '%s'; is not part of SIMPLE.") %\
                        (character, self.__textPosition)
          raise InvalidCharacterException (exc_string)
      except InvalidCharacterException, e:
        return e;
        self._counter()

    if token is None:
      token = Token("eof", self.__textPosition, self.__textPosition, None)
    return token


  def all (self):
    """Return: [Token [Token...]]"""

    if self.__nextCalled is True:
      try:
        raise SeekAllAfterNextException("all() called after next() -- will terminate.")
      except SeekAllAfterNextException, e:
        sys.stderr.write("error: " + e)
        sys.exit(1)

    tokenList = [] # store the token list
    tokenValue = None # token value
    error = None # need to establish KError class that handles error requests

    while self._codeExists():
      tokenValue = self.next() # get token
      if not isinstance(tokenValue, Token): # is token of type Token?
        error = tokenValue 
        break
      if tokenValue.kind() is not "eof":
        tokenList.append(tokenValue)

    # only if error has not occured:
    if error is None:
      tokenList.append(self.next()) # accept end-of-source token
    return tokenList, error
