#!/usr/bin/python
#
# kexception.py
#
# Created by Kartik Thapar on 02/16/2013 at 02:46:13
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

class InvalidCharacterException (Exception):
  """Deals with invalid characters in source input that are not part of SIMPLE."""
  pass


class SeekAllAfterNextException (Exception):
  """Deals with the invocation of the 'all()' after 'next()' has been called already."""
  pass


class TokenMismatchException (Exception):
  """Deals with the token kind mismatch in the parser."""
  pass


class SymbolTableWriteException (Exception):
  """Deals with the situation when an entry for an insert already exists in the symbol table."""
  pass


class SymbolTableReadException (Exception):
  """Deals with the situation when the symbol table doesn't have the entry that is requested by the parser"""
  pass
