@Author: [Kartik Thapar](mailto:Kartik.Thapar@jhu.edu)

# Assignment 6 - Code Generator

# List of files:
	
	./kcodegen.py
	./kexception.py
	./kparser.py
	./kscanner.py
	./ksymboltable.py
	./ktoken.py
	./ktree.py
	./listeners
	./listeners/__init__.py
	./listeners/kdotlistener.py
	./listeners/kerrorlistener.py
	./listeners/klistener.py
	./listeners/ktextlistener.py
	./README.md
	./sc
	./visitors
	./visitors/__init__.py
	./visitors/abstract_syntax_tree
	./visitors/abstract_syntax_tree/__init__.py
	./visitors/abstract_syntax_tree/kdotvisitor.py
	./visitors/abstract_syntax_tree/kinterpretervisitor.py
	./visitors/abstract_syntax_tree/ktextvisitor.py
	./visitors/abstract_syntax_tree/kvisitor.py
	./visitors/symbol_table
	./visitors/symbol_table/__init__.py
	./visitors/symbol_table/kdotvisitor.py
	./visitors/symbol_table/kenvironment.py
	./visitors/symbol_table/ktextvisitor.py
	./visitors/symbol_table/kvisitor.py

# How to run:

`sc` is the binary that needs to be executed.

    > ./sc -s # for scanner
    # requires standard input

    > ./sc -s filename # for scanner
    # requires filename to be a valid file

    > ./sc -c # for parser; also generates CST
    # requires standard input

    > ./sc -c filename # for parser; also generates CST
    # requires filename to be valid

    > ./sc -cg # for parser; also generates graph output
    # requires standard input

    > ./sc -cg filename # for parser; also generates graph output
    # requires filename to be valid

    > ./sc -t # for parser; also generates symbol table text output
    # requires standard input

    > ./sc -t filename # for parser; also generates symbol table text output
    # requires filename to be valid

    > ./sc -tg # for parser; also generates symbol table graph output
    # requires standard input

    > ./sc -tg filename # for parser; also generates symbol table graph output
    # requires filename to be valid

    > ./sc -a # for parser; also generates abstract syntax tree text output
    # requires standard input

    > ./sc -a filename # for parser; also generates abstract syntax tree text output
    # requires filename to be valid

    > ./sc -ag # for parser; also generates abstract syntax tree graph output
    # requires standard input

    > ./sc -ag filename # for parser; also generates abstract syntax tree graph output
    # requires filename to be valid

    > ./sc -i # for interpreter;
    # requires standard input

    > ./sc -i filename # for interpreter;
    # requires filename to be valid

    > ./sc
    # requires standard input
    
    > ./sc filename
    # requires filename to be valid

# Architecture

**Target architecture: MIPS**

Example program:

	PROGRAM X;
	
	  TYPE I = INTEGER;
	  TYPE A = ARRAY 10 OF I;
	
	  VAR i : INTEGER;
	  VAR j : A;
	
	BEGIN
	
	  i := 0;
	
	  REPEAT
	    j[i] := i;
	    i := i + 1
	  UNTIL
	    i = 10 
	  END;
	
	  WRITE i;
	
	  i := 0;
	  WHILE
	    i < 10
	  DO
	    WRITE j[i];
	    i := i + 1
	  END
	
	END X.

Example gcc output:

	# .author: Kartik Thapar
	# .file:   SIMPLE code generator
	
	        # PROLOG initialized
	
	        .file   1 "no.c"
	        .section .mdebug.abi32
	        .previous
	        .gnu_attribute 4, 1
	        .abicalls
	
	        # storage allocation
	vars:
	        .space  44                               # .space total size
	
	        .rdata
	        .align  2
	$LC0:
	        .ascii  "%d\012\000"                     # '%d\012\000' used for __printf__
	        .align  2
	$LC1:
	        .ascii  "%d\000"                         # '%d\000' used for __scanf__
	        .text
	        .align  2
	        .globl  main
	        .set    nomips16
	        .ent    main
	        .type   main, @function
	main:
	        .frame  $fp,32,$31                       # use a stack of size: '32'
	        .set    noreorder
	        .set    nomacro
	
	        la      $s7, vars
	        addiu   $sp,$sp,-32
	        sw      $31,28($sp)
	        sw      $fp,24($sp)
	        move    $fp,$sp
	        lui     $28,%hi(__gnu_local_gp)
	        addiu   $28,$28,%lo(__gnu_local_gp)
	        .cprestore  16
	
	        # --------------- __PROGRAM_START__ ---------------
	        
	        #  -> __ASSIGNMENT__ <- 
	
	        add     $2,$s7,0                         # applying offset '0' for 'i'
	
	        # ksymboltable.Integer assignment initialized
	
	        li      $3,0
	        sw      $3,0($2)
	
	        # -> __REPEAT__ <-
	
	$L1:
	        add     $2,$s7,0                         # applying offset '0' for 'i'
	        lw      $2,0($2)
	        li      $3,10
	        bne     $2,$3,$L2                        # branches to '$L2' if not equal
	        nop
	        b       $L3 # issue?
	        nop
	$L2:
	
	        #  -> __ASSIGNMENT__ <- 
	
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	        add     $2,$s7,4                         # applying offset '4' for 'j'
	        add     $t0,$s7,0                        # applying offset '0' for 'i'
	        lw      $t0,0($t0)
	        sll     $t0,$t0,2
	        add     $2,$2,$t0
	
	        # ksymboltable.Integer assignment initialized
	
	        lw      $3,0($3)
	        sw      $3,0($2)
	
	        #  -> __ASSIGNMENT__ <- 
	
	        add     $2,$s7,0                         # applying offset '0' for 'i'
	        lw      $2,0($2)
	        addiu   $2,$2,1                          # add immediate 1 to register '$2'
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	
	        # ksymboltable.Integer assignment initialized
	
	        sw      $2,0($3)
	        b       $L1
	        nop
	$L3: # issue?
	
	        #  -> __WRITE__ <- 
	        # printf initialized
	
	        lui     $3,%hi($LC0)                     # loading .ascii from $LCO
	        addiu   $3,$3,%lo($LC0)
	        move    $4,$3                            # first argument to printf
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	        lw      $3,0($3)
	        move    $5,$3
	        lw      $3,%call16(printf)($28)          # printf call to print contents of $5
	        nop
	        move    $25,$3
	        jalr    $25
	        nop
	        lw      $28,16($fp)                      # __printf__ end
	
	        #  -> __ASSIGNMENT__ <- 
	
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	
	        # ksymboltable.Integer assignment initialized
	
	        li      $2,0
	        sw      $2,0($3)
	
	        # -> __IF__ <-
	
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	        lw      $3,0($3)
	
	        li      $2,10
	        slt     $3,$3,$2                         # set register '$3' to 1 if '$3' is less than '$2'
	        beq     $3,$0,$L4                        # branches to '$L4' if '$3' is equal to zero
	        nop
	
	        # -> __REPEAT__ <-
	
	$L5:
	        add     $2,$s7,0                         # applying offset '0' for 'i'
	        lw      $2,0($2)
	        li      $3,10
	        slt     $2,$2,$3                         # set register '$2' to 1 if '$2' is less than '$3'
	        bne     $2,$0,$L6
	        nop
	        b       $L7 # issue?
	        nop
	$L6:
	
	        #  -> __WRITE__ <- 
	        # printf initialized
	
	        lui     $3,%hi($LC0)                     # loading .ascii from $LCO
	        addiu   $3,$3,%lo($LC0)
	        move    $4,$3                            # first argument to printf
	        add     $3,$s7,4                         # applying offset '4' for 'j'
	        add     $2,$s7,0                         # applying offset '0' for 'i'
	        lw      $2,0($2)
	        sll     $2,$2,2
	        add     $3,$3,$2
	        lw      $3,0($3)
	        move    $5,$3
	        lw      $3,%call16(printf)($28)          # printf call to print contents of $5
	        nop
	        move    $25,$3
	        jalr    $25
	        nop
	        lw      $28,16($fp)                      # __printf__ end
	
	        #  -> __ASSIGNMENT__ <- 
	
	        add     $3,$s7,0                         # applying offset '0' for 'i'
	        lw      $3,0($3)
	        addiu   $3,$3,1                          # add immediate 1 to register '$3'
	        add     $t1,$s7,0                        # applying offset '0' for 'i'
	
	        # ksymboltable.Integer assignment initialized
	
	        sw      $3,0($t1)
	        b       $L5
	        nop
	$L7: # issue?
	        b       $L8
	        nop
	$L4:
	
	$L8:
	
	        # --------------- __PROGRAM_END__ ---------------
	
	        # EPILOG initialized
	
	        move    $2,$0
	        move    $sp,$fp
	        lw      $31,28($sp)
	        lw      $fp,24($sp)
	        addiu   $sp,$sp,32
	        j       $31
	        nop
	
	        .set    macro
	        .set    reorder
	        .end    main
	        .size   main, .-main
	
