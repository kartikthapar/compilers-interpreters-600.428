#!/usr/bin/python
#
# kdotvisitor.py
#
# Created by Kartik Thapar on 04/01/2013 at 04:20:45
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

from kvisitor import Visitor
IDENT_VAL = 2

class DotVisitor (Visitor):
  """Creates the DOT ouput for the abstract syntax tree created by the Parser."""
  
  def __init__ (self):
    self.__indentationLevel = -1
    self.__dotOutput = ""

  def _dot (self):
    """Generate label here."""
    self.__indentationLevel += 1
    return "label" + str(self.__indentationLevel)

  def dotOutput (self):
    """Returns DOT output of the symbol table (from the parser)."""
    self.__dotOutput = "digraph X {\n" + self.__dotOutput + "}"
    return self.__dotOutput

  def visitInstructions (self, instruction):
    pass

  def visitInstruction(self, instruction, dot):
    if instruction.next():
      n = instruction.next().accept(self)
      self.__dotOutput += dot + " -> " + n + " [label=next]" + ";\n"
      self.__dotOutput += "{rank=same; " + dot + " " + n + "}\n"

  def visitAssignment (self, _assign):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\":=\",shape=box]" + ";\n"
    e = _assign.location().accept(self)
    f = _assign.expression().accept(self)
    self.__dotOutput += dot + " -> " + e + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + f + " [label=expression]" + ";\n"
    self.visitInstruction(_assign, dot)
    return dot

  def visitIf (self, ifInstruction):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"If\",shape=box]" + ";\n"
    c = ifInstruction.condition().accept(self)
    self.__dotOutput += dot + " -> " + c + " [label=condition]" + ";\n"
    t = ifInstruction.trueInstruction().accept(self)
    self.__dotOutput += dot + " -> " + t + " [label=true]" + ";\n"
    if ifInstruction.falseInstruction():
      f = ifInstruction.falseInstruction().accept(self)
      self.__dotOutput += dot + " -> " + f + " [label=false]" + ";\n"
    self.visitInstruction(ifInstruction, dot)
    return dot

  def visitRepeat (self, repeat):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Repeat\",shape=box]" + ";\n"
    c = repeat.condition().accept(self)
    self.__dotOutput += dot + " -> " + c + " [label=condition]" + ";\n"
    i = repeat.instruction().accept(self)
    self.__dotOutput += dot + " -> " + i + " [label=instructions]" + ";\n"
    self.visitInstruction(repeat, dot)
    return dot

  def visitRead (self, read):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Read\",shape=box]" + ";\n"
    e = read.location().accept(self)
    self.__dotOutput += dot + " -> " + e + " [label=location]" + ";\n"
    self.visitInstruction(read, dot)
    return dot

  def visitWrite (self, write):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Write\",shape=box]" + ";\n"
    e = write.expression().accept(self)
    self.__dotOutput += dot + " -> " + e + " [label=expression]" + ";\n"
    self.visitInstruction(write, dot)
    return dot

  def visitNumber (self, number):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Number\",shape=box]" + ";\n"
    x = self._dot()
    self.__dotOutput += x + \
      " [label=\"" + str(number.constant().value()) + "\",shape=diamond]" + ";\n"
    self.__dotOutput += dot + " -> " + x + " [label=ST]" + ";\n"
    return dot

  def visitVariable (self, variable):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Variable\",shape=box]" + ";\n"
    x = self._dot()
    self.__dotOutput += x + \
      " [label=\"" + str(variable.variable().name()) +  "\",shape=circle]" + ";\n"
    self.__dotOutput += dot + " -> " + x + " [label=ST]" + ";\n"
    return dot
    
  def visitIndex (self, index):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Index\",shape=box]" + ";\n"
    left = index.location().accept(self)
    right = index.expression().accept(self)
    self.__dotOutput += dot + " -> " + left + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=expression]" + ";\n"
    return dot

  def visitField (self, field):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"Field\",shape=box]" + ";\n"
    left = field.location().accept(self)
    right = field.variable().accept(self)
    self.__dotOutput += dot + " -> " + left + " [label=location]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=variable]" + ";\n"
    return dot

  def visitBinary (self, binary):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"" + binary.operator() + "\",shape=box]" + ";\n"
    left = binary.leftExpression().accept(self)
    right = binary.rightExpression().accept(self)
    self.__dotOutput += dot + " -> " + left + " [label=left]" + ";\n"
    self.__dotOutput += dot + " -> " + right + " [label=right]" + ";\n"
    return dot

  def visitCondition (self, condition):
    dot = self._dot()
    self.__dotOutput += dot + " [label=\"" + condition.relation() + "\",shape=box]" + ";\n"
    l = condition.leftExpression().accept(self)
    r = condition.rightExpression().accept(self)
    self.__dotOutput += dot + " -> " + l + " [label=left]" + ";\n"
    self.__dotOutput += dot + " -> " + r + " [label=right]" + ";\n"
    return dot
