#!/usr/bin/python
#
# kenvironment.py
#
# Created by Kartik Thapar on 04/08/2013 at 18:56:50
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys
import ksymboltable as KST
from copy import deepcopy
from kvisitor import Visitor

class Box (object):
  pass

# ----------- INTEGER BOX --------------

class IntegerBox (Box):
  def __init__ (self):
    self.__boxValue = 0

  def boxType (self):
    return "INTEGER"

  def setValueForBox (self, value):
    self.__boxValue = value

  def boxValue (self):
    return self.__boxValue

  def __str__ (self):
    return "<IntegerBox: <value:%s>>" % (self.__boxValue)

# ------------ ARRAY BOX ------------

class ArrayBox (Box):
  def __init__ (self, length, boxType):
    self.__length = length # length of array
    self.__boxList = []
    self.__boxType = boxType
    for i in range(0, length):
      self.__boxList.append(deepcopy(boxType)) # create a box for all indices in box list

  def canIndexForIndex (self, index):
    """Return if index is a valid is in bounds."""
    return True if index < len(self.__boxList) else False

  def boxType (self):
    return self.__boxType

  def boxAtIndex (self, index):
    """Return element in the array at a particular index."""
    return self.__boxList[index] # return if eleme

  def setValueForBox (self, value):
    """Overwrites the calling array box object with another array box object completely (deepcopy)."""
    i = value.iterator() # will come as a deepcopy, so chill out!
    newList = []
    try:
      while True:
        val = i.next()
        newList.append(val)
    except:
      self.__boxList = newList

  def setValueForBoxAtIndex (self, index, value):
    """Set the value of the memory at a particular index in the array."""
    if isinstance(self.__boxType, IntegerBox):
      self.__boxList[index].setValue(value)
    elif isinstance(self.__boxType, ArrayBox):
      self.__boxList[index] = value # a : ARRAY 10 OF ARRAY 10 OF INTEGER; a[5] = a[7]

  def __str__ (self):
    return "<ArrayBox: <length:%s>, <contents:%s>>" % (self.__length, self.__boxList)

  def iterator (self):
    return iter(self.__boxList)

# --------- RECORD BOX ---------

class RecordBox (Box):
  """"RECORD" {IdentifierList ":" Type ";"} "END" ."""

  def __init__ (self, boxMap):
    self.__boxMap = boxMap # create a container for record variables

  def setValueForBox (self, value):
    """Overwrites the calling record box object with another record box object completely (deepcopy)."""
    self.__boxMap = value.boxMap()

  def boxMap (self):
    """Returns the hash table/dictionary for a particular record."""
    return self.__boxMap

  def retrieveVariable (self, variable):
    """Returns the variable variable from record."""
    return self.__boxMap[variable]

  def setValueForVariable (self, variable, newVariable):
    """Set value for a particular variable in the record."""
    self.__boxMap[variable] = newVariable

  def __str__ (self):
    return "<RecordBox: <variables: %s>>" % (self.__boxMap)

  def iterator (self):
    return iter(self.__boxMap.items()) # return the iterator for the items [0][1]

# ---------------- CREATE ENVIRONMENT ------------------

class EnvironmentVisitor (Visitor):
  """Visitor class that operates on the symbol table data structure."""

  def visitVariable (self, variable):
    log ("__variable__")
    return variable.elementType().accept(self) # depending on the type of the variable, visit

  def visitInteger (self, integer):
    log ("__integer__")
    return IntegerBox() # create an integer box and send

  def visitArray (self, array):
    log ("__array__")
    boxType = array.elementType().accept(self) # based on the type of array, create a box
    return ArrayBox(array.length(), boxType) # create an array box of length, .length()

  def visitRecord (self, record):
    log ("__record__")
    boxMap = record.scope().accept(self)
    return RecordBox(boxMap)

  def visitScope (self, scope):
    log ("__scope__")
    environment = {}
    symbolTable = scope.symbolTable()
    for identifer in sorted(symbolTable):
      element = symbolTable[identifer]
      if isinstance(element, KST.Variable):
        boxObj = element.accept(self)
        environment[element.name()] = boxObj
    return environment

def log(v):
  # print v
  pass
