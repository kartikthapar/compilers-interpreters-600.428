#!/usr/bin/python

#
# test-token.py
#
# Created by Kartik Thapar on 02/15/2013 at 23:43:54
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys

sys.path.append("../") # look for the directory above for tokens.py

import ktoken as KToken

def keywordCheck():
	"""
	Test keyword tokens.
	"""

	"""
	object = Token(kind, start_position, end_position, value = None):
	keywords = ["PROGRAM", "BEGIN", "END", "CONST", "TYPE", "VAR",
			"ARRAY", "OF", "RECORD", "DIV", "MOD", "IF", "THEN", "ELSE",
			"REPEAT", "UNTIL", "WHILE", "DO", "WRITE", "READ"]
	"""

	keywords = ["PROGRAM", "BEGIN", "END", "CONST", "TYPE", "VAR",
			"ARRAY", "OF", "RECORD", "DIV", "MOD", "IF", "THEN", "ELSE",
			"REPEAT", "UNTIL", "WHILE", "DO", "WRITE", "READ"]

	# check all keywords --- no breaks: useless though
	for keyword in keywords:
		t = KToken.Token ("keyword", 2, 5, keyword)
		print str(t)

	# check keyword against int value --- assert should should
	t = KToken.Token ("keyword", 1, 5, 666)
	t = KToken.Token ("keyword", "666", 5, 10)
	t = KToken.Token ("keyword", 1, "666", 10)

	# check if wrong kind used --- assert should shout
	t = KToken.Token ("keyworda", 1, 5, 1)


def numberCheck():
	"""
	Test number tokens.
	"""

	digits = "0123456789"

	# check all digits --- should work: checking 1 is enough
	for num in digits:
		t = KToken.Token ("integer", 0, 1, int(num))
		print str(t)

	# check "integer" with non-suitable type arguments --- assert should shout
	# t = KToken.Token ("integer", "0", 1, int(num))
	# t = KToken.Token ("integer", 0, "1", int(num))
	# t = KToken.Token ("integer", 0, 1, str(int(num)))

	# integer overflow kind of? --- check
	# added integer check for LongType
	val = 2**31
	t = KToken.Token ("integer", 0, 1, int(str(val)))
	print str(t)

	val = 2**62
	t = KToken.Token ("integer", 0, 1, int(str(val)))
	print str(t)



# ------------------------

# keywordCheck()
# numberCheck()



