#
# runtest.sh
#
# Created by Kartik Thapar on 02/17/2013 at 04:16:50
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

# run test for 'sc'
# usage: sc [-h] [-s | -c | -t | -a | -i] [filename]

echo 'sc --------------------------------------------------------'
sc # error - use scanner, show help

echo 'sc - ------------------------------------------------------'
sc -

echo 'sc -h------------------------------------------------------'
sc -h # help

echo 'sc -h -s---------------------------------------------------'
sc -h -s # help

echo 'sc -c------------------------------------------------------'
sc -c

echo 'sc -t------------------------------------------------------'
sc -t

echo 'sc -a------------------------------------------------------'
sc -a

echo 'sc -i------------------------------------------------------'
sc -i

echo 'sc -s -- Apply CTRL+D--------------------------------------'
sc -s # apply CTRL+D

echo 'sc --s-----------------------------------------------------'
sc --s

# wrong file name
echo 'sc -s filename -- no file called filename------------------'
sc -s filename

echo 'sc -s runtest_ef_666---------------------------------------'
touch runtest_ef_666
sc -s runtest_ef_666
rm runtest_ef_666

echo "PROGRAM VAR somestuff := some-stuff;-----------------------"
touch runtest_tf_666
echo "PROGRAM VAR somestuff := some-stuff;" > runtest_tf_666
sc -s runtest_tf_666
rm runtest_tf_666

