#!/usr/bin/python
#
# test-scanner.py
#
# Created by Kartik Thapar on 02/17/2013 at 21:29:45
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

#!/usr/bin/python

#
# test-token.py
#
# Created by Kartik Thapar on 02/15/2013 at 23:43:54
# Copyright (c) 2013 Kartik Thapar. All rights reserved.
#

import sys

sys.path.append("../") # look for the directory above for tokens.py

import kscanner as KScanner

def commentCheck():
	"""
	Test comments
	"""

	s = "PROGRAM (* something $_*) some"
	scanner = KScanner.Scanner(s)
	tokenlist, error = scanner.all()
	for token in tokenlist:
		print str(token)

def invalidCharacterCheck():

	"""
	Test for invalid characters

	s
	"""

	s = "This is a dollar sign: $ non sense"
	scanner = KScanner.Scanner(s)
	tokenlist, error = scanner.all()
	for tokenvalue in tokenlist:
		print str(tokenvalue)
	if error is not None:
		print "error:", error

	s = "This is a percentage sign: % non sense"
	scanner = KScanner.Scanner(s)
	tokenlist, error = scanner.all()
	for tokenvalue in tokenlist:
		print str(tokenvalue)
	if error is not None:
		print "error:", error


# commentCheck()
invalidCharacterCheck()

