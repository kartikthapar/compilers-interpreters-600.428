# Assignment 1

# Files included:

	README, kexception.py, kscanner.py, ktoken.py, sc*

	total 72
	-rw-r--r--  1 kartikthapar  staff    223 Feb 17 22:38 README
	-rw-r--r--  1 kartikthapar  staff    490 Feb 17 16:21 kexception.py
	-rw-r--r--@ 1 kartikthapar  staff  11063 Feb 17 22:14 kscanner.py
	-rw-r--r--  1 kartikthapar  staff   3085 Feb 17 21:19 ktoken.py
	-rwxr-xr-x@ 1 kartikthapar  staff   2734 Feb 17 22:14 sc*

# How to run:

`sc` is the binary that needs to be executed.

	> ./sc -s # for scanner
	# requires standard input
	
	> ./sc -s filename # for scanner
	# requires filename to be a valid file
	
