#
# Author: Kartik Thapar
# Date: 02/05/2013
#

#!/usr/bin/env python

import sys

# execution_starts_here
def main():
	for line in iter(sys.stdin.readline, ''):
		print line,

if __name__ == '__main__':
	main();

#----------------------------------------------------------------

# raw_input() implementation :: stupid

# data = None # define data input

# # define infinite stream available
# while 1:
# 	# try for data --- catch if KeyboardInterrupt
# 	try:
# 		# data = sys.stdin.readline()
# 		# data = data.rstrip()
# 		data = raw_input()
# 	except KeyboardInterrupt:
# 		print # done with stuff:: just next line like in 'cat'
# 		sys.exit(1)
# 	except EOFError:
# 		print
# 		# if data != None:
# 		# 	print data
# 		sys.exit(1)
# 	print data # cat: print data
