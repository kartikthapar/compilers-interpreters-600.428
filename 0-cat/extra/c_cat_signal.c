/*
@Author: Kartik Thapar
@Date: 02/05/13
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define BUFFSIZE 32768

static void sig_int(int);

int main(int argc, char **argv) {
	int n;

	char buf[BUFFSIZE]; //buffsize defined

	// SIGINT: signal interrupt for ctrl+c
	if (signal(SIGINT, sig_int) == SIG_ERR) {
		fprintf(stderr, "signal error: %d\n", strerror(errno));
		exit(1);
	}

	// reading from standard input
	while ((n = read(STDIN_FILENO, buf, BUFFSIZE)) > 0) {
		if (write(STDOUT_FILENO, buf, n) != n) {
			fprintf(stderr, "write error\n");
			exit(1);
		}
	}

	if (n < 0) {
		fprintf(stderr, "read error\n");
		exit(1);
	}

	return(0);
}


void sig_int(int signo){
	fprintf(stdout, "\nCtrl+c. Exiting...\n");
	exit(signo);
}
