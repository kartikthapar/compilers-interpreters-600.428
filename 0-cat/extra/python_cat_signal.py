#
# @Author: Kartik Thapar
# @Date: 02/05/13
#

#!/usr/bin/env

import signal # for SIGINT
import sys


# define interrupt handler and later configure it for SIGINT in main()
def signal_interrupt_handler(signal, f):
	print("\nDone.") # done with stuff
	sys.exit(1);


# execution_starts_here
def main():

	# define signal handler for SIGNINT (like in C)
	signal.signal(signal.SIGINT, signal_interrupt_handler)

	data = None # define data input
	
	# define infinite stream available
	while 1:
		data = sys.stdin.readline()
		data = data.rstrip()
		print data # cat: print data

	signal.pause() # when it's over, it's over!
		
if __name__ == '__main__':
	main();