/*
Author: Kartik Thapar
Date: 02/05/13
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// define buffer size as 32768
#define BUFSIZE 32768

int main(int argc, char **argv) {
	int n;

	char buf[BUFSIZE]; //bufsize defined

	// read from the standard input indefinitely
	while ((n = read(STDIN_FILENO, buf, BUFSIZE)) > 0) {
		if (write(STDOUT_FILENO, buf, n) != n) {
			fprintf(stderr, "write error\n"); //if cannot write
			exit(1);
		}
	}

	// if you cannot read, say you cannot read
	if (n < 0) {
		fprintf(stderr, "read error\n");
		exit(1);
	}

	return(0);
}
