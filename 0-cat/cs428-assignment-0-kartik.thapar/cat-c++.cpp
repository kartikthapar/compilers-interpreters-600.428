/*
Author: Kartik Thapar
Date: 02/08/2013
*/

#include <iostream>
#include <string>
#include <sstream>

using namespace std;

int main (){
    string inputString;
    
    while(getline(cin, inputString)){
        cout << inputString << "\n";;
    }

    if (cin.bad()) {
    // io_error; give error warnings and stuff
    } else if (!cin.eof()) {
        // format error; give error warnings and stuff
    } else {

        //end_of_file here
        exit(0);

        /* 
        - There is no need of this as we are returning 0 anyway.
        - But in case EOF needs to be customized, add code here.
        - Like: `cout << "Thank You. BBye";`
        */

    }

    return 0;
}